The project is based on ModelViewPresenter (MVP) architecture
Single activity multiple fragment based one
Detail activity controls the flow after LoginActivity
In this architecture there are view presenters and models are there
The view includes all the functions and required model for operation
For example consider the Login Activity

LoginActivity is extends MvpActivity<> which requires a presenter here it is LoginPresenter
and LoginActivity is implemented with LoginView
all the override methods are from LoginView
Now how these three works?

Start from Login Activity
.........................
 When user click on submit after verification it will call  "presenter.login(this, loginParamModel)"
 So all the logical operations need to be done in Presenter
 In presenter.login(this, loginParamModel) we added a subscriber for handling all retrofit requests
 ApiStories handle all requests
.........................