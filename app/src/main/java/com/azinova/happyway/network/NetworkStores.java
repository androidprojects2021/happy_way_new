package com.azinova.happyway.network;


import com.azinova.happyway.model.input.Login.LoginParamModel;
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputParam;
import com.azinova.happyway.model.input.PaymentInput.PaymentInputParam;
import com.azinova.happyway.model.input.contract.InputCreateContract;
import com.azinova.happyway.model.input.contract.InputGetBuildings;
import com.azinova.happyway.model.input.customerList.CustomerListParamModel;
import com.azinova.happyway.model.input.customer_pay.InputSingleCustomer;
import com.azinova.happyway.model.input.newReading.NewReadingParamModel;
import com.azinova.happyway.model.input.newReading.NewReadingParamModelUpdated;
import com.azinova.happyway.model.output.CustomerListNew.CustomerResponse;
import com.azinova.happyway.model.output.InvoiceResponse.InvoiceResponse;
import com.azinova.happyway.model.output.Login.LoginResponse;
import com.azinova.happyway.model.output.PaymentHistory.PaymentHistoryResponse;
import com.azinova.happyway.model.output.PaymentTypeModel.PaymentTypeModel;
import com.azinova.happyway.model.output.ReadingHistoryModel.ReadingHistoryResponse;
import com.azinova.happyway.model.output.ReadingResponse.ReadingResponse;
import com.azinova.happyway.model.output.Report.ResponseReport;
import com.azinova.happyway.model.output.SavePayment.SavePaymentResponse;
import com.azinova.happyway.model.output.contract.ResponseCreateContract;
import com.azinova.happyway.model.output.contract.ResponseGetBuildings;
import com.azinova.happyway.model.output.customer_pay.ResponseSingleCustomer;
import com.azinova.happyway.model.output.upliad_image.ResponseImgUploads;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;
import rx.Single;

/**
 * Created by Akhil on 23/09/2019
 */

public interface NetworkStores {

    //@GET("/d2d/data_collector/customer/list/?")
    //Observable<CustomersResponse> getCustomerList(@Query("routeId") int user_id);

    @POST("utility/login")
    Observable<LoginResponse> doLogin(@Body LoginParamModel loginParamModel);

    @POST("utility/customer_list")
    Observable<CustomerResponse> getCustomerList(@Body CustomerListParamModel customerListParamModel);

    @POST("utility/customer_list")
    Observable<ResponseSingleCustomer> getSingleCustomer(@Body InputSingleCustomer inputSingleCustomer);

    @POST("utility/payment_customer_list")
    Observable<ResponseSingleCustomer> getSingleCustomerNew(@Body InputSingleCustomer inputSingleCustomer);


    @POST("utility/meter_reading")
    Observable<ReadingResponse> postNewReading(@Body NewReadingParamModel newReadingParamModel);


 /* @POST("utility/invoice")
    Observable<InvoiceResponse> generateInvoice(@Body NewReadingParamModel newReadingParamModel); */


    @POST("utility/invoice")
    Observable<InvoiceResponse> generateInvoice(@Body NewReadingParamModelUpdated newReadingParamModel);


    @POST("utility/payment_method")
    Observable<PaymentTypeModel> getPaymentType(@Body CustomerListParamModel customerListParamModel);

    @POST("utility/payment_history")
    Observable<PaymentHistoryResponse> getPaymentHistory(@Body PaymentHistoryInputParam paymentHistoryParams);

    @POST("utility/meter_historyParameters")
    Observable<PaymentTypeModel> getReadingHistory(@Body CustomerListParamModel customerListParamModel);

    @POST("utility/payments")
    Observable<SavePaymentResponse> savePayment(@Body PaymentInputParam customerListParamModel);

   /* @POST("utility/meter_history")
    Observable<ReadingHistoryResponse> getreadingHistory(@Body PaymentHistoryInputParam historyParams);*/

    @POST("utility/invoice_history")
    Observable<ReadingHistoryResponse> getreadingHistory(@Body PaymentHistoryInputParam historyParams);


    @POST("utility/summary_report")
    Observable<ResponseReport> getsummaryReport(@Body PaymentHistoryInputParam historyParams);


    @POST("utility/building_list")
    Observable<ResponseGetBuildings> getBuildings(@Body InputGetBuildings inputGetBuildings);

    @POST("customercontract/create")
    Observable<ResponseCreateContract> createCustomerContract(@Body InputCreateContract inputCreateContract);


    @Multipart
    @POST("customer/document_uploading")
    Observable<ResponseImgUploads> uploadImages(@Part("customer_id") RequestBody customer_id,
                                                @Part MultipartBody.Part signature,
                                                @Part MultipartBody.Part emid_front,
                                                @Part MultipartBody.Part emid_back,
                                                @Part MultipartBody.Part ejari);


}