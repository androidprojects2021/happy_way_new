package com.azinova.happyway;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azinova.happyway.model.output.PaymentHistory.ResponseItem;
import com.azinova.happyway.utils.ButtonDelayUtils;
import com.azinova.happyway.utils.HandlerUtils;
import com.azinova.happyway.utils.ThreadPoolManager;
import com.iposprinter.iposprinterservice.IPosPrinterCallback;
import com.iposprinter.iposprinterservice.IPosPrinterService;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import woyou.aidlservice.jiuiv5.IWoyouService;

public class PrintPaymentHistory extends Activity implements View.OnClickListener {

    private static final String TAG = "PrintReadingHistory";
    private static final String VERSION = "V1.1.0";


    private static final String SERVICE＿PACKAGE = "woyou.aidlservice.jiuiv5";
    private static final String SERVICE＿ACTION = "woyou.aidlservice.jiuiv5.IWoyouService";
    private IWoyouService woyouService;


    DecimalFormat format = new DecimalFormat("##.00");
    private Button print_key;
    private Button close_key;
    private ImageView popUpCloseButton;
    private TextView receiptNo, dateTimeText, nameText, idText, flat, building, contract, mobile, outBal, paidAmt, remainBal, payTypeTextPay, chequeDateText, chequeNoText, cardrefText;
    private LinearLayout chequeDateLay, chequeNoLay, cardrefLay;

    private Button b_koubeiPay1;

    private ResponseItem response;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_payment_history);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        response = getIntent().getParcelableExtra("print");
        assert response != null;

        connectPrinterService();
        initPrinter();

        innitView();
        populate();



    }

    private void populate() {


        /*String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        dateTimeText.setText(currentDate + " " + currentTime);*/


        if (response.getPaymentDate().equals("") || response.getPaymentDate().equals(null)) {
            dateTimeText.setText("NA");
        } else {
            dateTimeText.setText(response.getPaymentDate());
        }



        /*userText.setText("");*/
        if (response.getPartnerName().equals("")||response.getPartnerName().equals(null)) {
            nameText.setText("NA");
        } else {
            nameText.setText(response.getPartnerName());
        }


        if (response.getPartnerId().equals("")||response.getPartnerId().equals(null)) {
            idText.setText("NA");
        } else {
            idText.setText(response.getPartnerId() + "");
        }


        if (response.getReceipt_no().equals("")||response.getReceipt_no().equals(null)) {
            receiptNo.setText("NA");
        } else {
            receiptNo.setText(response.getReceipt_no() + "");
        }


        if (response.getFlat_name().equals("")||response.getFlat_name().equals(null)) {
            flat.setText("NA");
        } else {
            flat.setText(response.getFlat_name() + "");
        }


        if (response.getBuilding_name().equals("")||response.getBuilding_name().equals(null)) {
            building.setText("NA");
        } else {
            building.setText(response.getBuilding_name() + "");
        }


        if (response.getContract().equals("")||response.getContract().equals(null)) {
            contract.setText("NA");
        } else {
            contract.setText(response.getContract() + "");
        }


        /*if (response.getMobile().equals("")||response.getMobile().equals(null)) {
            mobile.setText("NA");
        } else {
            mobile.setText(response.getMobile() + "");
        }*/


        if (response.getOutstanding_bal().equals("")||response.getOutstanding_bal().equals(null)) {
            outBal.setText("NA");
        } else {
            outBal.setText(format.format(Double.parseDouble(response.getOutstanding_bal())) + "");
        }


        if (response.getAmount().equals("")||response.getAmount().equals(null)) {
            paidAmt.setText("NA");
        } else {
            paidAmt.setText(format.format(response.getAmount()) + "");
        }


        if (response.getRemaining_bal().equals("")||response.getRemaining_bal().equals(null)) {
            remainBal.setText("NA");
        } else {
            remainBal.setText(format.format(Double.parseDouble(response.getRemaining_bal())) + "");
        }


        if (response.getJournalName().equals("")||response.getJournalName().equals(null)) {
            payTypeTextPay.setText("NA");
        } else {
            payTypeTextPay.setText(response.getJournalName() + "");
        }





        if (!response.getChequeNo().equals("")) {

            chequeNoLay.setVisibility(View.VISIBLE);
            chequeNoText.setText(response.getChequeNo() + "");

        }

        if (!response.getChequeDate().equals("")) {

            chequeDateLay.setVisibility(View.VISIBLE);
            chequeDateText.setText(response.getChequeDate() + "");

        }

        if (!response.getCard_ref().equals("")) {

            cardrefLay.setVisibility(View.VISIBLE);
            cardrefText.setText(response.getCard_ref() + "");

        }

    }


    private void innitView() {

        popUpCloseButton = findViewById(R.id.closePopupButtonPayHis);
        dateTimeText = findViewById(R.id.dateTimeTextPayHis);
        /*userText=findViewById(R.id.userTextPayHis);*/
        nameText = findViewById(R.id.nameTextPayHis);
        idText = findViewById(R.id.customerIdTextPayHis);
        receiptNo = findViewById(R.id.receiptNumberTextPayHis);
        flat = findViewById(R.id.flatTextPayHis);
        building = findViewById(R.id.buildingTextPayHis);
        contract = findViewById(R.id.contractTextPayHis);
        /*mobile = findViewById(R.id.mobileTextPayHis);*/
        outBal = findViewById(R.id.outBalTextPayHis);
        paidAmt = findViewById(R.id.paidBalTextPayHis);
        remainBal = findViewById(R.id.remainBalTextPayHis);
        payTypeTextPay = findViewById(R.id.payTypeTextPay);

        print_key = findViewById(R.id.b_koubeiPayHis);
        close_key = findViewById(R.id.b_exitPayHis);


        chequeDateLay = findViewById(R.id.chequeDateLay);
        chequeNoLay = findViewById(R.id.chequeNoLay);
        cardrefLay = findViewById(R.id.cardReflay);


        chequeDateText = findViewById(R.id.ChecqueDateTextPay);
        chequeNoText = findViewById(R.id.ChecqueNoTextPay);
        cardrefText = findViewById(R.id.CardRefTextPay);


        popUpCloseButton.setOnClickListener(this);
        print_key.setOnClickListener(this);
        close_key.setOnClickListener(this);

        b_koubeiPay1=findViewById(R.id.b_koubeiPay11);

        chequeNoLay.setVisibility(View.GONE);
        chequeDateLay.setVisibility(View.GONE);
        cardrefLay.setVisibility(View.GONE);


        b_koubeiPay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                printBill(1);

            }
        });

    }

    @Override
    public void onClick(View v) {

        if (ButtonDelayUtils.isFastDoubleClick()) {
            return;
        }


        int PRINTER_NORMAL = 0;
        switch (v.getId()) {

            case R.id.b_koubeiPayHis: printBill(0);
                break;

            case R.id.b_exitPayHis:
                finish();
                Log.e("id", "exit");
                break;

            case R.id.closePopupButtonPayHis:
                Log.e("id", "x");
                finish();
                break;

            default:
                break;
        }
    }


    public void initPrinter() {

        if (woyouService == null) {
            return;
        }

        try {
            woyouService.printerInit(null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public void connectPrinterService() {
        Intent intent = new Intent();
        intent.setPackage(SERVICE＿PACKAGE);
        intent.setAction(SERVICE＿ACTION);
        PrintPaymentHistory.this.getApplicationContext().startService(intent);
        PrintPaymentHistory.this.getApplicationContext().bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }


    private ServiceConnection connService = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };



    private Bitmap scaleImage(Bitmap bitmap1) {
        int width = bitmap1.getWidth();
        int height = bitmap1.getHeight();
        // 设置想要的大小
        int newWidth = (width/8+1)*6;
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, 1);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,
                true);
        return newbm;
    }


    public void printBill(final int types) {
        ThreadPoolManager.getInstance().executeTask(new Runnable() {

            @Override
            public void run() {
//                Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.happyway_printd);
                Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.petro_logo_print);

                try {

                    print_key.setClickable(false);

                    woyouService.printBitmap(scaleImage(mBitmap), null);

                    woyouService.lineWrap(1,null);

                    woyouService.printTextWithFont("         Al Qusais, Dubai,\n", "ST", 24, null);
                    woyouService.printTextWithFont("     United Arab Emirates\n", "ST", 24, null);

                    woyouService.printTextWithFont("     0502573388 / 042673200\n", "ST", 24, null);
                    woyouService.printTextWithFont("       utility@petrosafe.ae\n", "ST", 24, null);

                    woyouService.printTextWithFont("     TRN : 100065418400003 \n", "ST", 24, null);

                    woyouService.printTextWithFont("         " + "Payment Receipt\n********************************\n", "ST", 24, null);


                    if (response.getReceipt_no().equals("") || response.getReceipt_no().equals(null)) {
                        woyouService.printTextWithFont("Receipt number : " + "NA"+"\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Receipt number : " + response.getReceipt_no()+"\n", "ST", 24, null);
                    }


                    String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                    String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
//                    mIPosPrinterService.printSpecifiedTypeText("Date  :" + currentDate + " " + currentTime, "ST", 24, callback);


                    if (response.getPaymentDate().equals("") || response.getPaymentDate().equals(null)) {
                        woyouService.printTextWithFont("Date  :" + "NA"+"\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Date  :" + response.getPaymentDate()+"\n", "ST", 24, null);
                    }


                    /* mIPosPrinterService.printSpecifiedTypeText("User  :" + "" + "\n", "ST", 24, callback);*/
                    woyouService.printTextWithFont("********************************\n", "ST", 24, null);


                    if (response.getPartnerName().equals("") || response.getPartnerName().equals(null)) {
                        woyouService.printTextWithFont("Name             : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Name             : " + response.getPartnerName() + "\n", "ST", 24, null);
                    }


                    if (response.getPartnerId().toString().equals("") || response.getPartnerId().equals(null)) {
                        woyouService.printTextWithFont("Id               : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Id               : " + response.getPartnerId() + "\n", "ST", 24, null);
                    }


                    if (response.getFlat_name().equals("") || response.getFlat_name().equals(null)) {
                        woyouService.printTextWithFont("Flat No          : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Flat No          : " + response.getFlat_name() + "\n", "ST", 24, null);
                    }


                    if (response.getBuilding_name().equals("") || response.getBuilding_name().equals(null)) {
                        woyouService.printTextWithFont("Building         : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Building         : " + response.getBuilding_name() + "\n", "ST", 24, null);
                    }


                    if (response.getContract().equals("") || response.getContract().equals(null)) {
                        woyouService.printTextWithFont("Contract No      : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Contract No      : " + response.getContract() + "\n", "ST", 24, null);
                    }


                 /*
                    if (response.getMobile().equals("") || response.getMobile().equals(null)) {
                        woyouService.printTextWithFont("Mobile No        : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Mobile No        : " + response.getMobile() + "\n", "ST", 24, null);
                    }
                 */

                    if (response.getOutstanding_bal().equals("") || response.getOutstanding_bal().equals(null)) {
                        woyouService.printTextWithFont("Out.S balance    : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Out.S balance    : " + format.format(Double.parseDouble(response.getOutstanding_bal())) + "\n", "ST", 24, null);
                    }


                    if (response.getAmount().equals("") || response.getAmount().equals(null)) {
                        woyouService.printTextWithFont("Paid Amount      : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Paid Amount      : " + format.format(response.getAmount()) + "\n", "ST", 24, null);
                    }


                    if (response.getRemaining_bal().equals("") || response.getRemaining_bal().equals(null)) {
                        woyouService.printTextWithFont("Remaining Balance: " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Remaining Balance: " + format.format(Double.parseDouble(response.getRemaining_bal())) + "\n", "ST", 24, null);
                    }


                    if (response.getJournalName().equals("") || response.getJournalName().equals(null)) {
                        woyouService.printTextWithFont("Payment Type     : " + "NA" + "\n", "ST", 24, null);
                    } else {
                        woyouService.printTextWithFont("Payment Type     : " + response.getJournalName() + "\n", "ST", 24, null);
                    }


                    if (!response.getChequeNo().equals("")) {

                        woyouService.printTextWithFont("Cheque No        : " + response.getChequeNo() + "\n", "ST", 24, null);
                    }

                    if (!response.getChequeDate().equals("")) {

                        woyouService.printTextWithFont("Cheque Date      : " + response.getChequeDate() + "\n", "ST", 24, null);
                    }

                    if (!response.getCard_ref().equals("")) {

                        woyouService.printTextWithFont("Card Ref No      : " + response.getCard_ref() + "\n", "ST", 24, null);
                    }


                    if(types==0){

                        woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                        woyouService.printTextWithFont("Merchant Signature" + "\n", "ST", 24, null);
                        woyouService.lineWrap(3, null);

                    }


                    if(types==1){
                        woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                        woyouService.printTextWithFont("Customer Signature" + "\n", "ST", 24, null);
                        woyouService.lineWrap(3, null);

                    }


                    woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                    woyouService.printTextWithFont("          Thank you             ", "ST", 24, null);


                    woyouService.lineWrap(3,null);


                    print_key.setClickable(true);


                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}

