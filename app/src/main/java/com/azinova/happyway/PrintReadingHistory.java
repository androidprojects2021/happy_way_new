package com.azinova.happyway;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.azinova.happyway.model.output.ReadingHistoryModel.ResponseItem;
import com.azinova.happyway.utils.ButtonDelayUtils;
import com.azinova.happyway.utils.HandlerUtils;
import com.azinova.happyway.utils.ThreadPoolManager;
import com.iposprinter.iposprinterservice.IPosPrinterCallback;
import com.iposprinter.iposprinterservice.IPosPrinterService;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import woyou.aidlservice.jiuiv5.IWoyouService;


public class PrintReadingHistory extends Activity implements View.OnClickListener {

    private static final String TAG = "PrintReadingHistory";
    private static final String VERSION = "V1.1.0";

    private static final String SERVICE＿PACKAGE = "woyou.aidlservice.jiuiv5";
    private static final String SERVICE＿ACTION = "woyou.aidlservice.jiuiv5.IWoyouService";
    private IWoyouService woyouService;

    DecimalFormat format = new DecimalFormat("##.00");

    private Button print_key,b_koubeiReadHis1;
    private Button close_key;
    private ImageView popUpCloseButton;
    private TextView customerName, customerId, flatNo, buildingNo, contractNO,receiptNoText, lastMeter, meter, usage, unitPrice, fixedCharge, total, vatAmt, netAmt, outAmt,prevBal,mobileNO,dateTimeText,userText,lastReadTextReadHisDate,meter_Factor_TextReadHis;

    private ResponseItem response;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_reading_history);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        response = getIntent().getParcelableExtra("print");
        assert response != null;

        connectPrinterService();
        initPrinter();

        innitView();
        populate();
    }

    private void populate() {




        if(response.getInvoice_number().equals("")||response.getInvoice_number().equals(null)) {
            receiptNoText.setText("NA" + "");
        }else {receiptNoText.setText(response.getInvoice_number() + "");}


        if(response.getMobile().equals("")||response.getMobile().equals(null)) {
            mobileNO.setText("NA" + "");
        }else {mobileNO.setText(response.getMobile() + "");}

        if (response.getDate().equals("")||response.getDate().equals(null))
        { dateTimeText.setText("NA");}
        else {dateTimeText.setText(response.getDate());}


        if (response.getUser_name().equals("")||response.getUser_name().equals(null))
        {userText.setText("NA");}
        else {userText.setText(response.getUser_name());}


        if (response.getPartnerName().equals("")||response.getPartnerName().equals(null)) {
            customerName.setText("NA");
        } else {
            customerName.setText(response.getPartnerName());
        }


        if (response.getPartnerId().equals("")||response.getPartnerId().equals(null)) {
            customerId.setText("NA");
        } else {
            customerId.setText(response.getPartnerId() + "");
        }


        if (response.getFlatName().equals("")||response.getFlatName().equals(null)) {
            flatNo.setText("NA");
        } else {
            flatNo.setText(response.getFlatName());
        }


        if (response.getBuildingName().equals("")||response.getBuildingName().equals(null)) {
            buildingNo.setText("NA");
        } else {
            buildingNo.setText(response.getBuildingName());
        }


        if (response.getContract_ref().equals("")||response.getContract_ref().equals(null)) {
            contractNO.setText("NA");
        } else {
            contractNO.setText(response.getContract_ref());
        }


        if (response.getLastMeterReading().equals("")||response.getLastMeterReading().equals(null)) {
            lastMeter.setText("NA");
        } else {
            lastMeter.setText(format.format(response.getLastMeterReading()) + "");
        }


        if (response.getMeterReading().equals("")||response.getMeterReading().equals(null)) {
            meter.setText("NA");
        } else {
            meter.setText(format.format(response.getMeterReading()) + "");
        }


        if (response.getDifferenceReading().equals("")||response.getDifferenceReading().equals(null)) {
            usage.setText("NA");
        } else {
            usage.setText(format.format(response.getDifferenceReading()) + "");
        }


        if (response.getPrice_unit().equals("")||response.getPrice_unit().equals(null)) {
            unitPrice.setText("NA");
        } else {
            unitPrice.setText(response.getPrice_unit() + "");
        }


        if (response.getMonthly_fee().equals("")||response.getMonthly_fee().equals(null)) {
            fixedCharge.setText("NA");
        } else {
            fixedCharge.setText(response.getMonthly_fee() + "");
        }


        if (response.getAmount_untaxed().equals("")||response.getAmount_untaxed().equals(null)) {
            total.setText("NA");
        } else {
            total.setText(format.format(response.getAmount_untaxed()) + "");
        }


        if (response.getAmount_tax().equals("")||response.getAmount_tax().equals(null)) {
            vatAmt.setText("NA");
        } else {
            vatAmt.setText(format.format(response.getAmount_tax()) + "");
        }


        if(response.getLast_reading_date().equals("")) {
            lastReadTextReadHisDate .setText("NA");
        }else {lastReadTextReadHisDate.setText(response.getLast_reading_date() + "");}


        if (response.getAmount_total().equals("")||response.getAmount_total().equals(null)) {
            netAmt.setText("NA");
        } else {
            netAmt.setText(format.format(response.getAmount_total()) + "");
        }


       if (response.getCredit().equals("")||response.getCredit().equals(null)) {
            outAmt.setText("NA");
        } else {
            outAmt.setText(response.getCredit() + "");
        }


        if (response.getCredit().equals("")||response.getCredit().equals(null)) {
            outAmt.setText("NA");
        } else {
            outAmt.setText(response.getCredit() + "");
        }


        if (response.getMeter_factor().equals("")||response.getMeter_factor().equals(null)) {
            meter_Factor_TextReadHis.setText("NA");
        } else {
            meter_Factor_TextReadHis.setText(response.getMeter_factor() + "");
        }



        if (response.getPrevious_credit().equals("")||response.getPrevious_credit().equals(null)) {
            prevBal.setText("NA");
        } else {
            prevBal.setText(response.getPrevious_credit() + "");
        }


    }


    private void innitView() {

        popUpCloseButton = findViewById(R.id.closePopupButtonReadHis);

        customerName = findViewById(R.id.customerNameTextReadHis);
        customerId = findViewById(R.id.customerIdTextReadHis);
        flatNo = findViewById(R.id.flatNoTextReadHis);
        buildingNo = findViewById(R.id.BuildingTextReadHis);
        contractNO = findViewById(R.id.contractTextReadHis);
        lastMeter = findViewById(R.id.lastReadTextReadHis);
        meter = findViewById(R.id.meterTextReadHis);
        usage = findViewById(R.id.usageTextReadHis);
        unitPrice = findViewById(R.id.unitPriceTextReadHis);
        fixedCharge = findViewById(R.id.fixedTextReadHis);
        total = findViewById(R.id.totalTextReadHis);
        vatAmt = findViewById(R.id.vatTextReadHis);
        netAmt = findViewById(R.id.netTextReadHis);
        outAmt = findViewById(R.id.outBalTextReadHis);
        prevBal = findViewById(R.id.prevBalTextReadHis);

        dateTimeText=findViewById(R.id.dateTimeText);
        userText=findViewById(R.id.userText);

        mobileNO=findViewById(R.id.mobileNoText);

        print_key = findViewById(R.id.b_koubeiReadHis);
        close_key = findViewById(R.id.b_exitReadHis);

        popUpCloseButton.setOnClickListener(this);
        print_key.setOnClickListener(this);
        close_key.setOnClickListener(this);
        receiptNoText=findViewById(R.id.receiptNoText);

        b_koubeiReadHis1=findViewById(R.id.b_koubeiReadHis1);

        meter_Factor_TextReadHis=findViewById(R.id.meter_Factor_TextReadHis);

        lastReadTextReadHisDate=findViewById(R.id.lastReadTextReadHisDate);

        b_koubeiReadHis1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                printBill(1);

            }
        });


    }

    @Override
    public void onClick(View v) {

        if (ButtonDelayUtils.isFastDoubleClick()) {
            return;
        }

        int PRINTER_NORMAL = 0;
        switch (v.getId()) {

            case R.id.b_koubeiReadHis: printBill(0);
                break;

            case R.id.b_exitReadHis:
                finish();
                Log.e("id", "exit");
                break;

            case R.id.closePopupButtonReadHis:
                Log.e("id", "x");
                finish();
                break;

            default:
                break;
        }
    }


    public void initPrinter() {

        if (woyouService == null) {
           // Toast.makeText(PrintReadingHistory.this, "Service Has Been Disconnected", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            woyouService.printerInit(null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public void connectPrinterService() {
        Intent intent = new Intent();
        intent.setPackage(SERVICE＿PACKAGE);
        intent.setAction(SERVICE＿ACTION);
        PrintReadingHistory.this.getApplicationContext().startService(intent);
        PrintReadingHistory.this.getApplicationContext().bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }


    private ServiceConnection connService = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };



    private Bitmap scaleImage(Bitmap bitmap1) {
        int width = bitmap1.getWidth();
        int height = bitmap1.getHeight();
        // 设置想要的大小
        int newWidth = (width/8+1)*6;
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, 1);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,
                true);
        return newbm;
    }


    public void printBill(final int types) {
        ThreadPoolManager.getInstance().executeTask(new Runnable() {

            @Override
            public void run() {
//                Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.happyway_printd);
                Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.petro_logo_print);

                try {

                    print_key.setClickable(false);

                    woyouService.printBitmap(scaleImage(mBitmap), null);

                    woyouService.lineWrap(1,null);

                    woyouService.printTextWithFont("         Al Qusais, Dubai,\n", "ST", 24, null);
                    woyouService.printTextWithFont("     United Arab Emirates\n", "ST", 24, null);

                    woyouService.printTextWithFont("     0502573388 / 042673200\n", "ST", 24, null);
                    woyouService.printTextWithFont("       utility@petrosafe.ae\n", "ST", 24, null);

                    woyouService.printTextWithFont("     TRN : 100065418400003 \n", "ST", 24, null);
                    woyouService.printTextWithFont("         " + "Tax Invoice\n********************************\n", "ST", 24, null);



                    if(response.getInvoice_number().equals("")||response.getInvoice_number().equals(null)) {
                        woyouService.printTextWithFont("Invoice No :" + "NA" + "\n", "ST", 24, null);
                    }else {
                        woyouService.printTextWithFont("Invoice No :" + response.getInvoice_number() + "\n", "ST", 24, null);
                    }



                    woyouService.printTextWithFont("Date  :" + response.getDate()+"\n", "ST", 24, null);

                    woyouService.printTextWithFont("User  :" + response.getUser_name()+"" + "\n", "ST", 24, null);

                    woyouService.printTextWithFont("********************************\n", "ST", 24, null);



                    if(response.getPartnerName().equals("")||response.getPartnerName().equals(null)) {
                        woyouService.printTextWithFont("Name             : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Name             : " + response.getPartnerName() + "\n", "ST", 24, null);}





                    if(response.getPartnerId().equals("")||response.getPartnerId().equals(null)) {
                        woyouService.printTextWithFont("Customer Id      : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Customer Id      : " + response.getPartnerId() + "\n", "ST", 24, null);}





                    if(response.getFlatName().equals("")||response.getFlatName().equals(null)) {
                        woyouService.printTextWithFont("Flat No          : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Flat No          : " + response.getFlatName() + "\n", "ST", 24, null);}





                    if(response.getBuildingName().equals("")||response.getBuildingName().equals(null)) {
                        woyouService.printTextWithFont("Building         : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Building         : " + response.getBuildingName() + "\n", "ST", 24, null);}



                    if(response.getContract_ref().equals("")||response.getContract_ref().equals(null)) {
                        woyouService.printTextWithFont("Contract No      : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Contract No      : " + response.getContract_ref() + "\n", "ST", 24, null);}




                 /*   if(response.getMobile().equals("")||response.getMobile().equals(null)) {
                        woyouService.printTextWithFont("Mobile No        : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Mobile No        : " + response.getMobile() + "\n", "ST", 24, null);}

*/

                    if(response.getLastMeterReading().equals("")||response.getLastMeterReading().equals(null)) {
                        woyouService.printTextWithFont("Previous Reading : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Previous Reading : " + format.format(response.getLastMeterReading()) + "\n", "ST", 24, null);}



                    if(response.getLast_reading_date().equals(null)||response.getLast_reading_date().equals("")) {
                        woyouService.printTextWithFont("Pre Reading Date : " + "NA" + "\n", "ST", 24, null);
                    }else{ woyouService.printTextWithFont("Pre Reading Date : " + response.getLast_reading_date() + "\n", "ST", 24, null);}



                    if(response.getMeterReading().equals("")||response.getMeterReading().equals(null)) {
                        woyouService.printTextWithFont("Current Reading  : " + "NA" + "\n", "ST", 24, null);
                    }else { woyouService.printTextWithFont("Current Reading  : " + format.format(response.getMeterReading()) + "\n", "ST", 24, null);}






                    if(response.getDifferenceReading().equals("")||response.getDifferenceReading().equals(null)) {
                        woyouService.printTextWithFont("Usage            : " + "NA" + "\n", "ST", 24, null);
                    } else {woyouService.printTextWithFont("Usage            : " + format.format(response.getDifferenceReading()) + "\n", "ST", 24, null);}





                    if(response.getPrice_unit().equals("")||response.getPrice_unit().equals(null)) {
                        woyouService.printTextWithFont("Unit Price       : " + "NA" + "\n", "ST", 24, null);
                    } else { woyouService.printTextWithFont("Unit Price       : " + format.format(response.getPrice_unit()) + "\n", "ST", 24, null);}


                    if(response.getMeter_factor().equals("")||response.getMeter_factor().equals(null)) {
                        woyouService.printTextWithFont("Meter Factor     : " + "NA" + "\n", "ST", 24, null);
                    } else { woyouService.printTextWithFont("Meter Factor     : " + format.format(response.getMeter_factor()) + "\n", "ST", 24, null);}





                    if(response.getMonthly_fee().equals("")||response.getMonthly_fee().equals(null)) {
                        woyouService.printTextWithFont("Service Charge   : " + "NA" + "\n", "ST", 24, null);
                    } else { woyouService.printTextWithFont("Service Charge   : " + response.getMonthly_fee() + "\n", "ST", 24, null);}




                    if(response.getAmount_untaxed().equals("")||response.getAmount_untaxed().equals(null)) {
                        woyouService.printTextWithFont("Amount           : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Amount           : " + format.format(response.getAmount_untaxed()) + "\n", "ST", 24, null);}





                    if(response.getAmount_tax().equals("")||response.getAmount_tax().equals(null)) {
                        woyouService.printTextWithFont("Vat Amount (5%)  : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Vat Amount (5%)  : " + format.format(response.getAmount_tax()) + "\n", "ST", 24, null);}



                    if(response.getAmount_total().equals("")||response.getAmount_total().equals(null)) {
                        woyouService.printTextWithFont("Net Amount       : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Net Amount       : " + format.format(response.getAmount_total()) + "\n", "ST", 24, null);}



                    if(response.getPrevious_credit().equals("")||response.getPrevious_credit().equals(null)) {
                        woyouService.printTextWithFont("Prev balance     : " + "NA" + "\n", "ST", 24, null);
                    }else {

                        woyouService.printTextWithFont("Prev balance     : " + format.format(response.getPrevious_credit()) + "\n", "ST", 24, null);

                    }


                    if(response.getCredit().equals("")||response.getCredit().equals(null)) {
                        woyouService.printTextWithFont("Out.S balance    : " + "NA" + "\n", "ST", 24, null);
                    }else {

                        woyouService.printTextWithFont("Out.S balance    : " + format.format(response.getCredit()) + "\n", "ST", 24, null);

                    }





                    if(types==0){

                        woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                        woyouService.printTextWithFont("Merchant Signature" + "\n", "ST", 24, null);
                        woyouService.lineWrap(3, null);

                    }


                    if(types==1){

                        woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                        woyouService.printTextWithFont("Customer Signature" + "\n", "ST", 24, null);
                        woyouService.lineWrap(3, null);

                    }




                    woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                    woyouService.printTextWithFont("          Thank you             ", "ST", 24, null);

                    woyouService.lineWrap(3,null);

                    print_key.setClickable(true);


                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}


