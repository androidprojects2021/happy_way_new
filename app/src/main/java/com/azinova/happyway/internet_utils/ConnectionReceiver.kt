package com.azinova.happyway.internet_utils

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.SearchEventModel


class ConnectionReceiver : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {


        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if(isConnected){

            Toast.makeText(context,"Network Connected",Toast.LENGTH_LONG).show()

                MessageEventBus.publish(SearchEventModel(1,"net"))

        }else{


            Toast.makeText(context,"Not Network Connected",Toast.LENGTH_LONG).show()


               MessageEventBus.publish(SearchEventModel(0,"net"))


        }

    }


}