package com.azinova.happyway.internet_utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object ConnectionCheck{

    private var cm : ConnectivityManager? =null
    private var activeNetwork : NetworkInfo? = null
    private var isConnected :Boolean = false


    fun startCheck(context: Context)
    {
        cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        activeNetwork = cm!!.activeNetworkInfo
        isConnected = activeNetwork?.isConnectedOrConnecting == true
    }

    fun getConnectStatus():Boolean{
        return isConnected
    }

}