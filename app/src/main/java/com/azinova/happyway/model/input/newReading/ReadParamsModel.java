package com.azinova.happyway.model.input.newReading;

public class ReadParamsModel {
    private int user_id, customer_id;
    private double meter_reading;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public double getMeter_reading() {
        return meter_reading;
    }

    public void setMeter_reading(double meter_reading) {
        this.meter_reading = meter_reading;
    }
}
