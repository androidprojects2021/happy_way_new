package com.azinova.happyway.model.input.PaymentInput;

public class PaymentInputParam {
    PaymentInputModel params=new PaymentInputModel();

    public PaymentInputModel getParams() {
        return params;
    }

    public void setParams(PaymentInputModel params) {
        this.params = params;
    }
}
