package com.azinova.happyway.model.output.PaymentTypeModel

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class ResponsePayPrint(

        @field:SerializedName("payment_date")
        val date: String? = null,

        @field:SerializedName("building_code")
        val building_code: String? = null,

        @field:SerializedName("bank_name")
        val bank_name: String? = null,

        @field:SerializedName("payment_method")
        val payment_method: String? = null,

        @field:SerializedName("cheque_date")
        val cheque_date: String? = null,

        @field:SerializedName("cheque_no")
        val cheque_no: String? = null,

        @field:SerializedName("building_name")
        val building_name: String? = null,

        @field:SerializedName("partner_name")
        val partner_name: String? = null,

        @field:SerializedName("card_ref")
        val card_ref: String? = null,

        @field:SerializedName("notes")
        val notes: String? = null,

        @field:SerializedName("bank_id")
        val bank_id: String? = null,

        @field:SerializedName("contract")
        val contract: String? = null,

        @field:SerializedName("flat_id")
        val flat_id: String? = null,

        @field:SerializedName("amount")
        val amount: String? = null,

        @field:SerializedName("remaining_bal")
        val remaining_bal: String? = null,

        @field:SerializedName("outstanding_bal")
        val outstanding_bal: String? = null,


        @field:SerializedName("mobile")
        val mobile: String? = null,

        @field:SerializedName("payment_id")
        val payment_id: String? = null,

        @field:SerializedName("receipt_no")
        val receipt_no: String? = null,

        @field:SerializedName("flat_name")
        val flat_name: String? = null,

        @field:SerializedName("partner_id")
        val partner_id: String? = null

) : Parcelable
