package com.azinova.happyway.model.output.PaymentHistory

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class PaymentHistoryResponse(

	@field:SerializedName("result")
	val result: Result? = null,

	@field:SerializedName("id")
	val id: Any? = null,

	@field:SerializedName("jsonrpc")
	val jsonrpc: String? = null
)