package com.azinova.happyway.model.output.ReadingResponse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class Response(

        @field:SerializedName("building_id")
        val buildingId: Int? = null,

        @field:SerializedName("partner_id")
        val partnerId: Int? = null,

        @field:SerializedName("flat_id")
        val flatId: Int? = null,

        @field:SerializedName("user_id")
        val userId: Int? = null,

        @field:SerializedName("difference_reading")
        val differenceReading: Double? = null,

        @field:SerializedName("reading_id")
        val readingId: Int? = null,

        @field:SerializedName("last_meter_reading")
        val lastMeterReading: Double? = null,

       /* @field:SerializedName("meter_reading")
        val meterReading: Int? = null,*/


        @field:SerializedName("meter_reading")
        val meterReading: Double? = null,


        @field:SerializedName("monthly_fee")
        val monthlyFee: Double? = null,

        @field:SerializedName("amount_total")
        val amountTotal: Double? = null,

        @field:SerializedName("price_unit")
        val priceUnit: Double? = null,

        @field:SerializedName("amount_tax")
        val amountTax: Double? = null,

        @field:SerializedName("amount_untaxed")
        val amountUntaxed: Double? = null,

        @field:SerializedName("meter_factor")
        val meter_factor: Double? = null



) : Parcelable

