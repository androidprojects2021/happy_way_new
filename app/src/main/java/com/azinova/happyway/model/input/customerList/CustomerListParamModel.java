package com.azinova.happyway.model.input.customerList;

public class CustomerListParamModel {
    CustomerListInput params = new CustomerListInput();

    public CustomerListInput getParams() {
        return params;
    }

    public void setParams(CustomerListInput customerListInput) {
        this.params = customerListInput;
    }
}
