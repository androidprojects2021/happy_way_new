package com.azinova.happyway.model.input.newReading;

public class NewReadingParamModelUpdated {

    ReadParamsNewReadUpdated params=new ReadParamsNewReadUpdated();

    public ReadParamsNewReadUpdated getParams() {
        return params;
    }

    public void setParams(ReadParamsNewReadUpdated params) {
        this.params = params;
    }
}
