package com.azinova.happyway.model.output.Login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Result(

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("building_list")
	val buildingList: List<BuildingListItem?>? = null
)