package com.azinova.happyway.model.input.contract

import com.google.gson.annotations.SerializedName

data class InputGetBuildings(

	@field:SerializedName("params")
	val params: Params? = null
)

data class Params(

	@field:SerializedName("code")
	val code: Int? = null
)
