package com.azinova.happyway.model.input.PaymentHistoryInput;

public class PaymentHistoryInputModel {
    private int user_id;
    private String date;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
