package com.azinova.happyway.model.output.contract

import com.google.gson.annotations.SerializedName

data class ResponseCreateContract(

	@field:SerializedName("result")
	val result: Result1,

	@field:SerializedName("id")
	val id: Any,

	@field:SerializedName("jsonrpc")
	val jsonrpc: String
)

data class Response(

		@field:SerializedName("first_reading")
		val first_reading: String,

		@field:SerializedName("deposit_amount")
		val deposit_amount: String,

		@field:SerializedName("contract_code")
		val contract_code: String,

		@field:SerializedName("customer_id")
		val customer_id: String,

		@field:SerializedName("connection_charges")
		val connection_charges: String	,

		@field:SerializedName("disconnection_charges")
		val disconnection_charges: String

)

data class Result1(

	@field:SerializedName("response")
	val response: Response,

	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("customer_id")
	val customerId: String,

	@field:SerializedName("status")
	val status: String
)
