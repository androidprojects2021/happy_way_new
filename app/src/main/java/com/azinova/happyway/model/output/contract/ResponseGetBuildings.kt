package com.azinova.happyway.model.output.contract

import com.google.gson.annotations.SerializedName

data class ResponseGetBuildings(

	@field:SerializedName("result")
	val result: Result? = null,

	@field:SerializedName("id")
	val id: Any? = null,

	@field:SerializedName("jsonrpc")
	val jsonrpc: String? = null
)

data class ResponseItem(

	@field:SerializedName("flat_id")
	val flatId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null
)

data class Result(

	@field:SerializedName("response")
	val response: ArrayList<ResponseItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
