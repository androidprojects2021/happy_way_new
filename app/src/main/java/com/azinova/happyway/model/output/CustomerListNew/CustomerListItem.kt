package com.azinova.happyway.model.output.CustomerListNew

import android.os.Parcelable
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Generated("com.robohorse.robopojogenerator")
@Parcelize

data class CustomerListItem(

	@field:SerializedName("previous_reading")
	val previousReading: Double? = null,

	@field:SerializedName("notes")
	val notes: String? = null,

	@field:SerializedName("contract_date")
	val contractDate: String? = null,

	@field:SerializedName("street")
	val street: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("contract_ref")
	val contractRef: String? = null,

	@field:SerializedName("street2")
	val street2: String? = null,

	@field:SerializedName("previous_reading_date")
	val previousReadingDate: String? = null,

	@field:SerializedName("meter_number")
	val meterNumber: String? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("flat_id")
	val flat_id: String? = null,

	@field:SerializedName("customer_id")
	val customerId: Int? = null,

	@field:SerializedName("pending_amount")
	val pendingAmount: Double? = null,

	@field:SerializedName("flat_name")
   val flat_name: String? = null,

	@field:SerializedName("building_name")
	val building_name: String? = null,

	@field:SerializedName("last_invoiced_amount")
	val last_invoiced_amount: Double? = null,

	@field:SerializedName("code")
	val code: String? = null,

		@field:SerializedName("whatsap")
		val whatsap: String? = "",

		@field:SerializedName("email")
		val email: String? = ""


): Parcelable