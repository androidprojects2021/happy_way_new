package com.azinova.happyway.model.output.Login

import android.os.Parcelable
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class BuildingListItem(

	@field:SerializedName("building_id")
	val buildingId: Int? = null,

	@field:SerializedName("building_name")
	val buildingName: String? = null
):Parcelable