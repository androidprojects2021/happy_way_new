package com.azinova.happyway.model.output.upliad_image

import com.google.gson.annotations.SerializedName

data class ResponseImgUploads(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
