package com.azinova.happyway.model.input.newReading;

public class ReadParamsNewReadUpdated {

    private int user_id, customer_id , reading_id;
    private double meter_reading;
    private String date;
    private String final_invoice;

    public String getFinalInvoice() {
        return final_invoice;
    }

    public void setFinalInvoice(String final_invoice) {
        this.final_invoice = final_invoice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getReading_id() {
        return reading_id;
    }

    public void setReading_id(int reading_id) {
        this.reading_id = reading_id;
    }

    public double getMeter_reading() {
        return meter_reading;
    }

    public void setMeter_reading(double meter_reading) {
        this.meter_reading = meter_reading;
    }
}
