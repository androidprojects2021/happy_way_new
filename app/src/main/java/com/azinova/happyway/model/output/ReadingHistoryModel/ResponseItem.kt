package com.azinova.happyway.model.output.ReadingHistoryModel

import android.os.Parcelable
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class ResponseItem(

	@field:SerializedName("building_id")
	val buildingId: Int? = null,

	@field:SerializedName("building_name")
	val buildingName: String? = null,

	@field:SerializedName("partner_id")
	val partnerId: Int? = null,

	@field:SerializedName("flat_id")
	val flatId: Int? = null,

	@field:SerializedName("partner_name")
	val partnerName: String? = null,

	@field:SerializedName("difference_reading")
	val differenceReading: Double? = null,

	@field:SerializedName("flat_name")
	val flatName: String? = null,

	@field:SerializedName("last_meter_reading")
	val lastMeterReading: Double? = null,

	@field:SerializedName("last_reading_date")
	val last_reading_date: String? = null,

	@field:SerializedName("meter_reading")
	val meterReading: Double? = null,

	@field:SerializedName("contract_ref")
	val contract_ref: String? = null,

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("user_name")
	val user_name: String? = null,

	@field:SerializedName("building_code")
	val building_code: String? = null,

	@field:SerializedName("monthly_fee")
	val monthly_fee: Double? = null,

	@field:SerializedName("price_unit")
	val price_unit: Double? = null,

	@field:SerializedName("amount_tax")
	val amount_tax: Double? = null,

	@field:SerializedName("amount_untaxed")
	val amount_untaxed: Double? = null,

	@field:SerializedName("amount_total")
	val amount_total: Double? = null,

	@field:SerializedName("outstanding_bal")
	val outstanding_bal: Double? = null,

	@field:SerializedName("credit")
	val credit: Double? = null,

	@field:SerializedName("previous_credit")
	val previous_credit: Double? = null,

	@field:SerializedName("invoice_number")
	val invoice_number: String? = null,

	@field:SerializedName("meter_factor")
	val meter_factor: Double? = null


) : Parcelable

