package com.azinova.happyway.model.output.PaymentHistory

import android.os.Parcelable
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class ResponseItem(

	@field:SerializedName("journal_id")
	val journalId: Int? = null,

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("notes")
	val notes: String? = null,

	@field:SerializedName("partner_id")
	val partnerId: Int? = null,

	@field:SerializedName("cheque_no")
	val chequeNo: String? = null,

	@field:SerializedName("partner_name")
	val partnerName: String? = null,

	@field:SerializedName("journal_name")
	val journalName: String? = null,

	@field:SerializedName("cheque_date")
	val chequeDate: String? = null,

	@field:SerializedName("communication")
	val communication: String? = null,

	@field:SerializedName("payment_date")
	val paymentDate: String? = null,

	@field:SerializedName("building_code")
	val building_code: String? = null,

	@field:SerializedName("building_name")
	val building_name: String? = null,

	@field:SerializedName("flat_id")
	val flat_id: String? = null,

	@field:SerializedName("bank_name")
	val bank_name: String? = null,

	@field:SerializedName("bank_id")
	val bank_id: Int? = null,

	@field:SerializedName("contract")
	val contract: String? = null,

	@field:SerializedName("card_ref")
	val card_ref: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("outstanding_bal")
	val outstanding_bal: String? = null,

	@field:SerializedName("receipt_no")
	val receipt_no: String? = null,

	@field:SerializedName("flat_name")
	val flat_name: String? = null,

	@field:SerializedName("paid_amount")
	val paid_amount: String? = null,

	@field:SerializedName("remaining_bal")
    val remaining_bal: String? = null,

	@field:SerializedName("user_name")
    val user_name: String? = null



) : Parcelable

