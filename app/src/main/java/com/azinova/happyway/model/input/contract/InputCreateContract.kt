package com.azinova.happyway.model.input.contract

import com.google.gson.annotations.SerializedName

data class InputCreateContract(

	@field:SerializedName("params")
	val params: Params1? = null
)

data class Params1(

	@field:SerializedName("customer_id")
	val customerId: Int? = 0,

	@field:SerializedName("building_id")
	val buildingId: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("meter_id")
	val meterId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("flat_name")
	val flatName: String? = null,

	@field:SerializedName("last_meter_reading")
	val lastMeterReading: String? = null,

	@field:SerializedName("user_id")
	val user_id: Int? = null,

	@field:SerializedName("bedroom")
	val bedroom: String? = null	,

	@field:SerializedName("disconnection")
	val disconnect: String? = null,

	@field:SerializedName("whatsapp")
	val whatsapp: String? = null
)
