package com.azinova.happyway.model.output.Report

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class PaymentSplitupItem(

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("method")
	val method: String? = null
)