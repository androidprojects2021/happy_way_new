package com.azinova.happyway.model.output.SavePayment

import com.azinova.happyway.model.output.PaymentTypeModel.ResponsePayPrint
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Result(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("response")
	val response: ResponsePayPrint? = null


)
