package com.azinova.happyway.model.output.CustomerListNew

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Result(

	@field:SerializedName("customer_list")
	val customerList: List<CustomerListItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)