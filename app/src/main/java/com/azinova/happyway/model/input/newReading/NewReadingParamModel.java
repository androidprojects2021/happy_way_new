package com.azinova.happyway.model.input.newReading;

public class NewReadingParamModel {

    ReadParamsModel params=new ReadParamsModel();

    public ReadParamsModel getParams() {
        return params;
    }

    public void setParams(ReadParamsModel params) {
        this.params = params;
    }

}
