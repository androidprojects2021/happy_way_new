package com.azinova.happyway.model.input.customer_pay

import com.google.gson.annotations.SerializedName

data class InputSingleCustomer(

	@field:SerializedName("params")
	val params: Params? = null
)

data class Params(

	@field:SerializedName("building_id")
	val buildingId: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("customer_id")
	val customerId: Int? = null
)
