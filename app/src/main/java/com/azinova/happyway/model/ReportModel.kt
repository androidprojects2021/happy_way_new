package com.azinova.happyway.model

data class ReportModel (var totalInvoice:String,var totalPayment:String,var payTypes:ArrayList<Map<String,String>>)