package com.azinova.happyway.model.output.InvoiceResponse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class Response(

        @field:SerializedName("date")
        val date: String? = null,

        @field:SerializedName("monthly_fee")
        val monthlyFee: Double? = null,

        @field:SerializedName("partner_name")
        val partnerName: String? = null,

        @field:SerializedName("user_name")
        val userName: String? = null,

        @field:SerializedName("amount_total")
        val amountTotal: Double? = null,

        @field:SerializedName("difference_reading")
        val differenceReading: Double? = null,


        @field:SerializedName("price_unit")
        val priceUnit: Double? = null,

        @field:SerializedName("partner_id")
        val partnerId: Int? = null,


        @field:SerializedName("amount_tax")
        val amountTax: Double? = null,

        @field:SerializedName("amount_untaxed")
        val amountUntaxed: Double? = null,


        @field:SerializedName("user_id")
        val userId: Int? = null,

        @field:SerializedName("invoice_number")
        val invoiceNumber: String? = null,

        @field:SerializedName("last_meter_reading")
        val lastMeterReading: Double? = null,


        @field:SerializedName("last_reading_date")
        val last_reading_date: String? = null,


        /*@field:SerializedName("meter_reading")
        val meterReading: Int? = null,*/


        @field:SerializedName("meter_reading")
        val meterReading: Double? = null,

        @field:SerializedName("flat_name")
        val flat_name: String? = null,

        @field:SerializedName("contract_ref")
        val contract_ref: String? = null,

        @field:SerializedName("building_name")
        val building_name: String? = null,

        @field:SerializedName("mobile")
        val mobile: String? = null,

        @field:SerializedName("credit")
        val credit: Double? = null,

        @field:SerializedName("flat_id")
        val flat_id: Int? = null,

        @field:SerializedName("building_id")
        val building_id: Int? = null ,

        @field:SerializedName("prevBal")
        var prevBal: Double? = null ,

        @field:SerializedName("meter_factor")
        var meter_factor: Double? = null ,

        @field:SerializedName("previous_credit")
        var previous_credit: Double? = null,

        @field:SerializedName("lastBill")
        var lastBill: Boolean? = false



) : Parcelable
