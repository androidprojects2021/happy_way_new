package com.azinova.happyway.model.input.customerList;

public class CustomerListInput {
    int user_id;
    int building_id;

    public int getUserId() {
        return user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getBuildingId() {
        return building_id;
    }

    public void setBuildingId(int building_id) {
        this.building_id = building_id;
    }


}
