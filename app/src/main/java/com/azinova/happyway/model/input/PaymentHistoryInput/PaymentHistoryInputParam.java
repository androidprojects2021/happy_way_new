package com.azinova.happyway.model.input.PaymentHistoryInput;

public class PaymentHistoryInputParam {
    PaymentHistoryInputModel params=new PaymentHistoryInputModel();

    public PaymentHistoryInputModel getParam() {
        return params;
    }

    public void setParam(PaymentHistoryInputModel params) {
        this.params = params;
    }
}
