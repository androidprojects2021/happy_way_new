package com.azinova.happyway.model.output.Report

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Response(

	@field:SerializedName("total_paid")
	val totalPaid: Double? = null,

	@field:SerializedName("total_invoiced")
	val totalInvoiced: Double? = null,

	@field:SerializedName("payment_splitup")
	val paymentSplitup: List<PaymentSplitupItem?>? = null
)