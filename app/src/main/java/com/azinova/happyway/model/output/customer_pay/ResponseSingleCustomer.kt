package com.azinova.happyway.model.output.customer_pay

import com.google.gson.annotations.SerializedName

data class ResponseSingleCustomer(

	@field:SerializedName("result")
	val result: Result? = null,

	@field:SerializedName("id")
	val id: Any? = null,

	@field:SerializedName("jsonrpc")
	val jsonrpc: String? = null
)

data class CustomerListItem(

	@field:SerializedName("building_id")
	val buildingId: Int? = null,

	@field:SerializedName("building_name")
	val buildingName: String? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("notes")
	val notes: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("contract_ref")
	val contractRef: String? = null,

	@field:SerializedName("previous_reading_date")
	val previousReadingDate: String? = null,

	@field:SerializedName("meter_number")
	val meterNumber: String? = null,

	@field:SerializedName("flat_name")
	val flatName: String? = null,

	@field:SerializedName("pending_amount")
	val pendingAmount: Double? = null,

	@field:SerializedName("previous_reading")
	val previousReading: Double? = null,

	@field:SerializedName("last_invoiced_amount")
	val lastInvoicedAmount: Double? = null,

	@field:SerializedName("contract_date")
	val contractDate: String? = null,

	@field:SerializedName("flat_id")
	val flatId: Int? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("street")
	val street: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("street2")
	val street2: String? = null,

	@field:SerializedName("customer_id")
	val customerId: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class Result(

	@field:SerializedName("customer_list")
	val customerList: List<CustomerListItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)
