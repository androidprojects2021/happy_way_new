package com.azinova.happyway.base;

import android.view.View;

/**
 * Created by Akhil on 23-09-2019.
 */

public interface OnItemClickListener {

    void onItemClick(View view, int position);

    void onLongItemClick(View view, int position);

    void onButtonClick(View view, int position);

}
