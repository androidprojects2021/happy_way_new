package com.azinova.happyway.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Akhil on 23-09-19.
 */
public class AppPreference {

    private Context context;
    private static final String KEY_LOGIN = "KEY_LOGIN";
    private static final String KEY_VERSION_NUMBER = "KEY_VERSION_NUMBER";
    private static final String KEY_USER_ID = "USER_ID";
    private static final String KEY_BUILDING = "BUILDINGS";


    public AppPreference(Context context) {

        this.context = context;
    }

    public void updateContext(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MDSSALE", context.getApplicationContext().MODE_PRIVATE);
        return pref;
    }

    public boolean isLogin() {
        return getSharedPreferences().getBoolean(KEY_LOGIN, false);
    }

    public void setLoginStatus(boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(KEY_LOGIN, status);
        editor.apply();
        editor.commit();
    }

    public void setVersion(int version) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(KEY_VERSION_NUMBER, version);
        editor.apply();
        editor.commit();
    }


    public int getVersion() {

        return getSharedPreferences().getInt(KEY_VERSION_NUMBER, -1);
    }


    public void setUserId(int data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(KEY_USER_ID, data);
        editor.apply();
        editor.commit();


    }

    public int getUserId() {
        return getSharedPreferences().getInt(KEY_USER_ID, 0);

    }

    public void setBuildings(String data) {

        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(KEY_BUILDING, data);
        editor.apply();
        editor.commit();
    }
    public String getBuildings(){
        return getSharedPreferences().getString(KEY_BUILDING, "NUll");
    }
}
