package com.azinova.happyway.utils;

import android.bluetooth.BluetoothDevice;
import android.content.Context;

/**
 * Created by Akhil on 23-09-2019.
 */

public class AppPreferenceManager {


    Context context;

    public AppPreferenceManager(Context context) {

        this.context = context;
    }

    public void updateContext(Context context) {
        this.context = context;
    }


    public boolean getLoginStatus() {
        return ObjectFactory.getInstance(context).getAppPreference().isLogin();
    }

    public void setLoginStatus(boolean data) {
        ObjectFactory.getInstance(context).getAppPreference().setLoginStatus(data);
    }
    public void setUserId(int data) {
        ObjectFactory.getInstance(context).getAppPreference().setUserId(data);
    }
    public int getUserId() {
        return ObjectFactory.getInstance(context).getAppPreference().getUserId();
    }
    public void setBuildings(String data){
        ObjectFactory.getInstance(context).getAppPreference().setBuildings(data);
    }
    public String getBuildings(){
       return ObjectFactory.getInstance(context).getAppPreference().getBuildings();
    }




}
