package com.azinova.happyway.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;

public class ReadTextFromImage {

    Bitmap bitmap;
    Context context;

    public ReadTextFromImage(Bitmap bitmap, Context context) {
        this.bitmap = bitmap;
        this.context = context;
    }

    public ReadTextFromImage(@NotNull Context context) {
        this.context = context;

    }

    @NotNull
    public String result() {
        TextRecognizer txtRecognizer = new TextRecognizer.Builder(context).build();
        if (!txtRecognizer.isOperational()) {

        } else {
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray items = txtRecognizer.detect(frame);
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < items.size(); i++) {
                TextBlock item = (TextBlock) items.valueAt(i);
                strBuilder.append(item.getValue());
                strBuilder.append("/");
                for (Text line : item.getComponents()) {
                    //extract scanned text lines here
                    Log.v("lines", line.getValue());
                    for (Text element : line.getComponents()) {
                        //extract scanned text words here
                        Log.v("element", element.getValue());
                    }
                }
            }
            try {
                return strBuilder.toString().substring(0, strBuilder.toString().length() - 1);
            } catch (Exception e) {
                Log.e("TAG", "" + e.toString());
                return "insert manually";
            }
        }
        return "null";
    }

    @NotNull
    public Uri getImageUri(Bitmap b) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), b, "Title", null);
        return Uri.parse(path);
    }


}
