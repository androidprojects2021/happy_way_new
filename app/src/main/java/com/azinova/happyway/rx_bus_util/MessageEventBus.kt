package com.azinova.happyway.rx_bus_util

import android.util.Log
import rx.Observable
import rx.subjects.PublishSubject

object MessageEventBus {

    private val publisher=PublishSubject.create<Any>()

    fun publish(event:Any){
        try{
        publisher.onNext(event)}catch (e:Exception){Log.e("Error","MessageEventBus"+e.toString())}

    }

    fun <T> listen (eventType : Class<T>) : Observable<T> = publisher.ofType(eventType)


}