package com.azinova.happyway;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.azinova.happyway.model.output.InvoiceResponse.Response;
import com.azinova.happyway.rx_bus_util.MessageEventBus;
import com.azinova.happyway.rx_bus_util.ReadingreturnEventModel;
import com.azinova.happyway.utils.ButtonDelayUtils;
import com.azinova.happyway.utils.HandlerUtils;
import com.azinova.happyway.utils.ThreadPoolManager;
import com.iposprinter.iposprinterservice.IPosPrinterCallback;
import com.iposprinter.iposprinterservice.IPosPrinterService;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import woyou.aidlservice.jiuiv5.ICallback;
import woyou.aidlservice.jiuiv5.IWoyouService;

public class PrintActivity extends Activity implements OnClickListener {

    private static final String SERVICE＿PACKAGE = "woyou.aidlservice.jiuiv5";
    private static final String SERVICE＿ACTION = "woyou.aidlservice.jiuiv5.IWoyouService";
    private IWoyouService woyouService;

    boolean FINALBILL = false;

    private static final String TAG = "PrintActivity";
    private static final String VERSION = "V1.1.0";

    private Button b_koubei,b_koubei1;
    private ImageView popUpCloseButton;

    DecimalFormat format = new DecimalFormat("##.00");

    private TextView invoiceNumberText,dateTimeText,userText,nameText,idText,prevReadingText,currentReadingText,usageText,amountText,fixedChargeText,totalText,vatText,netAmountText;
    private TextView flatNo,buildingNo,consumerNo,mobileNO,unitPrice,outstandingBalance,prevAmountText,prevReadingTextDate,meterFactorText;

    private Response response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipos_printer_test_demo);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        response = getIntent().getParcelableExtra("print");


        try {
            FINALBILL = response.getLastBill();
        } catch (Exception e) {
            FINALBILL=false;
        }

        assert response != null;

        connectPrinterService();
        initPrinter();

        innitView();
        populate();

    }

    private void populate() {


        try {


            if(response.getInvoiceNumber().equals("")||response.getInvoiceNumber().equals(null))
            {invoiceNumberText.setText("NA" + "");}
            else {invoiceNumberText.setText(response.getInvoiceNumber() + "");}


            String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            dateTimeText.setText(currentDate + " " + currentTime);


            if (response.getUserName().equals("")||response.getUserName().equals(null))
            {userText.setText("NA");}
            else {userText.setText(response.getUserName());}



            if(response.getPartnerName().equals("")||response.getPartnerName().equals(null)) {
                nameText.setText("NA");
            }else {nameText.setText(response.getPartnerName());}



            if(response.getPartnerId().toString().equals("")||response.getPartnerId().toString().equals(null)) {
                idText.setText("NA");
            } else {idText.setText(response.getPartnerId() + "");}




            if(response.getFlat_name().equals("")||response.getFlat_name().equals(null)) {
                flatNo.setText("NA" + "");
            }else {flatNo.setText(response.getFlat_name() + "");}




            if(response.getBuilding_name().equals("")||response.getBuilding_name().equals(null)) {
                buildingNo.setText("NA" + "");
            }else {buildingNo.setText(response.getBuilding_name() + "");}



            if(response.getContract_ref().equals("")||response.getContract_ref().equals(null)) {
                consumerNo.setText("NA" + "");
            }else {consumerNo.setText(response.getContract_ref() + "");}



            if(response.getMobile().equals("")||response.getMobile().equals(null)) {
                mobileNO.setText("NA" + "");
            }else {mobileNO.setText(response.getMobile() + "");}


            if(response.getPriceUnit().equals("")||response.getPriceUnit().equals(null)) {
                unitPrice.setText("NA" + "");
            }else {unitPrice.setText(response.getPriceUnit() + "");}



            if (response.getCredit().equals("")||response.getCredit().equals(null)) {
                outstandingBalance.setText("NA");
            } else { outstandingBalance.setText(response.getCredit() + ""); }



            if(response.getLastMeterReading().equals("")||response.getLastMeterReading().equals(null)) {
                prevReadingText.setText("NA");
            }else {prevReadingText.setText(response.getLastMeterReading() + "");}



            if(response.getLast_reading_date().equals("")||response.getLast_reading_date().equals(null)) {
                prevReadingTextDate .setText("NA");
            }else {prevReadingTextDate.setText(response.getLast_reading_date() + "");}




            if(response.getMeterReading().equals("")||response.getMeterReading().equals(null)) {
                currentReadingText.setText("NA");
            }currentReadingText.setText(response.getMeterReading() + "");




            if(response.getDifferenceReading().toString().equals("")||response.getDifferenceReading().toString().equals(null)) {
                usageText.setText("NA");
            }usageText.setText(response.getDifferenceReading().toString());




            if(response.getAmountUntaxed().equals("")||response.getAmountUntaxed().equals(null)) {
                amountText.setText("NA");
            }else {amountText.setText(response.getAmountUntaxed() + "");}




            if(response.getMonthlyFee().equals("")||response.getMonthlyFee().equals(null)) {
                fixedChargeText.setText("NA");
            }else {fixedChargeText.setText(response.getMonthlyFee() + "");}


            if(response.getMeter_factor().equals("")||response.getMeter_factor().equals(null)) {
                meterFactorText.setText("NA");
            }else {meterFactorText.setText(response.getMeter_factor() + "");}




            /* totalText.setText(response.getPriceUnit()+"");*/


            if(response.getAmountTax().toString().equals("")||response.getAmountTax().toString().equals(null)) {
                vatText.setText("NA");
            }else {vatText.setText(response.getAmountTax().toString());}



            if(response.getAmountTotal().toString().equals("")||response.getAmountTotal().toString().equals(null)) {
                netAmountText.setText("NA");
            }else {netAmountText.setText(response.getAmountTotal().toString());}


          /*  if(response.getPrevBal().toString().equals("")||response.getPrevBal().toString().equals(null)) {
                prevAmountText.setText("NA");
            }else {prevAmountText.setText(response.getPrevBal().toString());}
            */

            if(response.getPrevious_credit().toString().equals("")||response.getPrevious_credit().toString().equals(null)) {
                prevAmountText.setText("NA");
            }else {prevAmountText.setText(response.getPrevious_credit().toString());}




        }catch (Exception e){}


    }

    private void innitView() {

        flatNo=findViewById(R.id.flatNoText);
        buildingNo=findViewById(R.id.buildingNoText);
        consumerNo=findViewById(R.id.consumerNoText);
        mobileNO=findViewById(R.id.mobileNoText);
        unitPrice=findViewById(R.id.unitPriceText);
        outstandingBalance=findViewById(R.id.outstandingAmountText);
        meterFactorText=findViewById(R.id.meterFactorText);

        popUpCloseButton=findViewById(R.id.closePopupButton);
        invoiceNumberText=findViewById(R.id.invoiceNumberText);
        dateTimeText=findViewById(R.id.dateTimeText);
        userText=findViewById(R.id.userText);
        nameText=findViewById(R.id.nameText);
        idText=findViewById(R.id.idText);
        prevReadingText=findViewById(R.id.prevReadingText);
        currentReadingText=findViewById(R.id.currentReadingText);
        usageText=findViewById(R.id.usageText);
        amountText=findViewById(R.id.amountText);
        fixedChargeText=findViewById(R.id.fixedChargeText);
//        totalText=findViewById(R.id.amountText);
        vatText=findViewById(R.id.vatText);
        netAmountText=findViewById(R.id.netAmountText);
        prevAmountText=findViewById(R.id.prevAmountText);
        prevReadingTextDate=findViewById(R.id.prevReadingTextDate);
        popUpCloseButton.setOnClickListener(this);

        b_koubei = (Button) findViewById(R.id.b_koubei);

        b_koubei1 = (Button) findViewById(R.id.b_koubei1);



        b_koubei1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                printBill(1);

            }
        });



        findViewById(R.id.b_exit).setOnClickListener(this);
        b_koubei.setOnClickListener(this);




//        info = (TextView) findViewById(R.id.info);
//        info.setText(VERSION);
    }

    @Override
    public void onClick(View v) {
        if (ButtonDelayUtils.isFastDoubleClick()) {
            return;
        }

        switch (v.getId()) {

            case R.id.b_koubei: printBill(0);

                break;

            case R.id.b_exit:
                finish();
                break;

            case R.id.closePopupButton:

                finish();

                try {
                    MessageEventBus.INSTANCE.publish(new ReadingreturnEventModel(122,response.getMeterReading().toString(),response.getCredit().toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            default:
                break;
        }
    }





    public void initPrinter() {

        if (woyouService == null) {
            // Toast.makeText(PrintReadingHistory.this, "Service Has Been Disconnected", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            woyouService.printerInit(null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void connectPrinterService() {
        Intent intent = new Intent();
        intent.setPackage(SERVICE＿PACKAGE);
        intent.setAction(SERVICE＿ACTION);
        PrintActivity.this.getApplicationContext().startService(intent);
        PrintActivity.this.getApplicationContext().bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection connService = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    private Bitmap scaleImage(Bitmap bitmap1) {
        int width = bitmap1.getWidth();
        int height = bitmap1.getHeight();
        // 设置想要的大小
        int newWidth = (width/8+1)*6;
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, 1);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,
                true);
        return newbm;
    }

    public void printBill(final int types) {
        ThreadPoolManager.getInstance().executeTask(new Runnable() {

            @Override
            public void run() {

//                Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.happyway_printd);
                Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.petro_logo_print);

                try {

                    b_koubei.setClickable(false);


                    woyouService.printBitmap(scaleImage(mBitmap), null);

                    woyouService.lineWrap(1,null);

                    woyouService.printTextWithFont("         Al Qusais, Dubai,\n", "ST", 24, null);
                    woyouService.printTextWithFont("     United Arab Emirates\n", "ST", 24, null);

                    woyouService.printTextWithFont("     0502573388 / 042673200\n", "ST", 24, null);
                    woyouService.printTextWithFont("       utility@petrosafe.ae\n", "ST", 24, null);

                    woyouService.printTextWithFont("     TRN : 100065418400003 \n", "ST", 24, null);
                    woyouService.printTextWithFont("         " + "Tax Invoice\n********************************\n", "ST", 24, null);


                    if(response.getInvoiceNumber().equals("")||response.getInvoiceNumber().equals(null)) {
                        woyouService.printTextWithFont("Invoice number : " + "NA"+"\n", "ST", 24, null);
                    }
                    else {woyouService.printTextWithFont("Invoice number : " + response.getInvoiceNumber()+"\n", "ST", 24, null);}


                    if(FINALBILL){

                        woyouService.printTextWithFont("(Final Bill) \n", "ST", 24, null);

                    }


                    String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                    String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                    woyouService.printTextWithFont("Date  :" + currentDate + " " + currentTime+"\n", "ST", 24, null);



                    if(response.getUserName().equals("")||response.getUserName().equals(null)){
                        woyouService.printTextWithFont("User  :" + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("User  :" + response.getUserName() + "\n", "ST", 24, null);}


                    woyouService.printTextWithFont("********************************\n", "ST", 24, null);

                    if(response.getPartnerName().equals("")||response.getPartnerName().equals(null)) {
                        woyouService.printTextWithFont("Name             : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Name             : " + response.getPartnerName() + "\n", "ST", 24, null);}


                    if(response.getPartnerId().toString().equals("")||response.getPartnerId().equals(null)) {
                        woyouService.printTextWithFont("Customer Id      : " + "NA" + "\n", "ST", 24, null);
                    }
                    else {woyouService.printTextWithFont("Customer Id      : " + response.getPartnerId() + "\n", "ST", 24, null);}


                    if(response.getFlat_name().equals("")||response.getFlat_name().equals(null)) {
                        woyouService.printTextWithFont("Flat No          : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Flat No          : " + response.getFlat_name() + "\n", "ST", 24, null);}


                    if(response.getBuilding_name().equals("")||response.getBuilding_name().equals(null)) {
                        woyouService.printTextWithFont("Building         : " + "NA" + "\n", "ST", 24, null);
                    } else {woyouService.printTextWithFont("Building         : " + response.getBuilding_name() + "\n", "ST", 24, null);}


                    if(response.getContract_ref().equals("")||response.getContract_ref().equals(null)) {
                        woyouService.printTextWithFont("Contract No      : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Contract No      : " + response.getContract_ref() + "\n", "ST", 24, null);}

/*

                    if(response.getMobile().equals("")||response.getMobile().equals(null)) {
                        woyouService.printTextWithFont("Mobile No        : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Mobile No        : " + response.getMobile() + "\n", "ST", 24, null);}
*/


                    if(response.getLastMeterReading().equals(null)||response.getLastMeterReading().equals("")) {
                        woyouService.printTextWithFont("Previous Reading : " + "NA" + "\n", "ST", 24, null);
                    }else{ woyouService.printTextWithFont("Previous Reading : " + response.getLastMeterReading() + "\n", "ST", 24, null);}


                    if(response.getLast_reading_date().equals(null)||response.getLast_reading_date().equals("")) {
                        woyouService.printTextWithFont("Pre Reading Date : " + "NA" + "\n", "ST", 24, null);
                    }else{ woyouService.printTextWithFont("Pre Reading Date : " + response.getLast_reading_date() + "\n", "ST", 24, null);}


                    if(response.getMeterReading().equals("")||response.getMeterReading().equals(null)) {
                        woyouService.printTextWithFont("Current Reading  : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Current Reading  : " + response.getMeterReading() + "\n", "ST", 24, null);}


                    if(response.getDifferenceReading().equals("")||response.getDifferenceReading().equals(null)) {
                        woyouService.printTextWithFont("Usage            : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Usage            : " + response.getDifferenceReading() + "\n", "ST", 24, null);}



                    if(response.getPriceUnit().equals("")||response.getPriceUnit().equals(null)) {
                        woyouService.printTextWithFont("Unit Price       : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Unit Price       : " + response.getPriceUnit() + "\n", "ST", 24, null);}



                    if(response.getMeter_factor().equals("")||response.getMeter_factor().equals(null)) {
                        woyouService.printTextWithFont("Meter factor     : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Meter factor     : " + response.getMeter_factor() + "\n", "ST", 24, null);}



                    if(response.getMonthlyFee().equals("")||response.getMonthlyFee().equals(null)) {
                        woyouService.printTextWithFont("Service Charge   : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Service Charge   : " + response.getMonthlyFee() + "\n", "ST", 24, null);}

                    if(response.getAmountUntaxed().equals("")||response.getAmountUntaxed().equals(null)) {
                        woyouService.printTextWithFont("Amount           : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Amount           : " + response.getAmountUntaxed() + "\n", "ST", 24, null);}

                    if(response.getAmountTax().equals("")||response.getAmountTax().equals(null)) {
                        woyouService.printTextWithFont("Vat Amount (5%)  : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Vat Amount (5%)  : " + response.getAmountTax() + "\n", "ST", 24, null);}

                    if(response.getAmountTotal().equals("")||response.getAmountTotal().equals(null)) {
                        woyouService.printTextWithFont("Net Amount       : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Net Amount       : " + response.getAmountTotal() + "\n", "ST", 24, null);}


                  /*  if(response.getPrevBal().equals("")||response.getPrevBal().equals(null)) {
                        woyouService.printTextWithFont("Prev Balance     : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Prev Balance     : " + response.getPrevBal() + "\n", "ST", 24, null);}*/

                    if(response.getPrevious_credit().equals("")||response.getPrevious_credit().equals(null)) {
                        woyouService.printTextWithFont("Prev Balance     : " + "NA" + "\n", "ST", 24, null);
                    }else {woyouService.printTextWithFont("Prev Balance     : " + response.getPrevious_credit() + "\n", "ST", 24, null);}


                    String outBalT = "";

                    if(response.getCredit().equals("")||response.getCredit().equals(null)){

                        outBalT="NA";

                    }else outBalT = response.getCredit().toString();

                    woyouService.printTextWithFont("Out.S balance    : " + outBalT +"\n", "ST", 24, null);



                    if(types==0){

                        woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                        woyouService.printTextWithFont("Merchant Signature" + "\n", "ST", 24, null);
                        woyouService.lineWrap(3, null);

                    }


                    if(types==1){

                        woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                        woyouService.printTextWithFont("Customer Signature" + "\n", "ST", 24, null);
                        woyouService.lineWrap(3, null);

                    }


                    woyouService.printTextWithFont("********************************\n", "ST", 24, null);
                    woyouService.printTextWithFont("          Thank you             ", "ST", 24, null);

                    woyouService.lineWrap(3, null);

                    b_koubei.setClickable(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }


}
