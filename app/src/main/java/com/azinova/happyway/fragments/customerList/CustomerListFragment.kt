package com.azinova.happyway.fragments.customerList

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.input.customerList.CustomerListInput
import com.azinova.happyway.model.input.customerList.CustomerListParamModel
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.SearchEventModel
import com.azinova.happyway.utils.ObjectFactory
import kotlinx.android.synthetic.main.fragment_customer_list.*
import kotlinx.android.synthetic.main.loader_layout.*

class CustomerListFragment : MvpFragment<CustomerListPresenter>(), CustomerListView, CustomerOnClick {

    private var rootView: View? = null
    private var customerList: MutableList<CustomerListItem>? = null
    private var mViewAdapter: CustomerListAdapter? = null
    private var buildingData: BuildingListItem? = null
    override fun onCustomerSelected(contact: CustomerListItem) {
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtra("page", DetailActivity.Pages.CUSTOMER_DETAILS)
            putExtra("customerDetails", contact)
        }
        startActivity(intent)
    }

    override fun createPresenter(): CustomerListPresenter {
        return CustomerListPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_customer_list, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        MessageEventBus.listen(SearchEventModel::class.java).subscribe{

            if (it.code==101){

                mViewAdapter?.filter?.filter(it.sent.toString())

            }

        }


        swipeRefreshLayout.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            Log.e("Scrollsaadasd","asdad")

            initView()

            swipeRefreshLayout.setRefreshing(false)

        })

    }

    private fun initView() {
        buildingData = arguments?.getParcelable("buildingList")
        customerList = ArrayList()
        mViewAdapter = context?.let { CustomerListAdapter(it, customerList as ArrayList<CustomerListItem>, this) }
        customerRv.apply {
            adapter = mViewAdapter
        }
        val customerListParamModel = CustomerListParamModel()
        val customerListInput = CustomerListInput()
        customerListInput.buildingId = buildingData?.buildingId!!.toInt()
        customerListInput.userId = ObjectFactory.getInstance(context).appPreference.userId
        customerListParamModel.params = customerListInput
        presenter.customerList(context, customerListParamModel)
    }

    override fun showAPILoading() {
        orderlistfragment_loader_lty.visibility = View.VISIBLE
    }

    override fun hideAPILoading() {
        orderlistfragment_loader_lty.visibility = View.GONE
    }

    override fun getAPIListFail(message: String) {
        orderlistfragment_loader_lty.visibility = View.GONE
    }

    override fun failed(message: String) {
        orderlistfragment_loader_lty.visibility = View.GONE
    }



    override fun getCustomerList(customerListModel: ArrayList<CustomerListItem>) {
        customerList?.clear()
        customerList?.addAll(customerListModel)
        mViewAdapter?.notifyDataSetChanged()
    }


    override fun onResume() {

        initView()
        super.onResume()

        getActivity()?.searchBox?.setText("")

        Log.e("on resume","entered")

    }

}