package com.azinova.happyway.fragments.ReadingHistory

import com.azinova.happyway.model.output.ReadingHistoryModel.ResponseItem

interface ReadingHistoryView {

    fun showAPILoading()

    fun hideAPILoading()

    fun failed(message: String)

    fun setLayout(res: ArrayList<ResponseItem>)

}