package com.azinova.happyway.fragments.BuildingsList

import com.azinova.happyway.model.output.Login.BuildingListItem
import java.util.*

interface BuildingListView {

    fun getBuildingDetails(buildingListModel: ArrayList<BuildingListItem>)

    fun showAPILoading()

    fun hideAPILoading()



}