package com.azinova.happyway.fragments.BuildingsList

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.SearchEventModel
import kotlinx.android.synthetic.main.fragment_customer_list.*
import kotlinx.android.synthetic.main.loader_layout.*


class BuildingFragment(var searchKeyWord:String) : MvpFragment<BuildingListPresenter>(), BuildingListView, BuildingOnClick ,SearchInterface{

    override fun onBuildingSelected(building: BuildingListItem) {
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtra("page", DetailActivity.Pages.CUSTOMER_LIST)
            putExtra("buildingList", building)
        }
        startActivity(intent)
    }

    private var rootView: View? = null
    private var buildingList: MutableList<BuildingListItem>? = null
    private var mViewAdapter: BuildingListAdapter? = null


    override fun createPresenter(): BuildingListPresenter {
        return BuildingListPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_customer_list, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        MessageEventBus.listen(SearchEventModel::class.java).subscribe {
            if (it.code == 100) {

                mViewAdapter?.filter?.filter(it.sent.toString())

            }
        }

        swipeRefreshLayout.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            Log.e("Scrollsaadasd","asdad")

            initView()

            swipeRefreshLayout.setRefreshing(false)

        })

    }

    override fun onResume() {
        super.onResume()
        getActivity()?.searchBox?.setText("")

    }

    private fun initView() {

        buildingList = ArrayList()
        mViewAdapter = context?.let { BuildingListAdapter(it, buildingList as ArrayList<BuildingListItem>, this) }
        customerRv.apply {
            adapter = mViewAdapter
        }
        context?.let { presenter.getBuildingList(it) }

    }

    override fun getBuildingDetails(buildingListModel: ArrayList<BuildingListItem>) {


        if(searchKeyWord.equals("")){

            buildingList?.clear()
            buildingList?.addAll(buildingListModel)
            mViewAdapter?.notifyDataSetChanged()
        }
        else{

            buildingList?.clear()
            buildingList?.addAll(buildingListModel)
            mViewAdapter?.filter?.filter(searchKeyWord)


        }

    }

    override fun showAPILoading() {
        orderlistfragment_loader_lty.visibility = View.VISIBLE
    }

    override fun hideAPILoading() {
        orderlistfragment_loader_lty.visibility = View.INVISIBLE
    }

    override fun filt(s: String) {

        Log.e("entered",s)

    }


}