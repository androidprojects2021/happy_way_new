package com.azinova.happyway.fragments.Payments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import com.azinova.happyway.PrintPayment
import com.azinova.happyway.R
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.input.PaymentInput.PaymentInputModel
import com.azinova.happyway.model.input.PaymentInput.PaymentInputParam
import com.azinova.happyway.model.input.customerList.CustomerListInput
import com.azinova.happyway.model.input.customerList.CustomerListParamModel
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.PaymentTypeModel.ResponseItem
import com.azinova.happyway.model.output.PaymentTypeModel.ResponsePayPrint
import com.azinova.happyway.utils.ObjectFactory
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.loader_layout.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PaymentFragment : MvpFragment<PaymentPresenter>(), PaymentView {

    private var paymentType: MutableList<ResponseItem>? = null
    var names: MutableList<String> = ArrayList()
    private var payment_method: Long? = 0
    private var note: String? = null

    private var resp: ResponsePayPrint? = null

    private var dateSelected=""

    override fun setLayout(result: ArrayList<ResponseItem>) {
        paymentType?.clear()
        paymentType?.addAll(result)

        for (i in this.paymentType!!) {
            i.name?.let { names.add(it) }
        }


        val spinnerArrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, names.toList())
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        payType.adapter = spinnerArrayAdapter

        payType.onItemSelectedListener = object : AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                Toast.makeText(context, paymentType!![position].name, Toast.LENGTH_SHORT).show()
                payment_method = paymentType!![position].id


                if(paymentType!![position].id==7.toLong()) //credit card
                {

                    bottomSeperate.visibility=View.VISIBLE
                    bottomSeperateCheck.visibility=View.GONE
                    refernce_number_constraint.visibility=View.VISIBLE
                    cheque_layout_cheque_one.visibility=View.GONE
                    cheque_layout.visibility=View.GONE
                }
                
                if(paymentType!![position].id==6.toLong()) //bank
                {
                    bottomSeperate.visibility=View.GONE
                    bottomSeperateCheck.visibility=View.VISIBLE
                    refernce_number_constraint.visibility=View.GONE
                    cheque_layout_cheque_one.visibility=View.VISIBLE
                    cheque_layout.visibility=View.VISIBLE
                }

                if(paymentType!![position].id==5.toLong())//cash
                {

                    bottomSeperate.visibility=View.GONE
                    bottomSeperateCheck.visibility=View.GONE
                    refernce_number_constraint.visibility=View.GONE
                    cheque_layout_cheque_one.visibility=View.GONE
                    cheque_layout.visibility=View.GONE

                }


                if(paymentType!![position].id==8.toLong())//cheque
                {

                    bottomSeperate.visibility=View.GONE
                    bottomSeperateCheck.visibility=View.VISIBLE
                    refernce_number_constraint.visibility=View.GONE
                    cheque_layout_cheque_one.visibility=View.VISIBLE
                    cheque_layout.visibility=View.VISIBLE

                }

            }

        }


    }

    override fun exit() {

        this.getActivity()?.onBackPressed()

    }

    override fun printSuccess(model: ResponsePayPrint?) {

        resp=model
        val intent = Intent(context, PrintPayment::class.java)
        intent.putExtra("print",resp)
        this.getActivity()?.onBackPressed()
        startActivity(intent)


    }


    override fun failed(s: String) {

        orderlistfragment_loader_lty.visibility = View.GONE

    }

    override fun showAPILoading() {
        orderlistfragment_loader_lty.visibility = View.VISIBLE
    }

    override fun hideAPILoading() {
        orderlistfragment_loader_lty.visibility = View.INVISIBLE
    }

    private var rootView: View? = null
    private var customerDetails: CustomerListItem? = null


    override fun createPresenter(): PaymentPresenter {
        return PaymentPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_payment, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        initUI()
        getPaymentType()
        onClick()
        setDAte()

    }

    private fun setDAte() {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
         dateSelected = sdf.format(Date())
        dateSelectPay.setText(dateSelected)
        Log.e("date ist",dateSelected)

    }

    private fun getPaymentType() {
        paymentType = ArrayList()
        val customerListParamModel = CustomerListParamModel()
        val customerListInput = CustomerListInput()
        customerListInput.buildingId = 1
        customerListInput.userId = ObjectFactory.getInstance(context).appPreference.userId
        customerListParamModel.params = customerListInput
        context?.let { presenter.getPaymentTypeFromApi(it, customerListParamModel) }

    }

    @SuppressLint("SetTextI18n")
    private fun initUI() {
        customerDetails = arguments?.getParcelable("customerDetails")
        customer_name_txt.text = customerDetails?.name
        hw_txt.text = customerDetails?.customerId.toString()

        if (customerDetails?.previousReading == 0.0) {
            txt_meter_reading.text = "0.0 m³"
        } else {
            txt_meter_reading.text = customerDetails?.previousReading.toString() + " m³"
        }
        serial_number.text = customerDetails?.meterNumber
        readDate.text = customerDetails?.contractDate
        balance.text = "AED "+customerDetails?.pendingAmount.toString()
//        last_invoiced.text="AED "+customerDetails?.previousReading.toString()
        last_invoiced.text="AED "+customerDetails?.last_invoiced_amount.toString()
        edit_txt_aed.hint=customerDetails?.pendingAmount.toString()

    }




    var cal = Calendar.getInstance()
    var date=""

    val dateSetListener = object : DatePickerDialog.OnDateSetListener {

        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {

            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "yyyy-MM-dd" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)

            date = sdf.format(cal.getTime())
            cheque_date.setText(date)



        }

    }



    private fun onClick() {


      /*  dateSelectPay.setOnClickListener {

            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(context!!,R.style.DialogTheme ,DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                dateSelected="${year}-${if(monthOfYear+1 >9)"${monthOfYear+1}" else "0${monthOfYear+1}" }-${if(dayOfMonth >9)"${dayOfMonth}" else "0${dayOfMonth}" }"
                dateSelectPay.setText(dateSelected)
                Log.e("DAte ", dateSelected)

            }, year, month, day)

            dpd.show()

        }   */

        checkNo.setOnClickListener {

            Log.e("sdadas","asfsdfsdfs")

            context?.let { it1 ->
                DatePickerDialog(it1,
                        dateSetListener,
                        // set DatePickerDialog to point to today's date when it loads up
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show()
            }


        }


        confirmPayment.setOnClickListener {

            val paymentInputModel = PaymentInputModel()
            paymentInputModel.user_id = ObjectFactory.getInstance(context).appPreference.userId
            paymentInputModel.notes = notes.text.toString()
            paymentInputModel.date=dateSelected
            paymentInputModel.customer_id = customerDetails?.customerId!!

            paymentInputModel.payment_method = payment_method?.toInt()!!







            if (edit_txt_aed.text.toString().isBlank()) {
                Toast.makeText(context, "Please enter amount", Toast.LENGTH_SHORT).show()
            } else {

                paymentInputModel.amount = edit_txt_aed.text.toString().toDouble()

            }
            paymentInputModel.notes=notes.text.toString()

            paymentInputModel.reference=""

            if(payment_method?.toInt()!=5)
            {
                if((payment_method?.toInt()==8)||(payment_method?.toInt()==6))
                {

                    if(cheque_number.text.toString().isBlank()||cheque_date.text.toString().isBlank())
                    {

                        Toast.makeText(context, "Please Enter Cheque Number and Date", Toast.LENGTH_SHORT).show()

                    }else{

                        paymentInputModel.cheque_no=cheque_number.text.toString()
                        paymentInputModel.cheque_date=cheque_date.text.toString()

                        val paramModel= PaymentInputParam()

                        paramModel.params=paymentInputModel
                        context?.let { it1 -> presenter.savePayment(it1,paramModel) }

                    }


                }


                if((payment_method?.toInt()==7))
                {

                    if(refernce_no_edittext.text.toString().isBlank())
                    {

                        Toast.makeText(context, "Please Cheque Number and Date", Toast.LENGTH_SHORT).show()

                    }else{

                        paymentInputModel.card_ref=refernce_no_edittext.text.toString()

                        val paramModel= PaymentInputParam()

                        paramModel.params=paymentInputModel
                        context?.let { it1 -> presenter.savePayment(it1,paramModel) }

                    }

                    paymentInputModel.card_ref=refernce_no_edittext.text.toString()
                }

            }

           else{

                val paramModel= PaymentInputParam()

                paramModel.params=paymentInputModel
                context?.let { it1 -> presenter.savePayment(it1,paramModel) }

            }






        }
//        radio_group_paymenttype?.setOnCheckedChangeListener { group, checkedId ->
//            var text = "Selected: "
//            when (checkedId) {
//                R.id.button_cash -> {
//                    text += "Cash"
//                    refernce_no_edittext.hint = "Cash Collected"
//                    cheque_layout.visibility = View.GONE
//
//                }
//                R.id.button_card -> {
//                    text += "Card"
//                    refernce_no_edittext.hint = "Reference No."
//                    cheque_layout.visibility = View.GONE
//
//                }
//                else -> {
//                    text += "Cheque"
//                    refernce_no_edittext.hint = "Cheque No."
//                    cheque_layout.visibility = View.VISIBLE
//                }
//            }
//            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
//
//        }
    }



}