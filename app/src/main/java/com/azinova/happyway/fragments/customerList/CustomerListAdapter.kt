package com.azinova.happyway.fragments.customerList

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azinova.happyway.R
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class CustomerListAdapter(private val context: Context,
                          private var customerDetailsItem: List<CustomerListItem>,
                          private val listener: CustomerOnClick) : RecyclerView.Adapter<CustomerListAdapter.MyViewHolder>(),
        Filterable {
    private var customerDetailsItemListfiltered: List<CustomerListItem>? = null

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.customerName)
        var userId: TextView = view.findViewById(R.id.userId)
        var customerId: TextView = view.findViewById(R.id.customerId)

        var address1: TextView = view.findViewById(R.id.bId)



        var address2: TextView = view.findViewById(R.id.bName)
        var date: TextView = view.findViewById(R.id.date)
        var outBal: TextView = view.findViewById(R.id.outBalCusList)
//        var mobileCusList: TextView = view.findViewById(R.id.mobileCusList)
        var relativeLayout: ConstraintLayout = view.findViewById(R.id.relativeLayout)
        var cardBorder: ConstraintLayout = view.findViewById(R.id.cardBorder)
        var cardBorderOuter: CardView = view.findViewById(R.id.outerCard)



        init {
            view.setOnClickListener {
                listener.onCustomerSelected(customerDetailsItemListfiltered!![adapterPosition])
            }
        }
    }

    init {
        this.customerDetailsItemListfiltered = customerDetailsItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        //val itemView = LayoutInflater.from(parent.context).inflate(R.layout.adapter_customer_list, parent, false)
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.adapter_customer_list_updated, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return customerDetailsItemListfiltered!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val customer = customerDetailsItemListfiltered!![position]

        holder.name.text = customer.name.toString()

       /* holder.userId.text = customer.customerId.toString()*/

        holder.userId.text = customer.code.toString()


//        holder.customerId.text=customer..toString()


        holder.customerId.text=customer.flat_name.toString()


        if(customer.building_name.equals(""))
        {
            holder.address1.text = "NA"
        }
        else holder.address1.text = customer.building_name



        /*if(customer.mobile.equals(""))
        {
            holder.address2.text = "NA"
        }
        else holder.address2.text = customer.mobile  */


        if(customer.meterNumber.equals(""))
        {
            holder.address2.text = "NA"
        }
        else holder.address2.text = customer.meterNumber


//        holder.date.text = customer.contractDate




        if(customer.previousReadingDate.equals(""))
        {
            holder.date.text = "NA"

        }else holder.date.text = customer.previousReadingDate

        holder.outBal.text=customer.pendingAmount.toString()+" AED"
//        holder.mobileCusList.text=customer.mobile.toString()

        val format = SimpleDateFormat("yyyy-MM-dd")

        if(!customer.previousReadingDate.equals("")) {

            val date = format.parse(customer.previousReadingDate)
            val todaydate = format.parse(SimpleDateFormat("yyyy-MM-dd").format(Date()).toString())
            Log.e("date", date.toString())

            val diff: Long = todaydate.getTime() - date.getTime()

            if(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)>=60)

            {

               /* holder.cardBorder.setBackgroundColor(Color.parseColor("#FE0001"))*/
                holder.cardBorderOuter.setBackgroundTintList(context.getResources().getColorStateList(R.color.border))

            }
            else holder.cardBorderOuter.setBackgroundTintList(context.getResources().getColorStateList(R.color.white))

        }else{

                 /*holder.cardBorder.setBackgroundColor(Color.parseColor("#FE0001"))*/
            holder.cardBorderOuter.setBackgroundTintList(context.getResources().getColorStateList(R.color.border))

            }




    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                customerDetailsItemListfiltered = if (charString.isEmpty()) {
                    customerDetailsItem
                } else {
                    val filteredList = ArrayList<CustomerListItem>()
                    for (row in customerDetailsItem) {


                       /* if (row.name!!.toLowerCase().contains(charString.toLowerCase()) || row.customerId.toString() == charSequence.toString()) {
                            filteredList.add(row)
                        }*/

                        if (row.flat_name!!.toLowerCase().contains(charString.toLowerCase()) || row.customerId.toString() == charSequence.toString() ||row.name!!.toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row)
                        }

                    }
                    filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = customerDetailsItemListfiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults?) {
                customerDetailsItemListfiltered = filterResults?.values as ArrayList<CustomerListItem>
                notifyDataSetChanged()
            }
        }
    }


}