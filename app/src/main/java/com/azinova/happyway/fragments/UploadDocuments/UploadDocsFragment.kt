package com.azinova.happyway.fragments.UploadDocuments

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.FileProvider
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.activity.signature.SavedSignBitmap
import com.azinova.happyway.activity.signature.SignatureActivity
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.input.customer_pay.InputSingleCustomer
import com.azinova.happyway.model.input.customer_pay.Params
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.utils.ObjectFactory
import kotlinx.android.synthetic.main.fab_view.*
import kotlinx.android.synthetic.main.fragment_upload_documents.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream


class UploadDocsFragment(val customerId:String, val buildingId :Int) : MvpFragment<UploadDocsPresenter>(), UploadDocsView {

    private var rootView: View? = null

    private var SIGNATURE_BITMAP:Bitmap ? =null

    private var SIGNATURE_URI:Uri ? =null

    private var EID_FRONT_URI: Uri? =null
    private var EID_BACK_URI:Uri? =null
    private var EJARI_URI:Uri? =null

    private var CUSTOMER_ID = customerId

    var customerPayDetail : com.azinova.happyway.model.output.customer_pay.CustomerListItem? = null

     var imagePath: String = ""

    override fun createPresenter(): UploadDocsPresenter {

        return UploadDocsPresenter(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("ENTER","Create")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_upload_documents, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        initView()
        onClick()
        /*presenter.getCustomer(InputSingleCustomer(Params(buildingId,ObjectFactory.getInstance(context).appPreference.userId,CUSTOMER_ID.toInt())))*/
    }

    private fun onClick() {

        clickImgSign.setOnClickListener { getSign() }
        clickSign.setOnClickListener { getSign() }
        signImage.setOnClickListener { getSign() }

        ejariImgAdd.setOnClickListener {  getEjari()}
        ejariAdd.setOnClickListener { getEjari() }
        ejariImage.setOnClickListener { getEjari() }

        eidBackAdd.setOnClickListener { getEidBack() }
        eidbackImgAdd.setOnClickListener { getEidBack() }
        back.setOnClickListener { getEidBack() }

        eidFrontAdd.setOnClickListener { getEidFront() }
        eidFrontImgAdd.setOnClickListener { getEidFront() }
        front.setOnClickListener { getEidFront() }

        Log.e("id got",CUSTOMER_ID)

        nextButtonContract.setOnClickListener {

            progressBarUps.visibility=View.VISIBLE
            presenter.upImages(CUSTOMER_ID,SIGNATURE_URI,frontLayout.visibility==View.GONE,backLayout.visibility==View.GONE,ejariLayout.visibility==View.GONE)

        }

    }


    private fun getSign(){

        val intent=Intent(context,SignatureActivity::class.java)
        startActivityForResult(intent,25)

    }


    private fun getEjari(){
        /*val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePictureIntent, 24)*/
        dispatchTakePictureIntent("ejari",24)
    }

    private fun getEidBack(){
       /* val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(takePictureIntent, 23)*/
        dispatchTakePictureIntent("eid_back",23)
    }

    private fun getEidFront(){
        /*val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(takePictureIntent, 22)*/

        dispatchTakePictureIntent("eid_front",22)

    }


    private fun initView() {



    }


    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==25 && resultCode==1)
        {
            var str = data?.getStringExtra("sign")
            if(str.equals("sign"))
            {
                var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                file = File(file, "${"signature"}.jpg")

                try {
                    val stream: OutputStream = FileOutputStream(file)
                    SIGNATURE_BITMAP = SavedSignBitmap.getBitmapSign()
                    SIGNATURE_BITMAP?.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                    stream.flush()
                    stream.close()

                    signLayout.visibility=View.GONE
                    signImage.setImageBitmap(SIGNATURE_BITMAP)


                } catch (e: IOException){
                    e.printStackTrace()
                }

                SIGNATURE_URI = Uri.parse(file.absolutePath)

            }
        }


        if (requestCode == 24 && resultCode == RESULT_OK) {

            var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            file = File(file, "${"ejari"}.jpg")
            try {

                val stream: OutputStream = FileOutputStream(file)

                val imageBitmap = data?.extras?.get("data") as Bitmap

                imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                stream.flush()
                stream.close()

                ejariLayout.visibility=View.GONE
                ejariImage.setImageBitmap(imageBitmap)

            } catch (e: IOException){
                e.printStackTrace()
            }

            EJARI_URI = Uri.parse(file.absolutePath)


        }

        if (requestCode == 23 && resultCode == RESULT_OK) {

            var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            file = File(file, "${"eid_back"}.jpg")
            try {

                val stream: OutputStream = FileOutputStream(file)

                var imageBitmap = data?.extras?.get("data") as Bitmap
                imageBitmap=imageBitmap.rotate(-90)
                imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                stream.flush()

                stream.close()
                backLayout.visibility=View.GONE
                back.setImageBitmap(imageBitmap)


            } catch (e: IOException){
                e.printStackTrace()
            }

            EID_BACK_URI = Uri.parse(file.absolutePath)


        }


        if (requestCode == 22 && resultCode == RESULT_OK) {

            var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            file = File(file, "${"eid_front"}.jpg")
            try {

                val stream: OutputStream = FileOutputStream(file)

                var imageBitmap = data?.extras?.get("data") as Bitmap
                imageBitmap=imageBitmap.rotate(-90)
                imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                stream.flush()
                stream.close()

                frontLayout.visibility=View.GONE
                front.setImageBitmap(imageBitmap)


            } catch (e: IOException){
                e.printStackTrace()
            }

            EID_FRONT_URI = Uri.parse(file.absolutePath)


        }


    }*/


    override fun hidePgb(status:String) {
        progressBarUps.visibility=View.GONE

        val dialogBuilder = AlertDialog.Builder(context)

        dialogBuilder.setMessage("Documents Upload ${status}")

                .setCancelable(false)
                .setPositiveButton(if (status.equals("Success")) "OK" else "Retry" , DialogInterface.OnClickListener {
                    dialog, id ->

                    if(status.equals("Success")){

                        val intent = Intent(context, DetailActivity::class.java).apply {
                            putExtra("page", DetailActivity.Pages.BUILDINGS_LIST)

                        }
                        startActivity(intent)


                    }else dialog.dismiss()


                })

               /* .setNegativeButton("Payment",DialogInterface.OnClickListener{
                    dialog, which ->
                        val intent = Intent(context, DetailActivity::class.java).apply {
                            putExtra("page", DetailActivity.Pages.CUSTOMER_DETAILS)
                            val contact = CustomerListItem(customerPayDetail?.previousReading, customerPayDetail?.notes, customerPayDetail?.contractDate,
                                    customerPayDetail?.street, customerPayDetail?.buildingName, customerPayDetail?.mobile, customerPayDetail?.contractRef,
                                    customerPayDetail?.street2, customerPayDetail?.previousReadingDate, customerPayDetail?.meterNumber, customerPayDetail?.rate,
                                    customerPayDetail?.flatName, customerPayDetail?.customerId, customerPayDetail?.pendingAmount, customerPayDetail?.flatName,
                                    customerPayDetail?.buildingName, customerPayDetail?.lastInvoicedAmount, customerPayDetail?.code)
                            putExtra("customerDetails", contact)
                        }

                         startActivity(intent)

                })*/


        val alert = dialogBuilder.create()

        alert.setTitle("Documents Upload")

        alert.show()

    }

    override fun payCustomer(status: String, customer: com.azinova.happyway.model.output.customer_pay.CustomerListItem?) {
        customerPayDetail = if(status.equals("success")){ customer } else null
    }


    override fun ShowPgb() {

    }


    /**
     * create file and store image from camera intent using providers
     */

    private fun createImageFile(timeStamp: String): File {
        var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        file = File(file, "${timeStamp}.jpg")
        imagePath=file.absolutePath
        return file
    }

    private fun dispatchTakePictureIntent(fileNameGet:String,reqCode:Int) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                val photoFile: File? = try {
                    createImageFile(fileNameGet)
                } catch (ex: IOException) { null }

                photoFile?.also {
                    var image_uri = FileProvider.getUriForFile(context!!, "com.azinova.happyway", it)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
                    startActivityForResult(takePictureIntent, reqCode)

                    when(reqCode){

                        22->{

                            EID_FRONT_URI = image_uri
                            Log.e("EID F",EID_FRONT_URI.toString())
                        }
                        23->{

                            EID_BACK_URI = image_uri
                            Log.e("EID B",EID_BACK_URI.toString())

                        }
                        24->{

                            EJARI_URI = image_uri
                            Log.e("EJARI_URI",EJARI_URI.toString())

                        }

                    }

                }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==25 && resultCode==1)
        {
            var str = data?.getStringExtra("sign")
            if(str.equals("sign"))
            {
                var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                file = File(file, "${"signature"}.jpg")

                try {
                    val stream: OutputStream = FileOutputStream(file)
                    SIGNATURE_BITMAP = SavedSignBitmap.getBitmapSign()
                    SIGNATURE_BITMAP?.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                    stream.flush()
                    stream.close()

                    signLayout.visibility=View.GONE
                    signImage.setImageBitmap(SIGNATURE_BITMAP)


                } catch (e: IOException){
                    e.printStackTrace()
                }

                SIGNATURE_URI = Uri.parse(file.absolutePath)

            }
        }


        if (requestCode == 24 && resultCode == RESULT_OK) {


            try {

                ejariLayout.visibility=View.GONE
                ejariImage.setImageURI(null)
                ejariImage.setImageURI(Uri.parse("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/ejari.jpg"))

            } catch (e: IOException){
                e.printStackTrace()
            }



        }

        if (requestCode == 23 && resultCode == RESULT_OK) {


            try {
                var  imageBitmap = BitmapFactory.decodeFile("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/eid_back.jpg")
                var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                file = File(file, "${"eid_back"}.jpg")
                val stream: OutputStream = FileOutputStream(file)
                imageBitmap=imageBitmap.rotate(-90)
                imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                stream.flush()
                stream.close()
            } catch (e: Exception) {
            }


            try {

                backLayout.visibility=View.GONE
                back.setImageURI(null)
                back.setImageURI(Uri.parse("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/eid_back.jpg"))

            } catch (e: IOException){
                e.printStackTrace()
            }


        }


        if (requestCode == 22 && resultCode == RESULT_OK) {


            try {
                var  imageBitmap = BitmapFactory.decodeFile("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/eid_front.jpg")
                var file = getActivity()?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                file = File(file, "${"eid_front"}.jpg")
                val stream: OutputStream = FileOutputStream(file)
                imageBitmap=imageBitmap.rotate(-90)
                imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                stream.flush()
                stream.close()
            } catch (e: Exception) {
            }



            try {

                frontLayout.visibility=View.GONE
                front.setImageURI(null)
                front.setImageURI(Uri.parse("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/eid_front.jpg"))

            } catch (e: IOException){
                e.printStackTrace()
            }


        }


    }


    fun Bitmap.rotate(degree:Int):Bitmap{
        val matrix = Matrix()

        matrix.postRotate(degree.toFloat())

        val scaledBitmap = Bitmap.createScaledBitmap(
                this,
                width,
                height,
                true
        )

        return Bitmap.createBitmap(
                scaledBitmap,
                0,
                0,
                scaledBitmap.width,
                scaledBitmap.height,
                matrix,
                true
        )
    }


}