package com.azinova.happyway.fragments.SummaryReport

import com.azinova.happyway.model.output.Report.ResponseReport

interface SummaryReportView {

    fun showAPILoading()

    fun hideAPILoading()

    fun failed(message: String)

    fun populateData(report: ResponseReport)

}