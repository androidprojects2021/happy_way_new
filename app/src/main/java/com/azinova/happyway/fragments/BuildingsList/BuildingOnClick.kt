package com.azinova.happyway.fragments.BuildingsList

import com.azinova.happyway.model.output.Login.BuildingListItem

interface BuildingOnClick {
    fun onBuildingSelected(building: BuildingListItem)
}