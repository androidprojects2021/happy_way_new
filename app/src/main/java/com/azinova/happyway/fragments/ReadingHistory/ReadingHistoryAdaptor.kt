package com.azinova.happyway.fragments.ReadingHistory

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.azinova.happyway.R
import com.azinova.happyway.model.output.ReadingHistoryModel.ResponseItem
import kotlinx.android.synthetic.main.custom_rview.view.*
import kotlinx.android.synthetic.main.custom_rview_updated.view.*
import java.text.SimpleDateFormat
import java.util.*


class ReadingHistoryAdaptor(val context: Context?, val list: List<ResponseItem>, var click: ClickInterface,var datePrint :String) : RecyclerView.Adapter<ReadingHistoryAdaptor.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun handleData(pos: Int, context: Context, data: ResponseItem, date:String ) {

           /* itemView.slNo.text = data.partnerId.toString()*/


            Log.e("date getasdasda",date)


            if(date.equals(""))
            {

                itemView.day_read_history.text = SimpleDateFormat("yyyy-MMM-dd").format(Date()).toString()

                Log.e("date get",SimpleDateFormat("yyyy-MMM-dd").format(Date()).toString())




            }else{


                itemView.day_read_history.text=date.substring(8,10)
                monthFind(date.substring(5,7))
                itemView.year_read_history.text=date.substring(0,4)

            }


            /*

            itemView.meterText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_building_24, 0, 0, 0)
            itemView.nameText.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            itemView.nameText.text = data.partnerName
            itemView.meterText.text = data.flatName
            itemView.amt.visibility = View.GONE
            itemView.totalText.visibility=View.GONE
            itemView.amountText.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            itemView.amountText.text = data.meterReading.toString() + " m³"

            */

            itemView.customerNameTextReadHistory.setText(data.partnerName)
            itemView.contractRefreadHistory.setText(data.contract_ref)
            itemView.meterReadReadHistory.setText(data.meterReading.toString())
            itemView.buildingreadHistory.setText(data.buildingName)
            itemView.priceReadHistory.setText("AED "+data.amount_total.toString())


        }


        fun monthFind(month:String){

            when(month){

                "01"->itemView.month_read_history.text="Jan"
                "02"->itemView.month_read_history.text="Feb"
                "03"->itemView.month_read_history.text="Mar"
                "04"->itemView.month_read_history.text="Apr"
                "05"->itemView.month_read_history.text="May"
                "06"->itemView.month_read_history.text="Jun"
                "07"->itemView.month_read_history.text="Jul"
                "08"->itemView.month_read_history.text="Aug"
                "09"->itemView.month_read_history.text="Sep"
                "10"->itemView.month_read_history.text="Oct"
                "11"->itemView.month_read_history.text="Nov"
                "12"->itemView.month_read_history.text="Dec"

            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_rview, parent, false)
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_rview_updated, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.handleData(position, context!!, list[position],datePrint)
        holder.itemView.setOnClickListener {

            click.clickItemList(position)

        }

    }



}