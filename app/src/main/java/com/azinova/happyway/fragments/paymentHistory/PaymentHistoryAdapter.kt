package com.azinova.happyway.fragments.paymentHistory

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.happyway.R
import com.azinova.happyway.model.output.PaymentHistory.ResponseItem
import kotlinx.android.synthetic.main.custom_rview.view.*
import kotlinx.android.synthetic.main.custom_rview_pay_his.view.*
import kotlinx.android.synthetic.main.custom_rview_updated.view.*
import kotlinx.android.synthetic.main.new_payment_history_print_scroll.view.*
import java.text.SimpleDateFormat
import java.util.*

class PaymentHistoryAdapter(val context: Context?, val list: List<ResponseItem>,var clickInterface: OnClickInterface,var datePrint :String) : RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

//        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_rview, parent, false)
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_rview_pay_his, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.handleData(position, context!!, list[position],datePrint)
        holder.itemView.setOnClickListener { clickInterface.itemClicked(position) }
    }

    class ViewHolder (view: View):RecyclerView.ViewHolder(view) {

        fun handleData(pos: Int, context: Context, data: ResponseItem,date:String) {




            if(date.equals(""))
            {

                itemView.day_pay_history.text = SimpleDateFormat("yyyy-MMM-dd").format(Date()).toString()

                Log.e("date get", SimpleDateFormat("yyyy-MMM-dd").format(Date()).toString())




            }else{


                itemView.day_pay_history.text=date.substring(8,10)
                monthFind(date.substring(5,7))
                itemView.year_pay_history.text=date.substring(0,4)

            }


            /* itemView.slNo.text = data.journalId.toString()
             itemView.dateText.text = data.paymentDate
             itemView.nameText.text = data.partnerName
             itemView.meterText.text = data.contract
             itemView.totalText.text = data.amount.toString()
             itemView.amountText.text = data.amount.toString()*/


            itemView.customerNameTextPayHistory.setText(data.partnerName)
            itemView.contractRefPayHistory.setText(data.contract)
            itemView.buildinPayHistory.setText(data.building_name)
            itemView.payTypePayHistory.setText(data.journalName)
            itemView.pricePayHistory.setText("AED "+data.amount.toString())



        }

        fun monthFind(month:String){

            when(month){

                "01"->itemView.month_pay_history.text="Jan"
                "02"->itemView.month_pay_history.text="Feb"
                "03"->itemView.month_pay_history.text="Mar"
                "04"->itemView.month_pay_history.text="Apr"
                "05"->itemView.month_pay_history.text="May"
                "06"->itemView.month_pay_history.text="Jun"
                "07"->itemView.month_pay_history.text="Jul"
                "08"->itemView.month_pay_history.text="Aug"
                "09"->itemView.month_pay_history.text="Sep"
                "10"->itemView.month_pay_history.text="Oct"
                "11"->itemView.month_pay_history.text="Nov"
                "12"->itemView.month_pay_history.text="Dec"

            }

        }


    }


}