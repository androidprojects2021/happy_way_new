package com.azinova.happyway.fragments.BuildingsList

import android.content.Context
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.azinova.happyway.R
import com.azinova.happyway.internet_utils.ConnectionCheck
import com.azinova.happyway.model.output.Login.BuildingListItem

class BuildingListAdapter(private val context: Context,
                          private val assignedBuildingsItem: List<BuildingListItem>,
                          private val listener: BuildingOnClick) : RecyclerView.Adapter<BuildingListAdapter.ViewHolder>(), Filterable{


    private var buildingDetaildFiltered: List<BuildingListItem>? = null


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var bName: TextView = view.findViewById(R.id.bName)
        var bId: TextView = view.findViewById(R.id.bId)

        init {



            view.setOnClickListener {


                ConnectionCheck.startCheck(context)

                if(ConnectionCheck.getConnectStatus())
                {
                    listener.onBuildingSelected(buildingDetaildFiltered!![adapterPosition])

                }else Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show()



            }
        }
    }

    init {
        this.buildingDetaildFiltered = assignedBuildingsItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_building_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        Log.e("Buildings",buildingDetaildFiltered!!.size.toString())
        return buildingDetaildFiltered!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val building = buildingDetaildFiltered!![position]
        holder.bName.text = building.buildingName
        holder.bId.text = building.buildingId.toString()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {


                Log.e("Buildings adapter","entered")


                val charString = charSequence.toString()
                buildingDetaildFiltered = if (charString.isEmpty()) {
                    assignedBuildingsItem
                } else {
                    val filteredList = ArrayList<BuildingListItem>()
                    for (row in assignedBuildingsItem) {
                        if (row.buildingName!!.toLowerCase().contains(charString.toLowerCase()) || row.buildingId.toString() == charSequence.toString()) {
                            filteredList.add(row)
                        }
                    }
                    filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = buildingDetaildFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults?) {
                buildingDetaildFiltered = filterResults?.values as ArrayList<BuildingListItem>
                notifyDataSetChanged()
            }

        }
    }

}