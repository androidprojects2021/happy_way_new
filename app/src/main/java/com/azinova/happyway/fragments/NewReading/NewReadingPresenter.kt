package com.azinova.happyway.fragments.NewReading

import android.content.Context
import android.util.Log
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.newReading.NewReadingParamModel
import com.azinova.happyway.model.input.newReading.NewReadingParamModelUpdated
import com.azinova.happyway.model.output.InvoiceResponse.InvoiceResponse
import com.azinova.happyway.model.output.ReadingResponse.ReadingResponse
import com.azinova.happyway.network.NetworkCallback

class NewReadingPresenter(newReadingView: NewReadingView) : BasePresenter<NewReadingView>() {

    init {
        super.attachView(newReadingView)
    }

    fun postMeterData(context: Context, newReadingParamModel: NewReadingParamModel) {
        view.showAPILoading()
        addSubscribe(apiStores.postNewReading(newReadingParamModel), object : NetworkCallback<ReadingResponse>() {

            override fun onSuccess(model: ReadingResponse?) {
                view.hideAPILoading()
//                view.showAlert(model?.result?.message, model?.result?.status)


               /* view.showAlert("Price Updated Successfully", model?.result?.status)*/

                view.showToast("Price Updated Successfully", model?.result?.status)

                 /*getPrintData(newReadingParamModel)*/

                view.updateUiSuccess(model?.result?.response)

       }

            override fun onFailure(message: String?) {
                view.hideAPILoading()
            }

            override fun onFinish() {

                view.hideAPILoading()

            }

        })

    }


/*    fun getCAllInvoiceOnSubmit(newReadingParamModel: NewReadingParamModel){

        Log.e("submit2","sdhfkjsdhfjkhs")

        getPrintData(newReadingParamModel)
    }

     private fun getPrintData(newReadingParamModel: NewReadingParamModel) {
        view.showAPILoading()

         Log.e("submit3","sdhfkjsdhfjkhs")

        addSubscribe(apiStores.generateInvoice(newReadingParamModel), object : NetworkCallback<InvoiceResponse>() {

            override fun onSuccess(model: InvoiceResponse?) {
                view.hideAPILoading()

//                view.showAlert(model?.result?.message,model?.result?.status)

                view.updateUi(model?.result?.response)

            }

            override fun onFailure(message: String?) {
                view.hideAPILoading()

            }

            override fun onFinish() {
                view.hideAPILoading()
            }

        })

    }*/




    fun getCAllInvoiceOnSubmit(newReadingParamModel: NewReadingParamModelUpdated){

        Log.e("submit2","sdhfkjsdhfjkhs")

        getPrintData(newReadingParamModel)
    }

    private fun getPrintData(newReadingParamModel: NewReadingParamModelUpdated) {
        view.showAPILoading()

        Log.e("submit3","sdhfkjsdhfjkhs")

        addSubscribe(apiStores.generateInvoice(newReadingParamModel), object : NetworkCallback<InvoiceResponse>() {

            override fun onSuccess(model: InvoiceResponse?) {
                view.hideAPILoading()

//                view.showAlert(model?.result?.message,model?.result?.status)

                view.updateUi(model?.result?.response)

            }

            override fun onFailure(message: String?) {
                view.hideAPILoading()

            }

            override fun onFinish() {
                view.hideAPILoading()
            }

        })

    }


}