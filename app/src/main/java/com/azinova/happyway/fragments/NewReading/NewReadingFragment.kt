package com.azinova.happyway.fragments.NewReading

import android.app.Activity
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.net.Uri.parse
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.azinova.happyway.PrintActivity
import com.azinova.happyway.R
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.fragments.customerDetails.CustomerDetailsFragment
import com.azinova.happyway.model.input.newReading.NewReadingParamModel
import com.azinova.happyway.model.input.newReading.NewReadingParamModelUpdated
import com.azinova.happyway.model.input.newReading.ReadParamsModel
import com.azinova.happyway.model.input.newReading.ReadParamsNewReadUpdated
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.InvoiceResponse.Response
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.ReadingreturnEventModel
import com.azinova.happyway.utils.Logger
import com.azinova.happyway.utils.ObjectFactory
import com.azinova.happyway.utils.ReadTextFromImage
import com.azinova.happyway.utils.Utils
import com.google.android.gms.vision.text.TextRecognizer
import kotlinx.android.synthetic.main.fragment_new_reading.*
import kotlinx.android.synthetic.main.fragment_new_reading.customer_name_txt
import kotlinx.android.synthetic.main.fragment_new_reading.hw_txt
import kotlinx.android.synthetic.main.fragment_new_reading.serial_number
import kotlinx.android.synthetic.main.fragment_new_reading.txt_meter_reading
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.loader_layout.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates

class NewReadingFragment : MvpFragment<NewReadingPresenter>(), NewReadingView {

    private var meter_reading_id:Int=0

    private var dateSelected=""

    var FINALBILL = false

    private val format = DecimalFormat("#.00")

    override fun updateUi(response: Response?) {

        resp = response

        /*net_pay_aed1.text = "AED "+resp?.amountTotal.toString()
        net_pay_aed.text = "AED "+resp?.amountTotal.toString()
        aed_value.text="AED "+resp?.amountUntaxed.toString()
        total_aed.text="AED "+resp?.amountUntaxed.toString()
        vat_aed.text="AED "+resp?.amountTax.toString()*/

        /**
         *
         * prev balance from customer details
         *
         */

        resp?.prevBal=customerDetails?.pendingAmount
        resp?.lastBill = FINALBILL



        val intent = Intent(context, PrintActivity::class.java).apply {
            putExtra("print", resp)
        }


        this.getActivity()?.onBackPressed()
        startActivity(intent)


        MessageEventBus.publish(ReadingreturnEventModel(122,resp?.meterReading.toString(),""))


    }

    override fun showToast(result: String?, status: String?) {

        Toast.makeText(context,result,Toast.LENGTH_SHORT).show()

    }

    override fun updateUiSuccess(response: com.azinova.happyway.model.output.ReadingResponse.Response?) {

        resp1 = response

      /*  net_pay_aed1.text = "AED "+format.format(resp1?.amountTotal).toString()

        net_pay_aed.text = "AED "+format.format(resp1?.amountTotal).toString()

        aed_value.text="AED "+format.format(resp1?.amountUntaxed).toString()

        total_aed.text="AED "+format.format(resp1?.amountUntaxed).toString()

        vat_aed.text="AED "+format.format(resp1?.amountTax).toString()  */


    try {

        meter_reading_id= resp1?.readingId!!

        //net_pay_aed1.text = "AED " + format.format((resp1?.amountUntaxed?.plus(resp1?.monthlyFee!!))?.plus((.05.times(resp1?.amountUntaxed?.plus(resp1?.monthlyFee!!)!!)))).toString()

        net_pay_aed.text = "AED " + format.format((resp1?.amountUntaxed?.plus(resp1?.monthlyFee!!))?.plus((.05.times(resp1?.amountUntaxed?.plus(resp1?.monthlyFee!!)!!)))).toString()


        aed_value.text = "AED " + format.format(resp1?.amountUntaxed).toString()

        total_aed.text = "AED " + format.format(resp1?.amountUntaxed?.plus(resp1?.monthlyFee!!)).toString()

        vat_aed.text = "AED " + format.format(.05.times(resp1?.amountUntaxed?.plus(resp1?.monthlyFee!!)!!)).toString()


    }catch (e:Exception){}

        txt_fixed_aed.text="AED "+format.format(resp1?.monthlyFee).toString()

        txt_units_aed.text="( "+format.format(resp1?.differenceReading).toString()+" Units x AED "+format.format(resp1?.priceUnit).toString()+" x ${resp1?.meter_factor}"+" )"

        /*ok.visibility=View.INVISIBLE*/

    }


    override fun showAlert(result: String?, status: String?) {
        Utils.infoDialogue(context, status, result)
    }

    override fun showAPILoading() {
        orderlistfragment_loader_lty.visibility = View.VISIBLE
    }

    override fun hideAPILoading() {
        orderlistfragment_loader_lty.visibility = View.INVISIBLE
    }

    private var rootView: View? = null
    private var camera: ImageView? = null
    private var meterReading: ImageView? = null
    private var textsample: ConstraintLayout? = null
    private val CAMERA_REQUEST_CODE = 12345
    private val REQUEST_GALLERY_CAMERA = 54654
    private val PIC_CROP = 5412
    private var textRecognizer by Delegates.notNull<TextRecognizer>()
    private var imageUri: Uri? = null
    var s: String? = ""
    private var customerDetails: CustomerListItem? = null
    private var resp: Response? = null
    private var resp1: com.azinova.happyway.model.output.ReadingResponse.Response? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_new_reading, container, false)
        return rootView

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
//        camera = view.findViewById(R.id.cardView1)
        camera = view.findViewById(R.id.camera_icon)
        textsample = view.findViewById(R.id.textSample)
        meterReading = view.findViewById(R.id.meterReading)
        initUI()
        onClick()
        setDAte()




    }


    private fun setDAte() {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        dateSelected = sdf.format(Date())
        dateSelectRead.setText(dateSelected)
        Log.e("date ist",dateSelected)

    }

    private fun initUI() {
        customerDetails = arguments?.getParcelable("customerDetails")
        customer_name_txt.text = customerDetails?.name
        hw_txt.text = customerDetails?.customerId.toString()
        prev_read_date.text=customerDetails?.previousReadingDate
        ok.visibility=View.VISIBLE

        if(customerDetails?.meterNumber.equals(""))
        {
            meterNew.text="NA"
        }
        else meterNew.text = customerDetails?.meterNumber

        var dateFrom =""

        if(customerDetails?.previousReadingDate.equals(""))

        {dateFrom = customerDetails?.contractDate+""}

        else {dateFrom = customerDetails?.previousReadingDate+""}

        billing_date.text=dateFrom +" to "+ SimpleDateFormat("yyyy-MM-dd").format(Date()).toString()

        if (customerDetails?.previousReading == 0.0) {
            txt_meter_reading.text = "0.0 m³"
        } else {
            txt_meter_reading.text = customerDetails?.previousReading.toString() + " m³"
        }
        serial_number.text = customerDetails?.meterNumber

//        readDate.text = customerDetails?.contractDate
//        balance.text = customerDetails?.pendingAmount.toString()

    }

    private fun onClick() {

        arrow_icon1.animate().rotation(180F).start()
        arrow_icon.animate().rotation(-180F).start()
        camera?.setOnClickListener {
            getPic()
        }

           textsample?.setOnClickListener {

               getPic()

        }


        ok.setOnClickListener {

            val read = edit_new_reading.text.toString().trim()

            val newReadingParamModel = NewReadingParamModel()
            val readingParamModel = ReadParamsModel()
            if (read.isBlank()) {
                Toast.makeText(context, "Enter meter reading", Toast.LENGTH_SHORT).show()
            } else {

                readingParamModel.meter_reading = read.toDouble()
                readingParamModel.customer_id = customerDetails?.customerId!!
                readingParamModel.user_id = ObjectFactory.getInstance(context).appPreference.userId
                newReadingParamModel.params = readingParamModel
                presenter.postMeterData(this.context!!, newReadingParamModel)

            }
        }

        submit?.setOnClickListener {
            if (resp1 == null) {
                Toast.makeText(context, "Please press check button to proceed", Toast.LENGTH_SHORT).show()
            } else {

                val read = edit_new_reading.text.toString().trim()

                /*val newReadingParamModel = NewReadingParamModel()*/
                /* val readingParamModel = ReadParamsModel() */

                val newReadingParamModel = NewReadingParamModelUpdated()
                val readingParamModel = ReadParamsNewReadUpdated()

                if (read.isBlank()) {
                    Toast.makeText(context, "Enter meter reading", Toast.LENGTH_SHORT).show()
                } else {

                        readingParamModel.meter_reading = read.toDouble()
                        readingParamModel.customer_id = customerDetails?.customerId!!
                        readingParamModel.user_id = ObjectFactory.getInstance(context).appPreference.userId
                        readingParamModel.reading_id=meter_reading_id
                        readingParamModel.date= dateSelected
                       if(finalBillCheck.isChecked){
                        readingParamModel.finalInvoice= "True"
                           FINALBILL = true
                       }else FINALBILL = false

                        newReadingParamModel.params = readingParamModel



                        presenter.getCAllInvoiceOnSubmit(newReadingParamModel)

                }


                 /*

                 val intent = Intent(context, PrintActivity::class.java).apply {
                    putExtra("print", resp)
                }
                this.getActivity()?.onBackPressed()
                startActivity(intent)

                */

            }

        }

        arrow_icon.setOnClickListener {
            lessDetail.visibility = View.VISIBLE
            detailPriceView.visibility = View.GONE
            lessDetail.animate().translationYBy(120F).translationY(0F).duration = 10
            scroll.post {
                scroll.fullScroll(ScrollView.FOCUS_UP)
            }
            arrow_icon1.animate().rotation(180F).start()
            arrow_icon.animate().rotation(-180F).start()
        }
        arrow_icon1.setOnClickListener {

            detailPriceView.visibility = View.VISIBLE
            lessDetail.visibility = View.GONE
            detailPriceView.animate().translationYBy(120F).translationY(0F).duration = 10
            scroll.post {
                scroll.fullScroll(ScrollView.FOCUS_DOWN)
            }
            arrow_icon.animate().rotation(-360F).start()
            arrow_icon1.animate().rotation(360F).start()
        }


     /*   dateSelectRead.setOnClickListener {

            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(context!!, R.style.DialogTheme ,DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                dateSelected="${year}-${if(monthOfYear+1 >9)"${monthOfYear+1}" else "0${monthOfYear+1}" }-${if(dayOfMonth >9)"${dayOfMonth}" else "0${dayOfMonth}" }"
                dateSelectRead.setText(dateSelected)
                Log.e("DAte ", dateSelected)

            }, year, month, day)

            dpd.show()

        }*/

    }



    private fun getPic() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this.context!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this.context!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        context as Activity,
                        arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_GALLERY_CAMERA)
            } else {
                openCamera()
            }
        } else {
            openCamera()
        }
    }

    private fun openCamera() {
        textRecognizer = TextRecognizer.Builder(context).build()
        if (!textRecognizer.isOperational) {
            Toast.makeText(context, "Dependencies are downloading....try after few moment", Toast.LENGTH_SHORT).show()
            Logger.d("Dependencies are downloading....try after few moment")
            return
        }
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
        imageUri = context?.contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        if (intent.resolveActivity(context?.packageManager!!) != null)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_GALLERY_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                Toast.makeText(context, "Permission Required", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CAMERA_REQUEST_CODE -> {
                    val imageBitmap = MediaStore.Images.Media.getBitmap(
                            context?.contentResolver, imageUri)
                    textsample?.visibility = View.INVISIBLE
                    meterReading?.visibility = View.VISIBLE

//                    meterReading?.setImageBitmap(imageBitmap)

                    val uriTemp = context?.let { ReadTextFromImage(it).getImageUri(imageBitmap) }
                    val cursor = context?.contentResolver?.query(uriTemp!!, null, null, null, null)
                    cursor?.moveToFirst()
                    val n = cursor?.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                    s = n?.let { cursor?.getString(it) }
                    val path = parse(s) as Uri
                    val rotatedBitmap = imageBitmap.rotate(90F)
                    meterReading?.setImageBitmap(rotatedBitmap)

                    try {
                        val cropIntent = Intent("com.android.camera.action.CROP")
                        cropIntent.setDataAndType(imageUri, "image/*")
                        cropIntent.putExtra("crop", "true")
                        cropIntent.putExtra("outputX", 100)
                        cropIntent.putExtra("outputY", 200)
                        cropIntent.putExtra("return-data", true)
//                        startActivityForResult(cropIntent, PIC_CROP)
                    } catch (e: ActivityNotFoundException) {
                        val errorMessage = "Your device doesn't support the crop action!"
                        val toast = Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }
                PIC_CROP -> {
                    val ext = data?.extras
                    val bitmapForRead = ext?.get("data") as Bitmap
                    val readFromImage = ReadTextFromImage(bitmapForRead.rotate(90F), context).result()

                    edit_new_reading.setText(readFromImage)

//                  uncoment for seeing posted image to google vision
//                  meterReading?.setImageBitmap(bitmapForRead)
                }

            }
        }
    }

    fun Bitmap.rotate(degrees: Float): Bitmap {
        var retBitmap: Bitmap? = null
        val matrix = Matrix().apply { postRotate(degrees) }
        retBitmap = Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
//        try {
//            val outputStream=FileOutputStream(s)
//            retBitmap?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
//        }catch (e:Exception){
//            e.printStackTrace()
//        }


        return retBitmap
    }

    override fun createPresenter(): NewReadingPresenter {
        return NewReadingPresenter(this)
    }

}