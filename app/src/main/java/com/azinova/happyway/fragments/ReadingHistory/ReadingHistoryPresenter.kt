package com.azinova.happyway.fragments.ReadingHistory

import android.content.Context
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputParam
import com.azinova.happyway.model.output.ReadingHistoryModel.ReadingHistoryResponse
import com.azinova.happyway.model.output.ReadingHistoryModel.ResponseItem
import com.azinova.happyway.network.NetworkCallback

class ReadingHistoryPresenter(readingHistoryView: ReadingHistoryView) : BasePresenter<ReadingHistoryView>() {

    init {
        super.attachView(readingHistoryView)
    }

    fun getData(context: Context?, paymentHistoryInputParam: PaymentHistoryInputParam) {

        addSubscribe(apiStores.getreadingHistory(paymentHistoryInputParam), object : NetworkCallback<ReadingHistoryResponse>() {
            override fun onSuccess(model: ReadingHistoryResponse?) {
                model?.result?.response?.let {

                    view.hideAPILoading()

                    view.setLayout(it as ArrayList<ResponseItem>)

                }

            }

            override fun onFailure(message: String?) {

                view.hideAPILoading()
                view.failed(message ?: "failed")
//                Toast.makeText(context, "HERE2", Toast.LENGTH_SHORT).show()
            }

            override fun onFinish() {
                view.hideAPILoading()
//                Toast.makeText(context, "HERE3", Toast.LENGTH_SHORT).show()
            }

        })

    }

    fun getData( paymentHistoryInputParam: PaymentHistoryInputParam) {
        addSubscribe(apiStores.getreadingHistory(paymentHistoryInputParam), object : NetworkCallback<ReadingHistoryResponse>() {
            override fun onSuccess(model: ReadingHistoryResponse?) {
                model?.result?.response?.let {
                    view.setLayout(it as ArrayList<ResponseItem>)

                }
            }

            override fun onFailure(message: String?) {
//                Toast.makeText(context, "HERE2", Toast.LENGTH_SHORT).show()
            }

            override fun onFinish() {
//                Toast.makeText(context, "HERE3", Toast.LENGTH_SHORT).show()
            }

        })

    }


}


