package com.azinova.happyway.fragments.Payments

import android.content.Context
import android.view.View
import com.azinova.happyway.R
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.PaymentInput.PaymentInputParam
import com.azinova.happyway.model.input.customerList.CustomerListParamModel
import com.azinova.happyway.model.output.PaymentTypeModel.PaymentTypeModel
import com.azinova.happyway.model.output.PaymentTypeModel.ResponseItem
import com.azinova.happyway.model.output.PaymentTypeModel.ResponsePayPrint
import com.azinova.happyway.model.output.SavePayment.SavePaymentResponse
import com.azinova.happyway.network.NetworkCallback
import com.yarolegovich.lovelydialog.LovelyStandardDialog


class PaymentPresenter(paymentView: PaymentView) : BasePresenter<PaymentView>() {
    init {
        super.attachView(paymentView)
    }

    fun getPaymentTypeFromApi(context: Context, customerListParamModel: CustomerListParamModel) {
        view.showAPILoading()
        addSubscribe(apiStores.getPaymentType(customerListParamModel), object : NetworkCallback<PaymentTypeModel>() {
            override fun onSuccess(model: PaymentTypeModel?) {
                view.hideAPILoading()
                model?.result?.response?.let {
                    view.setLayout(it as ArrayList<ResponseItem>)
                }
            }

            override fun onFailure(message: String?) {
                view.hideAPILoading()
                view.failed(message ?: "failed")
            }

            override fun onFinish() {
                view.hideAPILoading()
            }
        })
    }

    fun savePayment(context: Context, paymentParamModel: PaymentInputParam) {
        view.showAPILoading()
        addSubscribe(apiStores.savePayment(paymentParamModel), object : NetworkCallback<SavePaymentResponse>() {
            override fun onSuccess(model: SavePaymentResponse?) {
                view.hideAPILoading()

//                Utils.infoDialogue(context, model?.result?.status, model?.result?.message)


                if ( model?.result?.status== "success") {

                    var resp=model?.result?.response

                    view.printSuccess(resp)


                } else {

                    LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.VERTICAL)
                            .setTopColorRes(R.color.orange)
                            .setButtonsColorRes(R.color.orange)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle(model?.result?.status)
                            .setMessage(model?.result?.message)
                            .setPositiveButton("Close", View.OnClickListener{
//                                view.exit()

                            })
                            .show()
                }

            }

            override fun onFailure(message: String?) {
                view.hideAPILoading()
            }

            override fun onFinish() {
                view.hideAPILoading()
            }

        })
    }

}