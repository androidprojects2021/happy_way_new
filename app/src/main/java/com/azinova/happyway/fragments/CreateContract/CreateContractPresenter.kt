package com.azinova.happyway.fragments.CreateContract

import android.content.Context
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.contract.InputCreateContract
import com.azinova.happyway.model.input.contract.InputGetBuildings
import com.azinova.happyway.model.input.contract.Params
import com.azinova.happyway.model.input.customerList.CustomerListParamModel
import com.azinova.happyway.model.input.customer_pay.InputSingleCustomer
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.CustomerListNew.CustomerResponse
import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.model.output.contract.Response
import com.azinova.happyway.model.output.contract.ResponseCreateContract
import com.azinova.happyway.model.output.contract.ResponseGetBuildings
import com.azinova.happyway.model.output.contract.Result1
import com.azinova.happyway.model.output.customer_pay.ResponseSingleCustomer
import com.azinova.happyway.network.NetworkCallback
import com.azinova.happyway.utils.ObjectFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CreateContractPresenter(createContractView: CreateContractView): BasePresenter<CreateContractView>() {

    init {
        super.attachView(createContractView)
    }



    fun getBuildingLists(context: Context) {

        val gson= Gson()
        val buildingsPresf = ObjectFactory.getInstance(context).appPreference.buildings
        val buildingArray=object : TypeToken<ArrayList<BuildingListItem>>(){}.type
        val buildings=gson.fromJson<ArrayList<BuildingListItem>>(buildingsPresf,buildingArray)
        view.getBuildingDetails(buildings)

    }

    fun getOldBuildingLists(context: Context) {

        val gson= Gson()
        val buildingsPresf = ObjectFactory.getInstance(context).appPreference.buildings
        val buildingArray=object : TypeToken<ArrayList<BuildingListItem>>(){}.type
        val buildings=gson.fromJson<ArrayList<BuildingListItem>>(buildingsPresf,buildingArray)
        view.getOldBuildingDetails(buildings)

    }


    fun getUnits(building_id:Int){

        addSubscribe(apiStores.getBuildings(InputGetBuildings(Params(building_id))), object : NetworkCallback<ResponseGetBuildings>() {
            override fun onSuccess(model: ResponseGetBuildings?) {

                if(model?.result?.status.equals("success")){
                    view.getUnitDetails(model?.result?.response)
                }

            }

            override fun onFailure(message: String?) {


            }

            override fun onFinish() {



            }

        })

    }



    fun createContract(inputCreateContract: InputCreateContract){
        addSubscribe(apiStores.createCustomerContract(inputCreateContract), object : NetworkCallback<ResponseCreateContract>() {
            override fun onSuccess(model: ResponseCreateContract?) {
                    if (model != null) {
                        if(model.result.status.equals("success")){
                        view.createContractReturn(model)}
                        else{view.createContractfail(model.result.message)}
                    }
            }

            override fun onFailure(message: String?) {
               view.createContractfail(message!!)
            }

            override fun onFinish() {

            }

        })

    }



    fun getCustomer(input: InputSingleCustomer){

        addSubscribe(apiStores.getSingleCustomerNew(input), object : NetworkCallback<ResponseSingleCustomer>() {
            override fun onSuccess(model: ResponseSingleCustomer?) {

                if (model != null) {
                    if(model.result?.status.equals("success")) {

                        view.payCustomer("success", model.result?.customerList?.get(0))
                    }
                    else { view.payCustomer("fail",null)}

                }

            }

            override fun onFailure(message: String?) {

                view.payCustomer("fail",null)

            }

            override fun onFinish() {



            }

        })

    }


    fun customerList(context: Context?, customerListParamModel: CustomerListParamModel) {

        addSubscribe(apiStores.getCustomerList(customerListParamModel), object : NetworkCallback<CustomerResponse>() {
            override fun onSuccess(model: CustomerResponse?) {

                model?.result?.customerList?.let {
                    view.getCustomerList(it as java.util.ArrayList<CustomerListItem>)
                }
            }

            override fun onFailure(message: String?) {


            }

            override fun onFinish() {

            }

        })
    }

}