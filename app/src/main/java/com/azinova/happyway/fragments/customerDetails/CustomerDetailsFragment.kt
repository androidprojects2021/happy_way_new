package com.azinova.happyway.fragments.customerDetails

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.view.isVisible
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.rx_bus_util.GetUpdatesInvoice
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.ReadingreturnEventModel
import com.azinova.happyway.rx_bus_util.SearchEventModel
import kotlinx.android.synthetic.main.fragment_customer_details.*
import kotlinx.android.synthetic.main.fragment_customer_details.bId
import kotlinx.android.synthetic.main.fragment_customer_details.buildingId
import kotlinx.android.synthetic.main.fragment_customer_details.contractDate
import kotlinx.android.synthetic.main.fragment_customer_details.contractId
import kotlinx.android.synthetic.main.fragment_customer_details.date_prev
import kotlinx.android.synthetic.main.fragment_customer_details.etNotes
import kotlinx.android.synthetic.main.fragment_customer_details.mobile
import kotlinx.android.synthetic.main.fragment_customer_details.newReading
import kotlinx.android.synthetic.main.fragment_customer_details.oldReading
import kotlinx.android.synthetic.main.fragment_customer_details.outStandingBalance
import kotlinx.android.synthetic.main.fragment_customer_details.payments
import kotlinx.android.synthetic.main.fragment_customer_details.userId
import kotlinx.android.synthetic.main.fragment_customer_details.userName
import kotlinx.android.synthetic.main.fragment_customer_details_updated.*

class CustomerDetailsFragment : MvpFragment<CustomerDetailsPresenter>(), CustomerDetailsView {
    private var rootView: View? = null
    private var customerDetails: CustomerListItem? = null


    override fun createPresenter(): CustomerDetailsPresenter {
        return CustomerDetailsPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        rootView = inflater.inflate(R.layout.fragment_customer_details, container, false)
        rootView = inflater.inflate(R.layout.fragment_customer_details_updated, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        initView()
        onClick()
        moreMenuClick()


    }


    override fun onResume() {
        super.onResume()

        MessageEventBus.listen(ReadingreturnEventModel::class.java).subscribe{

            if(it.code==122)
            {

                Log.e("return",it?.meter.toString())

                if (it?.meter.equals("0.0")) {

                    oldReading.text="0.0 m³"

                }
                else{
                    oldReading.text=it?.meter.toString()+" m³"
                }


                if (it?.bal.equals("0.0")) {

                    outStandingBalance.text="0.0 AED (Outstanding Balance)"

                }
                else{

                    outStandingBalance.text=it?.bal.toString()+" AED (Outstanding Balance)"
                }


            }

        }


    }





    private fun moreMenuClick()
    {

        MessageEventBus.listen(SearchEventModel::class.java).subscribe{
            if(topMenu.visibility==(View.VISIBLE))
            {

                topMenu.visibility=View.INVISIBLE

            }else{

                topMenu.visibility=View.VISIBLE
            }
        }

    }


    private fun initView() {


        customerDetails = arguments?.getParcelable("customerDetails")
        userName.text = customerDetails?.name

        /*userId.text = customerDetails?.customerId.toString()*/

        userId.text = customerDetails?.code.toString()



//        bId.text=customerDetails?.street

        if(customerDetails?.building_name.equals(""))
        {
            bId.text="NA"

        }else bId.text=customerDetails?.building_name

        /*bName.text=customerDetails?.street2*/



   /*     if(customerDetails?.mobile.equals("")) {
            mobile.text = "NA"
        }else mobile.text = customerDetails?.mobile

        */

        if(customerDetails?.meterNumber.equals("")) {
            mobile.text = "NA"
        }else mobile.text = customerDetails?.meterNumber




        contractId.text=customerDetails?.contractRef
        contractDate.text=customerDetails?.contractDate

        if(customerDetails?.flat_name.equals(""))
        {
            buildingId.setText("NA")

        } else  buildingId.setText(customerDetails?.flat_name)


       if (customerDetails?.previousReading==0.0) {

           oldReading.text="0.0 m³"

        }
        else{
           oldReading.text=customerDetails?.previousReading.toString()+" m³"
       }

        outStandingBalance.text=customerDetails?.pendingAmount.toString()+" AED (Outstanding Balance)"

//        etNotes.text=customerDetails?.notes

        if(customerDetails?.notes.equals(""))
        {
            etNotes1.setText("No Notes Available")
        }
        else   etNotes1.setText(customerDetails?.notes)


        if(customerDetails?.rate.equals("fixed")){

            newReading.visibility=View.GONE
        }

        if(customerDetails?.previousReadingDate.equals(""))
        {
            date_prev.setText("NA")
        }else date_prev.setText(customerDetails?.previousReadingDate.toString())


    }

    private fun onClick() {

        newReading.setOnClickListener {
            var intent = Intent(getActivity(), DetailActivity::class.java).apply {
                putExtra("page", DetailActivity.Pages.READINGPAGE)
                putExtra("customerDetails", customerDetails)
            }
            startActivity(intent)
        }


        payments.setOnClickListener {
            var intent = Intent(getActivity(), DetailActivity::class.java).apply {
                putExtra("page", DetailActivity.Pages.PAYMENT)
                putExtra("customerDetails", customerDetails)

            }
            startActivity(intent)
        }

    }




}