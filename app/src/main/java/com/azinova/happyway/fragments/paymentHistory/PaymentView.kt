package com.azinova.happyway.fragments.paymentHistory

import com.azinova.happyway.model.output.PaymentHistory.ResponseItem

interface PaymentView {

    fun showAPILoading()

    fun hideAPILoading()

    fun failed(message: String)

    fun setLayout(res: ArrayList<ResponseItem>)
}