package com.azinova.happyway.fragments.SummaryReport

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.happyway.R
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputModel
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputParam
import com.azinova.happyway.model.output.Report.PaymentSplitupItem
import com.azinova.happyway.model.output.Report.ResponseReport
import com.azinova.happyway.utils.ObjectFactory
import com.azinova.happyway.utils.ThreadPoolManager
import kotlinx.android.synthetic.main.fragment_report_page.*
import kotlinx.android.synthetic.main.loader_layout.*
import woyou.aidlservice.jiuiv5.IWoyouService
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class SummaryReportFragment(var date : String) : MvpFragment<SummaryReportPresenter>() , SummaryReportView {

    private val SERVICE_PACKAGE = "woyou.aidlservice.jiuiv5"
    private val SERVICE_ACTION = "woyou.aidlservice.jiuiv5.IWoyouService"
    private var woyouService: IWoyouService? = null


    var formats = DecimalFormat("##.00")

    private var rootView: View? = null

    lateinit var paySplit : ArrayList<PaymentSplitupItem>
    lateinit var reportGlobal: ResponseReport

    override fun createPresenter(): SummaryReportPresenter {

        return SummaryReportPresenter(this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_report_page, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectPrinterService()
        initPrinter()

        initView()
        clickListen()

    }

    private fun clickListen() {

        printSummaryReportButton.setOnClickListener {

            printBill()

        }


    }


    fun initPrinter() {
        if (woyouService == null) { // Toast.makeText(PrintReadingHistory.this, "Service Has Been Disconnected", Toast.LENGTH_LONG).show();
            return
        }
        try {
            woyouService!!.printerInit(null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }


    fun connectPrinterService() {
        val intent = Intent()
        intent.setPackage(this.SERVICE_PACKAGE)
        intent.setAction(this.SERVICE_ACTION)

        getActivity()?.startService(intent)

        getActivity()?.bindService(intent, connService, Context.BIND_AUTO_CREATE)
    }


    private val connService: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            woyouService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            woyouService = IWoyouService.Stub.asInterface(service)
        }
    }


    private fun scaleImage(bitmap1: Bitmap): Bitmap? {
        val width = bitmap1.width
        val height = bitmap1.height
        // 设置想要的大小
        val newWidth = (width / 8 + 1) * 6
        // 计算缩放比例
        val scaleWidth = newWidth.toFloat() / width
        // 取得想要缩放的matrix参数
        val matrix = Matrix()
        matrix.postScale(scaleWidth, 1f)
        // 得到新的图片
        return Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,
                true)
    }


    fun printBill() {
        ThreadPoolManager.getInstance().executeTask {
//            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.happyway_printd)
            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.petro_logo_print)
            try {
                printSummaryReportButton.setClickable(false)
                woyouService!!.printBitmap(scaleImage(mBitmap), null)
                woyouService!!.lineWrap(1, null)

/*
                woyouService!!.printTextWithFont("         Al Qusais, Dubai,\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("     United Arab Emirates\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("     TRN : 100065418400003 \n", "ST", 24f, null)*/

                woyouService!!.printTextWithFont("         " + "Summary Report\n********************************\n", "ST", 24f, null)

                if(date.equals("")){
                    woyouService!!.printTextWithFont("Date  :" + SimpleDateFormat("yyyy-MM-dd").format(Date()).toString() + "\n", "ST", 24f, null)
                }else {

                    woyouService!!.printTextWithFont("Date  :" + date+ "\n", "ST", 24f, null)

                }

                woyouService!!.printTextWithFont("********************************\n", "ST", 24f, null)


                woyouService!!.printTextWithFont("Total Invoiced :" +formats.format(reportGlobal.result?.response?.totalInvoiced)+"\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Total Payment  :"+formats.format(reportGlobal.result?.response?.totalPaid) +"\n", "ST", 24f, null)

                for (paySplitItems in  paySplit){
                    woyouService!!.printTextWithFont(paySplitItems.method+" : "+formats.format(paySplitItems.amount)+"\n", "ST", 24f, null)
                }

                woyouService!!.printTextWithFont("          Thank you             ", "ST", 24f, null)
                woyouService!!.lineWrap(3, null)
                printSummaryReportButton.setClickable(true)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }




    private fun initView() {

        orderlistfragment_loader_lty.visibility = View.VISIBLE
        val paymentHistoryInputModel = PaymentHistoryInputModel()

        if(date.equals("")){
            paymentHistoryInputModel.date = SimpleDateFormat("yyyy-MM-dd").format(Date()).toString()
            dateText.text = SimpleDateFormat("yyyy-MM-dd").format(Date()).toString()
        }else {
            paymentHistoryInputModel.date = date

            dateText.text = date
        }
        paymentHistoryInputModel.user_id = ObjectFactory.getInstance(context).appPreference.userId
        val paymentHistoryInputParam = PaymentHistoryInputParam()
        paymentHistoryInputParam.param = paymentHistoryInputModel
        presenter.getData(context, paymentHistoryInputParam)



    }


    override fun showAPILoading() {


    }

    override fun hideAPILoading() {
        orderlistfragment_loader_lty.visibility = View.INVISIBLE

    }

    override fun failed(message: String) {
        orderlistfragment_loader_lty.visibility = View.INVISIBLE

    }

    override fun populateData(report: ResponseReport) {

        reportGlobal=report

        totalInvoiced.text="AED "+formats.format(report.result?.response?.totalInvoiced)
        totalPay.text="AED "+formats.format(report.result?.response?.totalPaid)

        paySplit= report.result?.response?.paymentSplitup as ArrayList<PaymentSplitupItem>

        Log.e("data get text",paySplit.toString())

        paySpliRview.layoutManager = LinearLayoutManager(context)

        var adapter: SummaryAdapter? = null

        adapter =SummaryAdapter(context,paySplit)
        paySpliRview.adapter = adapter


    }


}