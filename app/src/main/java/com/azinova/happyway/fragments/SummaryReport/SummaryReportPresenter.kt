package com.azinova.happyway.fragments.SummaryReport

import android.content.Context
import android.util.Log
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputParam
import com.azinova.happyway.model.output.Report.ResponseReport
import com.azinova.happyway.network.NetworkCallback

class SummaryReportPresenter(summaryReportView: SummaryReportView) : BasePresenter<SummaryReportView>() {

    init {
        super.attachView(summaryReportView)
    }

    fun getData(context: Context?, paymentHistoryInputParam: PaymentHistoryInputParam) {


        addSubscribe(apiStores.getsummaryReport(paymentHistoryInputParam), object : NetworkCallback<ResponseReport>() {
            override fun onSuccess(model: ResponseReport?) {
                model?.result?.response?.let {

                    view.hideAPILoading()

                   view.populateData(model)

                }

            }

            override fun onFailure(message: String?) {

                view.hideAPILoading()
                view.failed(message ?: "failed")
            }

            override fun onFinish() {
                view.hideAPILoading()
            }


        })

    }


}