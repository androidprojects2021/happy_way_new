package com.azinova.happyway.fragments.Payments

import com.azinova.happyway.model.output.PaymentTypeModel.ResponseItem

interface PaymentTypeClickListener {
    fun onTypeClicked(responseItem: ResponseItem)
}