package com.azinova.happyway.fragments.SummaryReport

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.happyway.R
import com.azinova.happyway.model.output.Report.PaymentSplitupItem
import com.azinova.happyway.model.output.Report.ResponseReport
import kotlinx.android.synthetic.main.pay_split_adapter.view.*
import java.text.DecimalFormat

class SummaryAdapter(val context: Context?, val list: List<PaymentSplitupItem>) : RecyclerView.Adapter<SummaryAdapter.ViewHolder>(){



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var formats = DecimalFormat("##.00")

        fun handleData(pos: Int, context: Context, data: PaymentSplitupItem ) {

            itemView.paylabel.text= data.method
            itemView.payvalue.text= "AED "+formats.format(data.amount).toString()

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pay_split_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.handleData(position, context!!, list[position])


    }

}
