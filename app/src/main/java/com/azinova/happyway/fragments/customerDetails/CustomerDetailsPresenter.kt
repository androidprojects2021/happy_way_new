package com.azinova.happyway.fragments.customerDetails

import com.azinova.happyway.base.BasePresenter

class CustomerDetailsPresenter (customerDetailsView: CustomerDetailsView): BasePresenter<CustomerDetailsView>() {
    init {
        super.attachView(customerDetailsView)
    }
}