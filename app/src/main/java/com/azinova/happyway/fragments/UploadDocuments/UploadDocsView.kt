package com.azinova.happyway.fragments.UploadDocuments

import com.azinova.happyway.model.output.customer_pay.CustomerListItem


interface UploadDocsView {

    fun hidePgb(status :String)
    fun payCustomer(status:String, customer : CustomerListItem?)
    fun ShowPgb()

}