package com.azinova.happyway.fragments.UploadDocuments

import android.app.PendingIntent.getActivity
import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.contract.InputCreateContract
import com.azinova.happyway.model.input.customer_pay.InputSingleCustomer
import com.azinova.happyway.model.output.contract.ResponseCreateContract
import com.azinova.happyway.model.output.customer_pay.ResponseSingleCustomer
import com.azinova.happyway.model.output.upliad_image.ResponseImgUploads
import com.azinova.happyway.network.NetworkCallback
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class UploadDocsPresenter(uploadDocsView: UploadDocsView): BasePresenter<UploadDocsView>() {

    init {
        super.attachView(uploadDocsView)
    }

   /* fun upImages(customerID: String, signatureUri1: Uri?, eidFrontUri: Uri?, eidBackUri: Uri?, ejariUri: Uri?) {*/

    fun upImages(customerID: String, signatureUri1: Uri?, eidFrontUri: Boolean, eidBackUri: Boolean, ejariUri: Boolean) {


        val file = File(signatureUri1.toString())
        val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("signature", file.getName(), requestFile)


        val file1 = File("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/eid_front.jpg")
        val requestFile1: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file1)
        val body1 = if(eidFrontUri) MultipartBody.Part.createFormData("emid_front", file.getName(), requestFile1) else null


        val file2 = File("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/eid_back.jpg")
        val requestFile2: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file2)
        val body2 = if(eidBackUri) MultipartBody.Part.createFormData("emid_back", file.getName(), requestFile2) else null


        val file3 = File("/storage/emulated/0/Android/data/com.azinova.happyway/files/Pictures/ejari.jpg")
        val requestFile3: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file3)
        val body3 = if(ejariUri) MultipartBody.Part.createFormData("ejari", file.getName(), requestFile3) else null

        val fullName = RequestBody.create(MediaType.parse("multipart/form-data"), customerID)




        addSubscribe(apiStores.uploadImages(fullName,body,body1,body2,body3), object : NetworkCallback<ResponseImgUploads>() {

            override fun onSuccess(model: ResponseImgUploads?) {

                if(model?.status.equals("success")){

                    view.hidePgb("Success")

                }else {
                    view.hidePgb("Failed")
                }

            }

            override fun onFailure(message: String?) {

                view.hidePgb("Failed")
                Log.e("fail",message.toString())

            }

            override fun onFinish() {


            }

        })


    }



    fun getCustomer(input: InputSingleCustomer){

        addSubscribe(apiStores.getSingleCustomer(input), object : NetworkCallback<ResponseSingleCustomer>() {
            override fun onSuccess(model: ResponseSingleCustomer?) {

                if (model != null) {
                    if(model.result?.status.equals("success")) {

                        view.payCustomer("success", model.result?.customerList?.get(0))
                    }
                    else { view.payCustomer("fail",null)}

                }

            }

            override fun onFailure(message: String?) {

                view.payCustomer("fail",null)

            }

            override fun onFinish() {



            }

        })

    }


}