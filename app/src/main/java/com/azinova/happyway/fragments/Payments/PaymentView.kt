package com.azinova.happyway.fragments.Payments

import com.azinova.happyway.model.output.PaymentTypeModel.ResponseItem
import com.azinova.happyway.model.output.PaymentTypeModel.ResponsePayPrint
import com.azinova.happyway.model.output.SavePayment.SavePaymentResponse

interface PaymentView {
    fun showAPILoading()
    fun hideAPILoading()
    fun failed(s: String)
    fun setLayout(result: ArrayList<ResponseItem>)



    fun exit()
    fun printSuccess(model: ResponsePayPrint?)

}