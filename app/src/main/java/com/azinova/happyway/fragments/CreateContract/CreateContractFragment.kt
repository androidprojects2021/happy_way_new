package com.azinova.happyway.fragments.CreateContract

import android.Manifest
import android.app.Dialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.input.contract.InputCreateContract
import com.azinova.happyway.model.input.contract.Params1
import com.azinova.happyway.model.input.customerList.CustomerListInput
import com.azinova.happyway.model.input.customerList.CustomerListParamModel
import com.azinova.happyway.model.input.customer_pay.InputSingleCustomer
import com.azinova.happyway.model.input.customer_pay.Params
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.model.output.contract.Response
import com.azinova.happyway.model.output.contract.ResponseCreateContract
import com.azinova.happyway.model.output.contract.ResponseItem
import com.azinova.happyway.utils.ObjectFactory
import com.azinova.happyway.utils.ThreadPoolManager
import kotlinx.android.synthetic.main.fragment_create_contract.*
import woyou.aidlservice.jiuiv5.IWoyouService
import java.lang.Exception
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateContractFragment :MvpFragment<CreateContractPresenter>(),CreateContractView {

    val APARTMENT_ARRAY= arrayOf("Studio","1 Bedroom","2 Bedroom","3 Bedroom","4 Bedroom")
    var APARTMENT_SELECTED = APARTMENT_ARRAY[0]

    var format = DecimalFormat("##.00")
    private val SERVICE_PACKAGE = "woyou.aidlservice.jiuiv5"
    private val SERVICE_ACTION = "woyou.aidlservice.jiuiv5.IWoyouService"
    private var woyouService: IWoyouService? = null
    var formats = DecimalFormat("##.00")

    private  var  building :BuildingListItem ? =null
    private  var  OldBuilding :BuildingListItem ? =null
    private  var  flat :ResponseItem ? =null
    private  var  OldCustomer :CustomerListItem ? =null

    private  var  meterId:String = ""
    private  var  lastReading:String = ""

    private  var  emailId:String = ""
    private  var  customerName:String = ""
    private  var  mobileNo:String = ""
    private  var  whatsappNo:String = ""

    private var  disconnectInvoiceValue = true

    lateinit var respToPrint:Response

    private var rootView: View? = null
    private var buildingList:ArrayList<BuildingListItem>  = ArrayList()
    private var oldBuildingList:ArrayList<BuildingListItem>  = ArrayList()
    private var oldCustomerListList:ArrayList<CustomerListItem>  = ArrayList()
    private var flatList:ArrayList<ResponseItem>  = ArrayList()

    override fun createPresenter(): CreateContractPresenter {
        return CreateContractPresenter(this)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_create_contract, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getActivity()?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        nextButtonContract.isEnabled = true
        connectPrinterService()
        initPrinter()



        var arrayApartmentAdapter = ArrayAdapter(requireContext(),android.R.layout.simple_spinner_item,APARTMENT_ARRAY)
        arrayApartmentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        apartmentSpinner.adapter=arrayApartmentAdapter

        presenter.getBuildingLists(context!!)


        buildingSpinner.onItemSelectedListener= object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                building = buildingList[position]
                building?.buildingId?.let { presenter.getUnits(it) }

                Log.e("buil ooooo",building?.buildingName)

            }

        }

        buildingPrevSpinner.onItemSelectedListener= object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                OldBuilding = buildingList[position]
                val customerListParamModel = CustomerListParamModel()
                val customerListInput = CustomerListInput()
                customerListInput.buildingId = OldBuilding?.buildingId!!.toInt()
                customerListInput.userId = ObjectFactory.getInstance(context).appPreference.userId
                customerListParamModel.params = customerListInput
                presenter.customerList(context, customerListParamModel)

                Log.e("buil ooooo OLD",building?.buildingName)

            }

        }

       unitSpinner.onItemSelectedListener= object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {


            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                flat = flatList[position]

            }

        }

        unitPrevSpinner.onItemSelectedListener= object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {


            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                OldCustomer = oldCustomerListList[position]
                usernameText.setText(OldCustomer?.name)
                mobileText.setText(OldCustomer?.mobile)
                wpText.setText(OldCustomer?.whatsap)
                emailText.setText(OldCustomer?.email)

            }

        }


        apartmentSpinner.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {



            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                APARTMENT_SELECTED = APARTMENT_ARRAY[position]
                Log.e("Apartment get",APARTMENT_ARRAY[position])

            }

        }



        onClick()

    }

    private fun onClick() {

        newCustomer.setOnClickListener {
            newCustomer.isChecked=true
            existingCustomer.isChecked=false
            prevText.visibility=View.GONE
            prevLay.visibility=View.GONE
        }

        existingCustomer.setOnClickListener {

            newCustomer.isChecked=false
            existingCustomer.isChecked=true
            prevText.visibility=View.VISIBLE
            prevLay.visibility=View.VISIBLE

            setBuildingList()
        }



        nextButtonContract.setOnClickListener {

            getAllInputs()
           if ( checkIfEmpty()){

               nextButtonContract.isEnabled = false


               if( existingCustomer.isChecked){

                   presenter.createContract(inputCreateContract = InputCreateContract(Params1(customerId = OldCustomer?.customerId,buildingId = building?.buildingId,phone = mobileNo,
                           customerName = customerName,meterId = meterId,email = emailId,flatName = flat?.name,lastMeterReading = lastReading,
                           user_id =ObjectFactory.getInstance(context).appPreference.userId,
                           bedroom = APARTMENT_SELECTED,disconnect = disconnectInvoiceValue.toString().capitalize(),whatsapp = whatsappNo)))

               }else{

                   presenter.createContract(inputCreateContract = InputCreateContract(Params1(customerId = 0 ,buildingId = building?.buildingId,phone = mobileNo,
                           customerName = customerName,meterId = meterId,email = emailId,flatName = flat?.name,lastMeterReading = lastReading,
                           user_id =ObjectFactory.getInstance(context).appPreference.userId,
                           bedroom = APARTMENT_SELECTED,disconnect = disconnectInvoiceValue.toString().capitalize(),whatsapp = whatsappNo)))

               }


           }


            /**
             *
             * for testing without creaton
             */

/*

                val intent = Intent(context, DetailActivity::class.java).apply {
                    putExtra("page", DetailActivity.Pages.UPLOAD_CONTRACT_DOCS)
                    putExtra("customer_id", "7740")
                }
                startActivity(intent)
*/


        }


      /* disconnectInvoice.setOnClickListener {

           Log.e("VALUE", disconnectInvoice.isChecked.toString())

           disconnectInvoice.isChecked = !disconnectInvoice.isChecked
       }
*/

    }

    private fun setBuildingList() {

        presenter.getOldBuildingLists(context!!)

    }

    private fun getAllInputs() {

        customerName = usernameText.text.toString()
        mobileNo = mobileText.text.toString()
        whatsappNo = wpText.text.toString()
        emailId = emailText.text.toString()
        meterId = meterIdText.text.toString()
        lastReading = lastReadingText.text.toString()
        disconnectInvoiceValue = disconnectInvoice.isChecked

    }

    private fun checkIfEmpty():Boolean{


        if(building==null)
        {
            Toast.makeText(context,"No Building Selected",Toast.LENGTH_SHORT).show()
            return false
        }

        if(flat==null)
        {
            Toast.makeText(context,"No Flat Selected",Toast.LENGTH_SHORT).show()
            return false
        }

        if(customerName.equals(""))
        {
            usernameText.setError("mandatory field")
            return false
        }
        if(mobileNo.equals(""))
        {
            mobileText.setError("mandatory field")
            return false
        }
        if(emailId.equals(""))
        {
            emailText.setError("mandatory field")
            return false
        }
        if(meterId.equals(""))
        {
            meterIdText.setError("mandatory field")
            return false
        }
        if(lastReading.equals(""))
        {
            lastReadingText.setError("mandatory field")
            return false
        }

        return true


    }

    override fun getBuildingDetails(buildings: ArrayList<BuildingListItem>) {

        buildingList.clear()
        buildingList.addAll(buildings.sortedWith(compareBy({ it.buildingName })))

        buildingSpinner.adapter  = BuildingSpinnerAdapter(context!!,buildingList)


    }

    override fun getOldBuildingDetails(buildings: ArrayList<BuildingListItem>) {
        oldBuildingList.clear()
        oldBuildingList.addAll(buildings.sortedWith(compareBy({ it.buildingName })))
        buildingPrevSpinner.adapter  = BuildingSpinnerAdapter(context!!,oldBuildingList)
    }

    override fun getUnitDetails(response: ArrayList<ResponseItem>?) {


        flatList.clear()
        response?.let { flatList.addAll(it) }
        unitSpinner.adapter = UnitsSpinnerAdapter(context!!,flatList)

    }

    override fun createContractReturn(response: ResponseCreateContract) {

        nextButtonContract.isEnabled = true

        if(response.result.status.equals("success"))
        {

            respToPrint=response.result.response

            val dialog = Dialog(context!!)

            dialog.setCancelable(false)
            dialog.setContentView(R.layout.cutsom_layout_dialog)

            (dialog.findViewById(R.id.c_p_name) as TextView).text = customerName
            (dialog.findViewById(R.id.c_p_email) as TextView).text = emailId
            (dialog.findViewById(R.id.c_p_phone) as TextView).text = mobileNo
            (dialog.findViewById(R.id.c_p_id) as TextView).text = respToPrint.customer_id
//            (dialog.findViewById(R.id.c_p_bid) as TextView).text = building?.buildingId.toString()
            (dialog.findViewById(R.id.c_p_bid) as TextView).text = building?.buildingName.toString()
            (dialog.findViewById(R.id.c_p_fname) as TextView).text = flat?.name
            (dialog.findViewById(R.id.c_p_mid) as TextView).text = meterId
            (dialog.findViewById(R.id.c_p_lread) as TextView).text = respToPrint.first_reading
            (dialog.findViewById(R.id.c_p_ccode) as TextView).text = respToPrint.contract_code
            (dialog.findViewById(R.id.c_p_dpamt) as TextView).text = respToPrint.deposit_amount
            (dialog.findViewById(R.id.c_p_concharge) as TextView).text = respToPrint.connection_charges
            (dialog.findViewById(R.id.c_p_disconnection_charges) as TextView).text = respToPrint.disconnection_charges

            val imgUps = dialog.findViewById(R.id.imageUpButton) as Button
            val depositRec = dialog.findViewById(R.id.deposirReceiptButton) as Button
            val chargeRec = dialog.findViewById(R.id.chargeReceiptButton) as Button
            val payBtnGo = dialog.findViewById(R.id.payBtnGo) as Button

            payBtnGo.setOnClickListener {

                presenter.getCustomer(InputSingleCustomer(Params(building?.buildingId,ObjectFactory.getInstance(context).appPreference.userId,respToPrint.customer_id.toInt())))

            }


            imgUps.setOnClickListener {

                dialog.dismiss().also {
                val intent = Intent(context, DetailActivity::class.java).apply {
                    putExtra("page", DetailActivity.Pages.UPLOAD_CONTRACT_DOCS)
                    putExtra("customer_id", response.result.customerId)
                    putExtra("building_id", building?.buildingId)

                    Log.e("test1",response.result.customerId)

                }
                this.getActivity()?.onBackPressed().also {  startActivity(intent)}
                }
            }

            chargeRec.setOnClickListener {
                /*printCharges(respToPrint)*/
            }

            depositRec.setOnClickListener {
                printCharges(respToPrint)
            }

            dialog.show()
        }


    }



    override fun createContractfail(response: String) {
        nextButtonContract.isEnabled = true
        Toast.makeText(context,response ,Toast.LENGTH_SHORT).show()

    }

    override fun payCustomer(status: String, customer: com.azinova.happyway.model.output.customer_pay.CustomerListItem?) {

         val intent = Intent(context, DetailActivity::class.java).apply {
                    putExtra("page", DetailActivity.Pages.CUSTOMER_DETAILS)
                    val contact = CustomerListItem(customer?.previousReading, customer?.notes, customer?.contractDate,
                            customer?.street, customer?.buildingName, customer?.mobile, customer?.contractRef,
                            customer?.street2, customer?.previousReadingDate, customer?.meterNumber, customer?.rate,
                            customer?.flatName, customer?.customerId, customer?.pendingAmount, customer?.flatName,
                            customer?.buildingName, customer?.lastInvoicedAmount, customer?.code)
                    putExtra("customerDetails", contact)
                }

                startActivity(intent)

    }

    override fun getCustomerList(customerListModel: java.util.ArrayList<CustomerListItem>) {


        oldCustomerListList.clear()
        oldCustomerListList.addAll(customerListModel.sortedWith(compareBy({ it.name })))
        unitPrevSpinner.adapter  = OldCustomerSpinnerAdapter(context!!,oldCustomerListList)


    }


    fun printDeposit(response: Response){

        ThreadPoolManager.getInstance().executeTask {
//            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.happyway_printd)
            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.petro_logo_print)
            try {

                woyouService!!.printBitmap(scaleImage(mBitmap), null)
                woyouService!!.lineWrap(1, null)


                woyouService!!.printTextWithFont("         Al Qusais, Dubai,\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("     United Arab Emirates\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("     0502573388 / 042673200\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("       utility@petrosafe.ae\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("     TRN : 100065418400003 \n", "ST", 24f, null)
                woyouService!!.printTextWithFont("     Deposit Amount Receipt\n********************************\n", "ST", 24f, null)


                val currentDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
                val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
                woyouService!!.printTextWithFont("Date  :$currentDate $currentTime\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Contract Code    : " + response.contract_code + "\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("Customer ID      : " + response.customer_id + "\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Name             : $customerName\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Email            : $emailId\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Phone            : $mobileNo\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Building         : ${building?.buildingId}\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Flat             : ${flat?.name}\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Phone            : $mobileNo\n", "ST", 24f, null)
                woyouService!!.printTextWithFont("Meter Id         : $meterId\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("Deposit Amount   : ${response.deposit_amount}\n", "ST", 24f, null)

                woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

                woyouService!!.lineWrap(3, null)


            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

    }

    fun printCharges(response: Response){

        try {

            ThreadPoolManager.getInstance().executeTask {
                //            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.happyway_printd)
                val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.petro_logo_print)
                try {


                    woyouService!!.printBitmap(scaleImage(mBitmap), null)
                    woyouService!!.lineWrap(1, null)


                    woyouService!!.printTextWithFont("         Al Qusais, Dubai,\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("     United Arab Emirates\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("     0502573388 / 042673200\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("       utility@petrosafe.ae\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("     TRN : 100065418400003 \n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("          GAS CONTRACT\n", "ST", 24f, null)
                    /*woyouService!!.printTextWithFont("            (RECEIPT)\n********************************\n", "ST", 24f, null)*/
                    woyouService!!.printTextWithFont("       (ACKNOWLEDGEMENT)\n********************************\n", "ST", 24f, null)


                    val currentDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
                    val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
                    woyouService!!.printTextWithFont("Date  :$currentDate $currentTime\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Contract Code : " + response.contract_code + "\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("Customer ID      : " + response.customer_id + "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Name             : $customerName\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Email            : $emailId\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Phone            : $mobileNo\n", "ST", 24f, null)
//                woyouService!!.printTextWithFont("Building         : ${building?.buildingId}\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Building         : ${building?.buildingName}\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Flat             : ${flat?.name}\n", "ST", 24f, null)
                   /* woyouService!!.printTextWithFont("Phone            : $mobileNo\n", "ST", 24f, null)*/
                    woyouService!!.printTextWithFont("Meter Id         : $meterId\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Start Reading    : ${response.first_reading}\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("Deposit Amount   : ${response.deposit_amount}\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Activation Charge: ${response.connection_charges}\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Disconn Charges  : ${response.disconnection_charges}\n", "ST", 24f, null)

                    var vatCharge = 0.0
                    var totalCharge = 0.0
                    var depositCharge = 0.0
                    var disconCharge = 0.0

                    try {

                        if (response.deposit_amount != null && !response.deposit_amount.equals("")) {
                            depositCharge = response.deposit_amount.toDouble()
                        }

                        if (response.disconnection_charges != null && !response.disconnection_charges.equals("")) {
                            disconCharge = response.disconnection_charges.toDouble()
                        }


                        if (response.connection_charges!=null && !response.connection_charges.equals("")) {
                            val connCharge = response.connection_charges.toDouble()
                            vatCharge = (connCharge + disconCharge) * 0.05
                            totalCharge = depositCharge + connCharge + vatCharge +disconCharge
                        }

                    } catch (e: Exception) {
                    }

                    woyouService!!.printTextWithFont("Vat (5%)          : ${format.format(vatCharge)}\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Total             : ${format.format(totalCharge)}\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

                    woyouService!!.lineWrap(3, null)


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }catch (e:Exception){

        }

    }


    fun initPrinter() {
        if (woyouService == null) { // Toast.makeText(PrintReadingHistory.this, "Service Has Been Disconnected", Toast.LENGTH_LONG).show();
            return
        }
        try {
            woyouService!!.printerInit(null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }


    fun connectPrinterService() {
        val intent = Intent()
        intent.setPackage(this.SERVICE_PACKAGE)
        intent.setAction(this.SERVICE_ACTION)

        getActivity()?.startService(intent)
        getActivity()?.bindService(intent, connService, Context.BIND_AUTO_CREATE)

    }


    private val connService: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            woyouService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            woyouService = IWoyouService.Stub.asInterface(service)
        }
    }

    private fun scaleImage(bitmap1: Bitmap): Bitmap? {
        val width = bitmap1.width
        val height = bitmap1.height
        // 设置想要的大小
        val newWidth = (width / 8 + 1) * 6
        // 计算缩放比例
        val scaleWidth = newWidth.toFloat() / width
        // 取得想要缩放的matrix参数
        val matrix = Matrix()
        matrix.postScale(scaleWidth, 1f)
        // 得到新的图片
        return Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,
                true)
    }




}