package com.azinova.happyway.fragments.customerList

import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import java.util.*

interface CustomerListView {

    fun showAPILoading()

    fun hideAPILoading()

    fun getAPIListFail(message: String)

    fun failed(message: String)

    fun getCustomerList(customerListModel: ArrayList<CustomerListItem>)
}