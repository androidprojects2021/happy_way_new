package com.azinova.happyway.fragments.paymentHistory

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.happyway.PrintPaymentHistory
import com.azinova.happyway.PrintReadingHistory
import com.azinova.happyway.R
import com.azinova.happyway.base.MvpFragment
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputModel
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputParam
import com.azinova.happyway.model.output.PaymentHistory.ResponseItem
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.SearchEventModel
import com.azinova.happyway.utils.ObjectFactory
import com.azinova.happyway.utils.ThreadPoolManager
import kotlinx.android.synthetic.main.adapter_payment_history.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_report_page.*
import kotlinx.android.synthetic.main.loader_layout.*
import woyou.aidlservice.jiuiv5.IWoyouService
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PaymentHistoryFragment(var date : String) : MvpFragment<PaymentPresenter>(), PaymentView ,OnClickInterface{
    private val SERVICE_PACKAGE = "woyou.aidlservice.jiuiv5"
    private val SERVICE_ACTION = "woyou.aidlservice.jiuiv5.IWoyouService"
    private var woyouService: IWoyouService? = null
    var formats = DecimalFormat("##.00")

    private var rootView: View? = null
    private var adapter: PaymentHistoryAdapter? = null
    private var responseItem: MutableList<ResponseItem>? = null
    private var responseItemPrint1: MutableList<ResponseItem>? = null

    var total=0.0
    var salesman=""

    override fun createPresenter(): PaymentPresenter {
        return PaymentPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.adapter_payment_history, container, false)
        return rootView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectPrinterService()
        initPrinter()

        initView()
        event()

    }

    private fun event() {

        MessageEventBus.listen(SearchEventModel::class.java).subscribe{

            if (it.code==201){

                if(responseItem?.size!=0){

                    responseItemPrint1=responseItem
                    printHistory()

                }

            }

        }

    }


    private fun initView() {

        orderlistfragment_loader_lty.visibility = View.VISIBLE

        val paymentHistoryInputModel = PaymentHistoryInputModel()

//        paymentHistoryInputModel.date = SimpleDateFormat("yyyy-MM-dd").format(Date()).toString()

        if(date.equals("")){

            paymentHistoryInputModel.date = SimpleDateFormat("yyyy-MM-dd").format(Date()).toString()

        }else paymentHistoryInputModel.date = date



        paymentHistoryInputModel.user_id = ObjectFactory.getInstance(context).appPreference.userId
        val paymentHistoryInputParam = PaymentHistoryInputParam()
        paymentHistoryInputParam.param = paymentHistoryInputModel
        presenter.getData(context, paymentHistoryInputParam)
        responseItem = ArrayList()
        rView.layoutManager = LinearLayoutManager(context)
        rView.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        )
        adapter = context?.let {
            PaymentHistoryAdapter(it, responseItem as ArrayList<ResponseItem>,this,date)
        }
        rView.adapter = adapter
    }

    override fun showAPILoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideAPILoading() {
        orderlistfragment_loader_lty.visibility = View.GONE
    }

    override fun failed(message: String) {
        orderlistfragment_loader_lty.visibility = View.GONE
    }

    override fun setLayout(res: ArrayList<ResponseItem>) {

        if(res.size==0){

            nodataPay.visibility=View.VISIBLE

        }else{

            nodataPay.visibility=View.GONE
            responseItem?.clear()
            responseItem?.addAll(res)
            adapter?.notifyDataSetChanged()



            for(items in res)
            {
                total=total.plus(items.paid_amount!!.toDouble())

            }

            totalPayHistoryTextValue.text="AED "+formats.format(total).toString()

            salesman= res[0].user_name.toString()
        }

    }

    override fun itemClicked(id: Int) {

        var resp: ResponseItem
        resp= responseItem!!.get(id)
        val intent = Intent(context, PrintPaymentHistory::class.java)
        intent.putExtra("print",resp)
        startActivity(intent)

    }


    fun initPrinter() {
        if (woyouService == null) { // Toast.makeText(PrintReadingHistory.this, "Service Has Been Disconnected", Toast.LENGTH_LONG).show();
            return
        }
        try {
            woyouService!!.printerInit(null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }


    fun connectPrinterService() {
        val intent = Intent()
        intent.setPackage(this.SERVICE_PACKAGE)
        intent.setAction(this.SERVICE_ACTION)

        getActivity()?.startService(intent)

        getActivity()?.bindService(intent, connService, Context.BIND_AUTO_CREATE)
    }


    private val connService: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            woyouService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            woyouService = IWoyouService.Stub.asInterface(service)
        }
    }

    private fun scaleImage(bitmap1: Bitmap): Bitmap? {
        val width = bitmap1.width
        val height = bitmap1.height
        // 设置想要的大小
        val newWidth = (width / 8 + 1) * 6
        // 计算缩放比例
        val scaleWidth = newWidth.toFloat() / width
        // 取得想要缩放的matrix参数
        val matrix = Matrix()
        matrix.postScale(scaleWidth, 1f)
        // 得到新的图片
        return Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,
                true)
    }

    private fun printHistory() {

        ThreadPoolManager.getInstance().executeTask {
//            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.happyway_printd)
            val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.petro_logo_print)
            try {

                woyouService!!.printBitmap(scaleImage(mBitmap), null)
                woyouService!!.lineWrap(1, null)


                woyouService!!.printTextWithFont("         " + "Payment List\n********************************\n", "ST", 24f, null)


                if(date.equals("")){
                    woyouService!!.printTextWithFont("Date  :" + SimpleDateFormat("yyyy-MM-dd").format(Date()).toString() + "\n", "ST", 24f, null)
                }else {

                    woyouService!!.printTextWithFont("Date  :" + date+ "\n", "ST", 24f, null)

                }

                if(salesman == null || salesman.equals(""))
                {
                    woyouService!!.printTextWithFont("Salesman  :" + "NA"+ "\n", "ST", 24f, null)

                } else  woyouService!!.printTextWithFont("Salesman  :" + salesman+ "\n", "ST", 24f, null)


                woyouService!!.printTextWithFont("\n********************************\n", "ST", 24f, null)

               /* woyouService!!.printColumnsString(arrayOf("Name", "Flat", "Amount","PayMode"), intArrayOf(1, 1, 1,1), intArrayOf(0, 0, 0,0), null)*/








                for(responses in responseItemPrint1 as ArrayList)
                {

                    var name=responses.partnerName

                    if(responses.partnerName?.length!! >=15)
                    {
                        name=name?.substring(0,15)
                    }
                    /*woyouService!!.printColumnsString(arrayOf(name, responses.flat_name,responses.paid_amount.toString(),responses.journalName.toString()), intArrayOf(1, 1, 1,1), intArrayOf(0, 0, 0,0), null)*/


                    woyouService!!.printColumnsString(arrayOf(name, responses.flat_name), intArrayOf(1, 1), intArrayOf(0, 0), null)
                    woyouService!!.printColumnsString(arrayOf(responses.paid_amount.toString(),responses.journalName.toString()), intArrayOf(1, 1), intArrayOf(0, 0), null)

                    woyouService!!.lineWrap(1, null)

                }

            /*    for(response in responseItemPrint1 as ArrayList)
                {
                    woyouService!!.printTextWithFont("Receipt No       : "+ response.receipt_no+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Name             : "+ response.partnerName+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Mobile           : "+ response.mobile+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Flat             : "+ response.flat_name+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Building         : "+ response.building_name+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Amount PAid      : "+ response.paid_amount+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Balance          : "+ response.remaining_bal+ "\n", "ST", 24f, null)
                    woyouService!!.printTextWithFont("Pay Type         : "+ response.journalName+ "\n\n", "ST", 24f, null)

                    woyouService!!.printTextWithFont("--------------------------------\n", "ST", 24f, null)
                    woyouService!!.lineWrap(2, null)

                }*/

                woyouService!!.printTextWithFont("Total  :" +"AED "+formats.format(total)+ "\n", "ST", 24f, null)


                woyouService!!.printTextWithFont("          Thank you             ", "ST", 24f, null)
                woyouService!!.lineWrap(3, null)


            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

    }


}
