package com.azinova.happyway.fragments.paymentHistory

import android.content.Context
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.PaymentHistoryInput.PaymentHistoryInputParam
import com.azinova.happyway.model.output.PaymentHistory.PaymentHistoryResponse
import com.azinova.happyway.model.output.PaymentHistory.ResponseItem
import com.azinova.happyway.network.NetworkCallback

class PaymentPresenter(paymentView: PaymentView) : BasePresenter<PaymentView>() {
    init {
        super.attachView(paymentView)
    }

    fun getData(context: Context?, paymentHistoryInputParam: PaymentHistoryInputParam) {
        addSubscribe(apiStores.getPaymentHistory(paymentHistoryInputParam), object : NetworkCallback<PaymentHistoryResponse>() {
            override fun onSuccess(model: PaymentHistoryResponse?) {
//                Toast.makeText(context, "HERE", Toast.LENGTH_SHORT).show()
                view.hideAPILoading()
                model?.result?.response?.let {
                    view.setLayout(it as ArrayList<ResponseItem>)

                }
            }

            override fun onFailure(message: String?) {

                view.hideAPILoading()
                view.failed(message ?: "failed")

//                Toast.makeText(context, "HERE2", Toast.LENGTH_SHORT).show()
            }

            override fun onFinish() {
                view.hideAPILoading()
//                Toast.makeText(context, "HERE3", Toast.LENGTH_SHORT).show()
            }

        })

    }

}