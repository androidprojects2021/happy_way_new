package com.azinova.happyway.fragments.CreateContract

import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.model.output.contract.ResponseCreateContract
import com.azinova.happyway.model.output.contract.ResponseItem
import com.azinova.happyway.model.output.customer_pay.CustomerListItem

interface CreateContractView {

    fun getBuildingDetails(buildings : ArrayList<BuildingListItem>)
    fun getOldBuildingDetails(buildings : ArrayList<BuildingListItem>)
    fun getUnitDetails(response: ArrayList<ResponseItem>?)
    fun createContractReturn(response: ResponseCreateContract)
    fun createContractfail(response: String)

    fun payCustomer(status:String, customer : CustomerListItem?)
    fun getCustomerList(customerListModel: java.util.ArrayList<com.azinova.happyway.model.output.CustomerListNew.CustomerListItem>)

}