package com.azinova.happyway.fragments.customerList

import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem

interface CustomerOnClick {
    fun onCustomerSelected(contact: CustomerListItem)
}