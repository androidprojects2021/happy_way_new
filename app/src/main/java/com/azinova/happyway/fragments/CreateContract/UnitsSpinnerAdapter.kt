package com.azinova.happyway.fragments.CreateContract

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.azinova.happyway.R
import com.azinova.happyway.model.output.contract.ResponseItem

class UnitsSpinnerAdapter(val context: Context, var buildingList: ArrayList<ResponseItem>?) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.custome_spinner_contract,null)

        var text = view.findViewById<TextView>(R.id.nameSpinner)

        text.setText(buildingList?.get(position)?.name)

        Log.e("set21","flat ss.toString()")

        return view
    }

    override fun getItem(p0: Int): Any {
        return 0
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return buildingList?.size!!
    }





}