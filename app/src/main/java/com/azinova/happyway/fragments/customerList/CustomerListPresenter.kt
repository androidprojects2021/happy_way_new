package com.azinova.happyway.fragments.customerList

import android.content.Context
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.customerList.CustomerListParamModel
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.CustomerListNew.CustomerResponse
import com.azinova.happyway.network.NetworkCallback
import java.util.*

class CustomerListPresenter(custoncustomerListView: CustomerListView) : BasePresenter<CustomerListView>() {
    init {
        super.attachView(custoncustomerListView)
    }

    fun customerList(context: Context?, customerListParamModel: CustomerListParamModel) {
        view.showAPILoading()
        addSubscribe(apiStores.getCustomerList(customerListParamModel), object : NetworkCallback<CustomerResponse>() {
            override fun onSuccess(model: CustomerResponse?) {
                view.hideAPILoading()
                model?.result?.customerList?.let {
                    view.getCustomerList(it as ArrayList<CustomerListItem>)
                }
            }

            override fun onFailure(message: String?) {
                view.hideAPILoading()
                view.failed(message ?: "failed")

            }

            override fun onFinish() {
                view.hideAPILoading()
            }

        })
    }
}