package com.azinova.happyway.fragments.BuildingsList

import android.content.Context
import android.util.Log
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.utils.ObjectFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class BuildingListPresenter(buildingListView: BuildingListView) : BasePresenter<BuildingListView>() {
    init {
        super.attachView(buildingListView)
    }

    fun getBuildingList(context: Context) {
        view.showAPILoading()
        val gson=Gson()
        val buildingsPresf = ObjectFactory.getInstance(context).appPreference.buildings
        val buildingArray=object :TypeToken<ArrayList<BuildingListItem>>(){}.type
        val buildings=gson.fromJson<ArrayList<BuildingListItem>>(buildingsPresf,buildingArray)
        view.getBuildingDetails(buildings)
        view.hideAPILoading()
    }



}