package com.azinova.happyway.fragments.NewReading

import com.azinova.happyway.model.output.InvoiceResponse.Response

interface NewReadingView {


    fun showAPILoading()

    fun hideAPILoading()
    fun showAlert(result: String?, status: String?)
    fun updateUi(response: Response?)

    fun showToast(result: String?, status: String?)

    fun updateUiSuccess(response: com.azinova.happyway.model.output.ReadingResponse.Response?)



}