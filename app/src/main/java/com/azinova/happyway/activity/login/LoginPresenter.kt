package com.azinova.happyway.activity.login

import android.content.Context
import android.util.Log
import com.azinova.happyway.base.BasePresenter
import com.azinova.happyway.model.input.Login.LoginParamModel
import com.azinova.happyway.model.output.Login.LoginResponse
import com.azinova.happyway.network.NetworkCallback
import com.azinova.happyway.utils.ObjectFactory
import com.google.gson.Gson


class LoginPresenter(loginView: LoginView) : BasePresenter<LoginView>() {
    init {
        super.attachView(loginView)
    }


    fun login(context: Context, loginParamModel: LoginParamModel) {
        view.showLoader()
        addSubscribe(apiStores.doLogin(loginParamModel), object : NetworkCallback<LoginResponse>() {
            override fun onSuccess(model: LoginResponse?) {
                view.hideLoader()
                model.let {
                    if (it != null) {
                        updatePreference(it, context)
                        view.getLoginVerification(it)
                    } else {
                        model?.let {
                            view.failed("Check Login Credentials")
                        }
                    }
                }
            }

            override fun onFailure(message: String?) {
                view.failed(message ?: "Failure")
                view.hideLoader()
            }

            override fun onFinish() {
                view.hideLoader()
            }
        })
    }

    private fun updatePreference(loginResponse: LoginResponse, context: Context) {
        val appManager = ObjectFactory.getInstance(context).appPreferenceManager
        appManager.loginStatus = true
        appManager.userId = loginResponse.result?.userId!!
        val gson = Gson().toJson(loginResponse.result.buildingList)
        appManager.buildings = gson
        Log.e("Buildings", gson)
    }


}