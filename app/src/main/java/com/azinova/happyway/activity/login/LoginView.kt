package com.azinova.happyway.activity.login

import com.azinova.happyway.model.output.Login.LoginResponse

interface LoginView  {

    fun showLocation()
    fun getLoginVerification(loginResponse: LoginResponse)
    fun failed(message: String)
    fun showLoader()
    fun hideLoader()


}