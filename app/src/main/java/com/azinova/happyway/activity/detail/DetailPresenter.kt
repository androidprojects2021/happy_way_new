package com.azinova.happyway.activity.detail

import com.azinova.happyway.activity.detail.DetailView
import com.azinova.happyway.base.BasePresenter

/**
 * Created by BlessonJRaj on 9/5/2019.
 */

class DetailPresenter (detailView: DetailView): BasePresenter<DetailView>(){

    init {
        super.attachView(detailView)
    }
}