package com.azinova.happyway.activity.signature

import android.graphics.Bitmap

object SavedSignBitmap {

    private var signBitmap: Bitmap?=null

    fun getBitmapSign(): Bitmap? { return signBitmap }

    fun setBitmapSign(img : Bitmap){ signBitmap=img }

}