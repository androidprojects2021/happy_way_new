package com.azinova.happyway.activity

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.activity.login.LoginActivity
import com.azinova.happyway.internet_utils.ConnectionCheck
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.SearchEventModel
import com.azinova.happyway.utils.ObjectFactory

class SplashActivity : AppCompatActivity() {
    private var handler: Handler? = null
    val DELAY_TIME = 3000



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
//        MyApplication.sDriverManager
//DriverManager.getInstance()


    }



    override fun onResume() {
        super.onResume()

        if (handler == null) {
            handler = Handler()
        }

        callNextActivity()


    /*
        ConnectionCheck.startCheck(this)

        if(ConnectionCheck.getConnectStatus())
        {
            callNextActivity()

        }else {

            val builder = AlertDialog.Builder(this)

            // Set the alert dialog title
            builder.setTitle("No Internet")

            // Display a message on alert dialog
            builder.setMessage("Connect Internet For Service")

            builder.setCancelable(false)

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()

        }


        MessageEventBus.listen(SearchEventModel::class.java).subscribe {
            if (it.code == 1) {

                callNextActivity()

            }
        }


        */



    }

    private fun callNextActivity() {
        handler?.postDelayed({
           /* var verCode = 0
            try {
                val appversion = packageManager.getPackageInfo(packageName, 0)
                verCode = appversion.versionCode
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            val saveVersionCode = ObjectFactory.getInstance(this@SplashActivity).appPreference.version
            if (saveVersionCode < verCode) {
                ObjectFactory.getInstance(this@SplashActivity).appPreference.version = verCode
            }*/
            if (ObjectFactory.getInstance(this@SplashActivity).appPreference.isLogin) {
                val intent = Intent(this@SplashActivity, DetailActivity::class.java).apply {
//                    putExtra("page", DetailActivity.Pages.CUSTOMER_LIST)
                    putExtra("page", DetailActivity.Pages.BUILDINGS_LIST)
                }

                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this@SplashActivity, LoginActivity::class.java)
//                val intent = Intent(this@SplashActivity, SampleTestActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, DELAY_TIME.toLong())
    }
}
