package com.azinova.happyway.activity.login

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.azinova.happyway.BuildConfig
import com.azinova.happyway.R
import com.azinova.happyway.activity.detail.DetailActivity
import com.azinova.happyway.base.MvpActivity
import com.azinova.happyway.model.input.Login.LoginInput
import com.azinova.happyway.model.input.Login.LoginParamModel
import com.azinova.happyway.model.output.Login.LoginResponse
import com.azinova.happyway.utils.Utils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.view.*
import kotlinx.android.synthetic.main.loader_layout.*

class LoginActivity : MvpActivity<LoginPresenter>(), LoginView {


    override fun showLoader() {
        orderlistfragment_loader_lty.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        orderlistfragment_loader_lty.visibility = View.GONE
    }

    override fun failed(message: String) {
        Utils.infoDialogue(this, message)
    }

    override fun getLoginVerification(loginResponse: LoginResponse) {
        val intent = Intent(this, DetailActivity::class.java).apply {
            putExtra("page", DetailActivity.Pages.BUILDINGS_LIST)
        }
        startActivity(intent)
        finish()
    }

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    /*
     * Represents a geographical location.
     */
    protected var mLastLocation: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_login)
        supportActionBar?.hide()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLocationData()
        OnClickFunction()

    }

    private fun OnClickFunction() {
        login.setOnClickListener {

            if (validateInputs()) {
                val loginInput = LoginInput()
                val loginParamModel = LoginParamModel()
                loginInput.password = etPassword.text.toString().trim()
                loginInput.username = etUsername.etUsername.text.toString().trim()
                loginParamModel.setParams(loginInput)
                presenter.login(this, loginParamModel)
//                Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validateInputs(): Boolean {
        return when {
            etUsername.text.toString().trim().isEmpty() -> {
                Toast.makeText(this, "Enter username", Toast.LENGTH_SHORT).show()
                false
            }
            etPassword.text.toString().trim().isEmpty() -> {
                Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show()
                false
            }
            else -> true
        }
    }

    override fun onStart() {
        super.onStart()
        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient!!.lastLocation
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful && task.result != null) {
                        mLastLocation = task.result
                        showMessage("Latitude " + ":   " +
                                (mLastLocation)!!.latitude + "\n" + "Lattitude " + ":   " +
                                (mLastLocation)!!.longitude)
                    } else {
                        Log.w(TAG, "getLastLocation:exception", task.exception)
                        showMessage(getString(R.string.no_location_detected))
                    }
                }
    }

    /**
     * Shows a [] using `text`.
     * @param text The Snackbar text.
     */
    private fun showMessage(text: String) {

        Toast.makeText(this@LoginActivity, text, Toast.LENGTH_LONG).show()

    }

    /**
     * Shows a [].
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * *
     * @param actionStringId   The text of the action item.
     * *
     * @param listener         The listener associated with the Snackbar action.
     */
    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int,
                             listener: View.OnClickListener) {

        Toast.makeText(this@LoginActivity, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this@LoginActivity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
        //
        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    View.OnClickListener {
                        // Request permission
                        startLocationPermissionRequest()
                    })

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {

                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            } else {

                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        View.OnClickListener {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null)
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        })
            }
        }
    }

    private fun showAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
                .setPositiveButton("Location Settings") { paramDialogInterface, paramInt ->
                    val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(myIntent)
                }
                .setNegativeButton("Cancel") { paramDialogInterface, paramInt -> }
        dialog.show()
    }

    companion object {

        private val TAG = "LocationProvider"

        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }


    private fun getLocationData() {


    }

    override fun showLocation() {
    }


    override fun createPresenter(): LoginPresenter {
        return LoginPresenter(this)
    }

}