package com.azinova.happyway.activity.detail

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.azinova.happyway.R
import com.azinova.happyway.activity.SplashActivity
import com.azinova.happyway.base.MvpActivity
import com.azinova.happyway.fragments.BuildingsList.BuildingFragment
import com.azinova.happyway.fragments.CreateContract.CreateContractFragment
import com.azinova.happyway.fragments.NewReading.NewReadingFragment
import com.azinova.happyway.fragments.Payments.PaymentFragment
import com.azinova.happyway.fragments.ReadingHistory.ReadingHistoryFragment
import com.azinova.happyway.fragments.SummaryReport.SummaryReportFragment
import com.azinova.happyway.fragments.UploadDocuments.UploadDocsFragment
import com.azinova.happyway.fragments.customerDetails.CustomerDetailsFragment
import com.azinova.happyway.fragments.customerList.CustomerListFragment
import com.azinova.happyway.fragments.paymentHistory.PaymentHistoryFragment
import com.azinova.happyway.internet_utils.ConnectionCheck
import com.azinova.happyway.model.output.CustomerListNew.CustomerListItem
import com.azinova.happyway.model.output.Login.BuildingListItem
import com.azinova.happyway.rx_bus_util.MessageEventBus
import com.azinova.happyway.rx_bus_util.SearchEventModel
import com.azinova.happyway.utils.ActivityUtils
import com.azinova.happyway.utils.ObjectFactory
import com.azinova.happyway.utils.ScannerUtil.BarcodeCaptureActivity
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fab_view.*
import kotlinx.android.synthetic.main.fragment_customer_details.*
import kotlinx.android.synthetic.main.fragment_customer_list.*
import java.text.SimpleDateFormat
import java.util.*


class DetailActivity : MvpActivity<DetailPresenter>(), DetailView {

    private var fabExpanded = false

    internal var isWhite = false


    var searchWord=""

    //activity
    enum class Pages {

        BUILDINGS_LIST,
        CUSTOMER_DETAILS,
        UPLOAD_CONTRACT_DOCS,
        CUSTOMER_LIST,
        PAYMENT,
        PAYMENTHISTORY,
        READINGPAGE,

    }

    override fun createPresenter(): DetailPresenter {
        return DetailPresenter(this)
    }

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_detail)

        fabHandler()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        intent?.let {
            val page = it.getSerializableExtra("page") as Pages
            loadPage(page)
        }

        barCode.setOnClickListener {

            val intent = Intent(this, BarcodeCaptureActivity::class.java)
            startActivityForResult(intent, 1)
        }

        search.setOnClickListener {

            Log.e("clicked search btn","yes")

            searchBox.visibility = if(searchBox.visibility == View.VISIBLE) View.GONE else View.VISIBLE

            if(searchBox.visibility==View.VISIBLE)
            {
                Log.e("clicked search btn","Visible")
                searchBuilding()
            }


//            searchBox.visibility = View.VISIBLE

        }

        printList.setOnClickListener {

            if( actionTitle.text == "Reading History"){

                MessageEventBus.publish(SearchEventModel(200,""))

            }else if(actionTitle.text == "Payment History") {

                MessageEventBus.publish(SearchEventModel(201,""))
            }

        }

        moreMenu.setOnClickListener {

            MessageEventBus.publish(SearchEventModel(102,""))

        }


        var cal =Calendar.getInstance()
        var date=""

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {

            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {

                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val myFormat = "yyyy-MM-dd" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)

                date = sdf.format(cal.getTime())

               if(actionTitle.text=="Reading History")
               {
                   val readingHistoryFragment = ReadingHistoryFragment(date)

                   ActivityUtils.replaceFragment(supportFragmentManager, readingHistoryFragment, R.id.detailsactivity_content)
               }
               else  if(actionTitle.text=="Payment History")
               {
                   val paymentHistoryFragment = PaymentHistoryFragment(date)
                   ActivityUtils.replaceFragment(supportFragmentManager, paymentHistoryFragment, R.id.detailsactivity_content)
               }
               else  if(actionTitle.text=="Summary Report")
               {
                   val summaryReportFragment = SummaryReportFragment(date)
                   ActivityUtils.replaceFragment(supportFragmentManager, summaryReportFragment, R.id.detailsactivity_content)

               }

            }

        }

        dateFilterReadingHistory.setOnClickListener {


            DatePickerDialog(this,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()


        }

        paymentHistory.setOnClickListener {
            closeSubMenusFab()
            moreMenu.visibility=View.GONE
            printList.visibility=View.VISIBLE
            dateFilterReadingHistory.visibility=View.VISIBLE
            search.visibility = View.GONE
            actionTitle.text = "Payment History"
            barCode.visibility = View.GONE
            val paymentHistoryFragment = PaymentHistoryFragment(date)
            ActivityUtils.replaceFragment(supportFragmentManager, paymentHistoryFragment, R.id.detailsactivity_content)
        }

        readingHistory.setOnClickListener {
            moreMenu.visibility=View.GONE
            dateFilterReadingHistory.visibility=View.VISIBLE
            readingHistory.visibility = View.GONE
            closeSubMenusFab()
            printList.visibility=View.VISIBLE
            actionTitle.text = "Reading History"
            barCode.visibility = View.GONE
            search.visibility = View.GONE
            val readingHistoryFragment = ReadingHistoryFragment("")
            ActivityUtils.replaceFragment(supportFragmentManager, readingHistoryFragment, R.id.detailsactivity_content)
        }

        contractClick.setOnClickListener {
            moreMenu.visibility=View.GONE
            dateFilterReadingHistory.visibility=View.GONE
            contractClick.visibility = View.GONE
            closeSubMenusFab()
            printList.visibility=View.GONE
            actionTitle.text = "Create Contract"
            barCode.visibility = View.GONE
            search.visibility = View.GONE
            fabSetting.visibility = View.GONE
            val createContractFragment = CreateContractFragment()
            ActivityUtils.replaceFragment(supportFragmentManager, createContractFragment, R.id.detailsactivity_content)
        }

        reportClick.setOnClickListener {
            printList.visibility=View.GONE
            moreMenu.visibility=View.GONE
            dateFilterReadingHistory.visibility=View.VISIBLE
            readingHistory.visibility = View.GONE
            closeSubMenusFab()
            actionTitle.text = "Summary Report"
            barCode.visibility = View.GONE
            search.visibility = View.GONE
            val readingHistoryFragment = SummaryReportFragment("")
            ActivityUtils.replaceFragment(supportFragmentManager, readingHistoryFragment, R.id.detailsactivity_content)
        }

        home1.setOnClickListener {
            val intent = Intent(this@DetailActivity, DetailActivity::class.java).apply {
                //                    putExtra("page", DetailActivity.Pages.CUSTOMER_LIST)
                putExtra("page", DetailActivity.Pages.BUILDINGS_LIST)
            }

            startActivity(intent)
            finish()
        }

        logout.setOnClickListener {


            LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                    .setTopColorRes(R.color.darkred)
                    .setButtonsColorRes(R.color.darkred)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("LOGOUT")
                    .setMessage("Are you sure to logout?")
                    .setPositiveButton("Yes") {
                        ObjectFactory.getInstance(this).appPreference.setLoginStatus(false)
                        startActivity(Intent(this, SplashActivity::class.java))
                        finish()
                    }
                    .setNegativeButton(R.string.no) {
                        closeSubMenusFab()
                    }
                    .show()


        }

        fabSetting?.setOnClickListener {
            ObjectAnimator.ofFloat(fabSetting, View.ROTATION, 0f, -360f).setDuration(500).start()
            val handler = Handler()
            handler.postDelayed({
                if (isWhite) {
                    fabSetting?.setImageDrawable(resources.getDrawable(R.drawable.ic_broadside_white))
                    isWhite = false
                } else {
                    fabSetting?.setImageDrawable(resources.getDrawable(R.drawable.petro_bw))
                    isWhite = true
                }
            }, 400)
            if (fabExpanded) {
                closeSubMenusFab()
            } else {
                openSubMenusFab()
            }
        }

        val builder = AlertDialog.Builder(this)

        builder.setTitle("No Internet")
        builder.setMessage("Connect Internet For Service")
        builder.setCancelable(false)
        val dialog: AlertDialog = builder.create()

            ConnectionCheck.startCheck(this)
            if(!ConnectionCheck.getConnectStatus())
            {
                dialog.show()
            }

            MessageEventBus.listen(SearchEventModel::class.java).subscribe {


                if (it.code == 1) {

                    dialog.dismiss()
                }

                if (it.code == 0) {

                    dialog.show()
                }

            }

    }

    private fun fabHandler() {
        logout.visibility = View.INVISIBLE
        paymentHistory.visibility = View.INVISIBLE
        reportClick.visibility=View.INVISIBLE
        readingHistory.visibility = View.INVISIBLE
        home1.visibility = View.INVISIBLE
        contractClick.visibility = View.INVISIBLE
    }

    private fun openSubMenusFab() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.hide_anim)
        animation.duration = 500
        logout.setAnimation(animation)
        readingHistory.setAnimation(animation)
        paymentHistory.setAnimation(animation)
        reportClick.setAnimation(animation)
        home1.setAnimation(animation)
        contractClick.setAnimation(animation)
        logout.animate()
        paymentHistory.animate()
        readingHistory.animate()
        reportClick.animate()
        home1.animate()
        contractClick.animate()
        animation.start()
        logout.visibility = View.VISIBLE
        paymentHistory.visibility = View.VISIBLE
        reportClick.visibility=View.VISIBLE
        readingHistory.visibility = View.VISIBLE
        home1.visibility = View.VISIBLE
        contractClick.visibility = View.VISIBLE
        fabSetting?.setImageResource(R.drawable.petro_bw)
        fabExpanded = true
    }

    private fun closeSubMenusFab() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.show_anim)
        animation.duration = 500
        logout.setAnimation(animation)
        paymentHistory.setAnimation(animation)
        readingHistory.setAnimation(animation)
        reportClick.setAnimation(animation)
        home1.setAnimation(animation)
        contractClick.setAnimation(animation)
        logout.animate()
        reportClick.animate()
        paymentHistory.animate()
        readingHistory.animate()
        contractClick.animate()
        animation.start()
        logout.visibility = View.INVISIBLE
        paymentHistory.visibility = View.INVISIBLE
        reportClick.visibility=View.INVISIBLE
        readingHistory.visibility = View.INVISIBLE
        home1.visibility = View.INVISIBLE
        contractClick.visibility = View.INVISIBLE
        fabSetting?.setImageResource(R.drawable.ic_broadside_white)
        fabExpanded = false
    }

    companion object {
        var tvresult: String? = null
    }

    override fun onResume() {
        super.onResume()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        if (!tvresult.isNullOrBlank()) {
            Toast.makeText(this, tvresult, Toast.LENGTH_SHORT).show()
            tvresult = ""
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    @SuppressLint("RestrictedApi")
    private fun loadPage(pages: Pages) = when (pages) {

        Pages.CUSTOMER_DETAILS -> {

            moreMenu.visibility=View.VISIBLE
            printList.visibility=View.GONE
            search.visibility = View.GONE
            fabSetting.visibility = View.GONE
            barCode.visibility = View.GONE
            actionTitle.text = "Customer Details"
            var customerDetailsFragment = CustomerDetailsFragment()
            val customerDetails = intent.getParcelableExtra("customerDetails") as CustomerListItem
            val bundleCustomerEdit = Bundle()
            bundleCustomerEdit.putParcelable("customerDetails", customerDetails)
            customerDetailsFragment.arguments = bundleCustomerEdit
            ActivityUtils.replaceFragment(supportFragmentManager, customerDetailsFragment, R.id.detailsactivity_content)
        }
        Pages.READINGPAGE -> {
            printList.visibility=View.GONE
            moreMenu.visibility=View.GONE
            actionTitle.text = "New Reading"
            search.visibility = View.GONE
            fabSetting.visibility = View.GONE
            barCode.visibility = View.GONE
            var newReadingFragment = NewReadingFragment()
            val customerDetails = intent.getParcelableExtra("customerDetails") as CustomerListItem
            val bundleCustomerEdit = Bundle()
            bundleCustomerEdit.putParcelable("customerDetails", customerDetails)
            newReadingFragment.arguments = bundleCustomerEdit
            ActivityUtils.replaceFragment(supportFragmentManager, newReadingFragment, R.id.detailsactivity_content)

        }
        Pages.CUSTOMER_LIST -> {
            printList.visibility=View.GONE
            moreMenu.visibility=View.GONE
            fabSetting.visibility = View.GONE
            actionTitle.text = "All Customers"
            val customerListFragment = CustomerListFragment()
            val buildingItem = intent.getParcelableExtra("buildingList") as BuildingListItem
            val bundleCustomerEdit = Bundle()
            bundleCustomerEdit.putParcelable("buildingList", buildingItem)
            customerListFragment.arguments = bundleCustomerEdit
            ActivityUtils.replaceFragment(supportFragmentManager, customerListFragment, R.id.detailsactivity_content)
        }

        Pages.PAYMENT -> {
            printList.visibility=View.GONE
            moreMenu.visibility=View.GONE
            actionTitle.text = "Payment"
            search.visibility = View.GONE
            barCode.visibility = View.GONE
            fabSetting.visibility = View.GONE
            val paymentFragment = PaymentFragment()
            val customerDetails = intent.getParcelableExtra("customerDetails") as CustomerListItem
            val bundleCustomerEdit = Bundle()
            bundleCustomerEdit.putParcelable("customerDetails", customerDetails)
            paymentFragment.arguments = bundleCustomerEdit
            ActivityUtils.replaceFragment(supportFragmentManager, paymentFragment, R.id.detailsactivity_content)
        }

        Pages.BUILDINGS_LIST -> {
            moreMenu.visibility=View.GONE
            actionTitle.text = "Building List"
            barCode.visibility = View.GONE
            val customerListFragment = BuildingFragment("")
            ActivityUtils.replaceFragment(supportFragmentManager, customerListFragment, R.id.detailsactivity_content)
        }

        Pages.PAYMENTHISTORY -> {
            moreMenu.visibility=View.GONE
            actionTitle.text = "Payment History"
            barCode.visibility = View.GONE
            search.visibility = View.GONE
            val paymentHistoryFragment = PaymentHistoryFragment("")
            ActivityUtils.replaceFragment(supportFragmentManager, paymentHistoryFragment, R.id.detailsactivity_content)
        }

        Pages.UPLOAD_CONTRACT_DOCS->{

                val permission = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)

                if (permission != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.CAMERA),
                            20)

                }else{

                    fabSetting.visibility = View.GONE
                    moreMenu.visibility=View.GONE
                    actionTitle.text = "Upload Document"
                    barCode.visibility = View.GONE
                    search.visibility = View.GONE
                    val uploadDocsFragment = UploadDocsFragment(intent.getStringExtra("customer_id"),intent.getIntExtra("building_id",0))
                    ActivityUtils.replaceFragment(supportFragmentManager, uploadDocsFragment, R.id.detailsactivity_content)

                }

            /*fabSetting.visibility = View.GONE
            moreMenu.visibility=View.GONE
            actionTitle.text = "Upload Document"
            barCode.visibility = View.GONE
            search.visibility = View.GONE
            val uploadDocsFragment = UploadDocsFragment(intent.getStringExtra("customer_id"))
            ActivityUtils.replaceFragment(supportFragmentManager, uploadDocsFragment, R.id.detailsactivity_content)*/

        }


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if(requestCode==20 && (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) )
        {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 20)

        }else{

            loadPage(Pages.UPLOAD_CONTRACT_DOCS)

        }

    }




    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()

            Log.e("back","if")

        } else {

            if(actionTitle.text.equals("Building List")||actionTitle.text.equals("Reading History")||actionTitle.text.equals("Payment History")
                    ||actionTitle.text.equals("Summary Report"))

            {
                val builder = AlertDialog.Builder(this)

                // Set the alert dialog title
                builder.setTitle("Exit HappyWay")

                // Display a message on alert dialog
                builder.setMessage("Are you sure to exit the app ?")

                // Set a positive button and its click listener on alert dialog
                builder.setPositiveButton("YES"){dialog, which ->
                    // Do something when user press the positive button
                    super.onBackPressed()
                }

                builder.setNegativeButton("No"){dialog,which ->

                    dialog.dismiss()

                }

                val dialog: AlertDialog = builder.create()

                dialog.show()

            }else if (actionTitle.text.equals("Create Contract"))
                    {

                        home1.callOnClick()

                    }
              else  if (actionTitle.text.equals("Upload Document"))
                    {

                        val builder = AlertDialog.Builder(this)

                        builder.setTitle("Skip Image Upload ")

                        builder.setMessage("Are you sure to skip image upload ?")

                        builder.setPositiveButton("YES"){dialog, which ->
                            super.onBackPressed()
                        }

                        builder.setNegativeButton("No"){dialog,which ->

                            dialog.dismiss()

                        }

                        val dialog: AlertDialog = builder.create()

                        dialog.show()

                    }
              else  super.onBackPressed()


        }


    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                tvresult = if (data != null) {
                    val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)
                    barcode.displayValue
                } else
                    ""
            } else
                Log.e("TAG", String.format(getString(R.string.barcode_error_format),
                        CommonStatusCodes.getStatusCodeString(resultCode)))
        } else
            super.onActivityResult(requestCode, resultCode, data)

        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }


    private fun searchBuilding(){

      searchBox.addTextChangedListener(object :TextWatcher{
          override fun afterTextChanged(s: Editable?) {

              if( actionTitle.text == "Building List"){


                  MessageEventBus.publish(SearchEventModel(100,s.toString()))
              }


              if( actionTitle.text == "All Customers"){


                  MessageEventBus.publish(SearchEventModel(101,s.toString()))
              }

          }

          override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

          }

          override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


          }
      })

  }


}
