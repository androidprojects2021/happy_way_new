package com.azinova.happyway.activity.signature

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.azinova.happyway.R
import com.github.gcacace.signaturepad.views.SignaturePad
import kotlinx.android.synthetic.main.activity_signature.*

class SignatureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signature)

        buttonConfirm.isEnabled=false
        buttonClear.isEnabled=false

        click()
        signature()
    }

    private fun signature() {


        signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener{
            override fun onStartSigning() {

            }

            override fun onClear() {
                buttonConfirm.isEnabled=false
                buttonClear.isEnabled=false
            }

            override fun onSigned() {

                buttonConfirm.isEnabled=true
                buttonClear.isEnabled=true

            }

        })

    }

    private fun click() {

        imageView7.setOnClickListener {
            finish()
        }

        buttonConfirm.setOnClickListener {

            val sign:Bitmap=signaturePad.signatureBitmap
            SavedSignBitmap.setBitmapSign(sign)
            val intent=Intent()
            intent.putExtra("sign","sign")
            setResult(1,intent)
            finish()

        }

        buttonClear.setOnClickListener {

            signaturePad.clear()

        }

    }
}
